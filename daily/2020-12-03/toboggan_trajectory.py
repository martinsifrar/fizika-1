def count_trees(map, dh=1, dw=3):
    count = 0
    h, w = len(map), len(map[0])
    for i, i_h in enumerate(range(dh, h, dh)):
        count += int(map[i_h][dw*(i + 1) % w] == '#')
    return count

with open('./input') as f:
    map = [line.rstrip() for line in f.readlines()]

    print(count_trees(map))
    print(count_trees(map, 1, 1) *
          count_trees(map, 1, 3) *
          count_trees(map, 1, 5) *
          count_trees(map, 1, 7) *
          count_trees(map, 2, 1))

