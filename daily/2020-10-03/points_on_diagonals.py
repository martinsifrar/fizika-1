import matplotlib
import matplotlib.pyplot as plot

matplotlib.rcParams.update({
    'pgf.texsystem': 'pdflatex',
    'font.family': 'serif',
    'text.usetex': True
})
matplotlib.use('pgf')

x = y = [i + 1 for i in range(5)]
pts = [[(x, n - x) for x in range(1, n)] for n in range(2, 6)]

fig, ax = plot.subplots(1, 1, figsize=[2.5, 2.5])

for line in pts:
    ax.plot(
        *zip(*line),
        linestyle='--',
        color='gray',
        marker='.',
        markersize=8,
        markerfacecolor='k',
        markeredgecolor='k'
    )

fig.tight_layout(pad=1)
ax.set_xlabel('$n_1$')
ax.set_ylabel('$n_2$')
ax.set_xticks(x)
ax.set_yticks(y)

fig.savefig('points-on-diagonals.pgf')
