import re


def count_valid(validate, lines):
    n = 0
    for line in lines:
        if validate(*re.split(r'-|:? ', line)):
            n += 1
    return n

def validate_sled_rental(i, j, char, password):
    return int(i) <= password.count(char) <= int(j)

def validate_toboggan(i, j, char, password):
    return (password[int(i) - 1] == char) != (password[int(j) - 1] == char)

with open('./input') as f:
    lines = [line.rstrip() for line in f.readlines()]

    print(count_valid(validate_sled_rental, lines))
    print(count_valid(validate_toboggan, lines))
