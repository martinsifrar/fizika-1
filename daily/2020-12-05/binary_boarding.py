def pass_ID(board_pass):
    row = int(board_pass[:7].replace('F', '0').replace('B', '1'), 2)
    column = int(board_pass[7:].replace('L', '0').replace('R', '1'), 2)
    return row * 8 + column

def find_missing_ID(passes):
    IDs = list(map(pass_ID, passes))
    for ID in IDs:
        if (ID + 1 not in IDs) and (ID + 2 in IDs):
            return ID + 1

with open('./input') as f:
    passes = [line.rstrip() for line in f.readlines()]

    print(max(map(pass_ID, passes)))
    print(find_missing_ID(passes))
