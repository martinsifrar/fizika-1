# Komentar.

def count_tiles(h, w, k, a=1):
    if a == 2**k:
        return h * w // a**2
    r = w % (2 * a)
    N = r and h / a
    if h % (2 * a) == 0:
        return N + count_tiles(h, w - r, k, 2 * a)
    return N + count_tiles(w - r, h, k, a)
