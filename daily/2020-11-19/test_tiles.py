from tiles import count_tiles

def test_count():
    cases = {
        (22, 29, 3): 53,
        (16, 29, 3): 26,
        (16, 24, 3): 6,
        (200, 2, 3): 100,
        (8, 6, 3): 6,
        (8, 8, 1): 16,
        (2, 3, 2): 3
    }
    for args, ret in cases.items():
        assert count_tiles(*args) == ret
