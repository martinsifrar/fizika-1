Pozdrav

\ 

pišem glede vprašanja, ki sem ga postavil na začetku včerajšnjih KLAFIZ vaj. Moram še najti nek boljši način, kako pisati enačbe preko emaila, zato je preostanek sporočila v priloženem HTML dokumentu.

Mislim, da zdaj razumem, zakaj sta nastavek, ki smo ga uporabili na vajah in nastavek, ki sem ga omenil, enakovredna. Gleda celotno energijo sistema (vode, ki je še v posodi) in preko simbolične manupulacije pride do tega, da energija, ki jo izgubljamo skozi odtekanje vode, ne vpliva na temperaturo vode v posodi. Tisti, ki smo ga uporabili — kot bi rekel profesor Mikuž — to upošteva po fenomenološkem premisleku.

$$-c_p m\,\mathrm{d}T = P\,\mathrm{d}t,$$

Posoda je z dnom debeline $d$ in površino $S$ postavljena na led, ki ima temperaturo $T_l$. Skozi dno posode v led torej teče tok toplotni tok

$$P = \frac{\lambda S}{d} (T - T_l).$$

Poleg tega iz posode izteka voda z volumskim pretokom $\Phi_V$. Zapišemo torej spremembo notranje toplote

$$-\mathrm{d}W_n = P\,\mathrm{d}t - c_p T \,\mathrm{d}m,$$

kar zares pomeni

$$-\frac{\mathrm{d}W_n}{\mathrm{d}t} = P - c_p T \frac{\mathrm{d}m}{\mathrm{d}t}.$$

Ker je $\Delta W_n = c_p m \Delta T$, lahko to razširimo v

$$-c_p\left(\frac{\mathrm{d}m}{\mathrm{d}t}T + m\frac{\mathrm{d}T}{\mathrm{d}t}\right) = P - c_p T \frac{\mathrm{d}m}{\mathrm{d}t}.$$

Vidimo, da se člena $c_p T\frac{\mathrm{d}m}{\mathrm{d}t}$ pokrajšata in če enačbo znova zapišemo v differencialni obliki, dobimo

$$-c_p m\,\mathrm{d}T = P\,\mathrm{d}t,$$

kar je dejanski izraz, ki smo ga zapisali na vajah. Je to smiselno? Če je, a obstaja kakšno hitro pravilo, po katerem bi lahko kar v diferencialni obliki videl, da se člena pokrajšata?

\ 

Lep pozdrav,

Martin
