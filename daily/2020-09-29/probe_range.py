import numpy as np
import matplotlib
import matplotlib.pyplot as plot

n = 21

a_1 = np.repeat(np.linspace(0, np.pi, num=n), n)
a_2 = np.tile(np.linspace(0, np.pi, num=n), n)

x = np.cos(a_1) + np.cos(a_1 + a_2)
y = np.sin(a_1) + np.sin(a_1 + a_2)

plot.scatter(x, y, c=np.sqrt(x**2 + y**2), cmap='viridis')

plot.show()
