import re

def split_by_empty(lines):
    groups = [[]]
    for line in lines:
       if line != '':
           groups[-1].append(line)
       else:
           groups.append([])
    return groups

def to_dict(lines):
    dict_ = dict()
    for line in lines:
        l = [key_val.split(':') for key_val in line.split()]
        dict_.update(
            [key_val.split(':') for key_val in line.split()])
    return dict_

def count_valid(validate, passports):
    n = 0
    for passport in passports:
        if validate(passport):
            n += 1
    return n

def validate_passport(passport):
    fields = {'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'}
    for field in fields:
        if field not in passport:
            return False
    return True

def validate_passport_strict(passport):
    def validate_number(string, lower=None, upper=None, digits=0):
        return bool((string.isnumeric() and
                    ((lower is None) or int(string) >= lower) and
                    ((upper is None) or int(string) <= upper) and
                    ((digits == 0) or len(string) == digits)))

    def validate_height(string):
        return bool(((m := re.match(r'(.+)cm', string)) and
                     validate_number(m.groups()[0], 150, 193)) or
                    ((m := re.match(r'(.+)in', string)) and
                     validate_number(m.groups()[0], 59, 76)))

    def validate_hair_color(string):
        return bool(re.match(r'#[0-9a-f]{6}', string))

    def validate_eye_color(string):
        return (string in {'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'})

    field_validators = {
            'byr': lambda s: validate_number(s, 1920, 2002, 4),
            'iyr': lambda s: validate_number(s, 2010, 2020, 4),
            'eyr': lambda s: validate_number(s, 2020, 2030, 4),
            'hgt': validate_height,
            'hcl': validate_hair_color,
            'ecl': validate_eye_color,
            'pid': lambda s: validate_number(s, digits=9)
    }

    for field, validate_field in field_validators.items():
        if not ((value := passport.get(field)) and
                validate_field(value)):
            return False
    return True

with open('./input') as f:
    lines = [line.rstrip() for line in f.readlines()]
    passports = [to_dict(lines) for lines in split_by_empty(lines)]

    print(count_valid(validate_passport, passports))
    print(count_valid(validate_passport_strict, passports))

