import re


def parse_rules(lines):
    p = re.compile(r'(\w+ \w+) bags contain (\d.*)?.')
    rules = dict()

    def parse_content(content):
        if content:
            content_dict = dict()
            for n_bag in content.split(', '):
                n, bag = n_bag.rsplit(' ', 1)[0].split(' ', 1)
                content_dict.update({bag: int(n)})
            return content_dict
        return dict()

    for line in lines:
        bag, content = p.match(line).groups()
        rules.update({bag: parse_content(content)})
    return rules

def possible_bags(color, rules):
    possible = set()
    for bag, content in rules.items():
        if color in content:
            possible.add(bag)
            possible.update(possible_bags(bag, rules))
    return possible

def count_necessary_bags(color, rules):
    count = 0
    if (content := rules.get(color)):
        count += sum(content.values())
        for bag, n in content.items():
            count += n * count_necessary_bags(bag, rules)
    return count

with open('./input') as f:
    lines = [line.rstrip() for line in f.readlines()]
    rules = parse_rules(lines)

    print(len(possible_bags('shiny gold', rules)))
    print(count_necessary_bags('shiny gold', rules))
