def split_by_empty(lines):
    groups = [[]]
    for line in lines:
       if line != '':
           groups[-1].append(line)
       else:
           groups.append([])
    return groups

def count_union(group):
    return len(set(''.join(group)))

def count_intersect(group):
    intersect = set(group[0])
    for answers in group[1:]:
        intersect = intersect & set(answers)
    return len(intersect)

with open('./input') as f:
    lines = [line.rstrip() for line in f.readlines()]

    print(sum(map(count_union, split_by_empty(lines))))
    print(sum(map(count_intersect, split_by_empty(lines))))
