import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({
    'font.family': 'serif',
    'text.usetex': True
})

fig, axs = plt.subplots(1, 2, figsize=[6, 2.8])

# Draw the triangles.
def serpinski_draw(ax, T, α=0.5, n=1):
    if n == 0:
        ax.fill(T.T[0], T.T[1], color='darkslategrey', linewidth=0)
    else:
        T_mid = (1 - α) * T + α * np.roll(T, -1, axis=0)
        subtris = np.stack(
            [T, T_mid, np.roll(T_mid, 1, axis=0)], axis=1)
        for t in subtris:
            serpinski_draw(ax, t, α, n - 1)

T_0 = np.array([[0, 0], [1, 0], [1, 1 / np.sqrt(2)]])
serpinski_draw(axs[0], T_0, 0.66, 6)
serpinski_draw(axs[1], T_0, 0.5, 6)

axs[0].set_xlabel('$x$')
axs[0].set_ylabel('$y$', labelpad=7)
axs[0].set_title(r'$\alpha = 0.66$', loc='left')
axs[1].set_xlabel('$x$')
axs[1].set_ylabel('$y$', labelpad=7)
axs[1].set_title(r'$\alpha = 0.5$', loc='left')

fig.tight_layout()
fig.savefig('trikotnik.pdf')

fig, ax = plt.subplots(1, 1, figsize=[4, 3])

# Calculate the areas by n iterations.
def serpinski_S(T, α=0.5, n=2):
    if n == 0:
        return 0.5 * np.cross(T[1] - T[0], T[2] - T[0])
    else:
        T_mid = (1 - α) * T + α * np.roll(T, -1, axis=0)
        subtris = np.stack(
            [T, T_mid, np.roll(T_mid, 1, axis=0)], axis=1)
        return sum([serpinski_S(t, α, n - 1) for t in subtris])

n = np.array(range(13))
S = [serpinski_S(T_0, 0.5, i) for i in n]
ax.scatter(n, S, color='darkslategrey', label='izračunane $S(n)$')
η = np.linspace(0, 12)
ax.plot(η, S[0] * (3/4)**η, linestyle='--',
        label='pričakovana $S(\eta)$', zorder=-1)

ax.set_xlabel('$n$ oz. $\eta$')
ax.set_yscale('log')
ax.set_ylabel('$S$')
ax.legend()

fig.tight_layout()
fig.savefig('A-n.pdf')
