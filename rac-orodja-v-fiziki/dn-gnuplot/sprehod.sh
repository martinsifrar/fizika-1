#!/usr/bin/env bash

# dolocimo seme nakljucnega generatorja
RANDOM=$1

# zacetna pozicija
x=0
y=0
z=0

echo "# X Y Z"

# zanka v kateri korakamo
for ((i=0; i < 100000; ++i))
do
  # zgeneriramo naslednji korak
  dx=$((RANDOM % 3 - 1))
  dy=$((RANDOM % 3 - 1))
  dz=$((RANDOM % 3 - 1))

  # pristejemo korak k trenutni legi
  x=$((x+dx))
  y=$((y+dy))
  z=$((z+dz))

  # izpisemo lego
  echo "$x $y $z"
done
