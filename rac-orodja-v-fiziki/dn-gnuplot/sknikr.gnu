#!/usr/bin/gnuplot

set terminal pdf enhanced size 8,6 font "Latin Modern Math,24.0"
set output 'sknikr.pdf'
set dummy nu
A(omega,omega_0,beta) = omega_0**2 / (omega_0**2 - omega**2 + {0,2}*omega*beta)
U(nu) = U_0 * abs(A(2*pi*nu,2*pi*nu_1,beta_1) - A(2*pi*nu,2*pi*nu_2,beta_2))
U_0 = 5e-3
nu_1 = 60e3
beta_1 = 4e3
nu_2 = 70e3
beta_2 = 4e3
fit U(nu) 'sknikr.dat' u 1:2 via U_0,nu_1,beta_1,nu_2,beta_2
set title 'Resonančna krivulja'
set xlabel 'ν [kHz]'
set ylabel 'U(ν) [V]'
set lmargin 12
set rmargin 8
set tmargin 5
set bmargin 5
set samples 1e4
plot 'sknikr.dat' u (1e-3 * $1):2 w p title 'meritve', U(1e3 * nu) w l title 'model'
