#!/usr/bin/gnuplot

set terminal png enhanced size 1024,1024 font 'Latin Modern Math,32.0'
set output 'sprehod-xyz.png'
set title 'Položaj delca'
set xlabel 'x' offset 0,-1
set ylabel 'y' offset 0,-1
set zlabel 'z'
set xtics -100,100,300
set ytics 100
set ztics 100
set lmargin 5
splot 'sprehod.dat' w l title 'položaj'

reset session

set terminal pdf enhanced size 8,6 font "Latin Modern Math,24.0" 
set output 'sprehod-r.pdf'
set title 'Razdalja od izhodišča'
set xlabel 't'
set ylabel 'r(t)'
set lmargin 12
set rmargin 8
set tmargin 5
set bmargin 5
set log xy
plot 'sprehod.dat' u 0:(sqrt($1**2 + $2**2 + $3**2)) w l title 'razdalja', '' u 0:(sqrt($0)) w l title 't^{1/2}'
