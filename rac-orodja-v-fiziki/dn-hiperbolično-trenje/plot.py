import numpy as np
import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({
    'font.family': 'serif',
    'text.usetex': True,
})

from scipy.integrate import solve_ivp
from scipy.signal import find_peaks


# Sistem n. d. enačb 1. reda za nihalo
def dot_y(t, y, b, c, v_c):
    x, v = y
    return [v, -b*np.tanh(v/v_c) - c*x]

# Začetni pogoji
y_0 = (5e-2, 0)

# Parametri
k_tr = 0.01
g = 9.81
k = 0.2
m = 0.1
v_c = 0.2
b = k_tr*g
c = k/m

# Reševanje za x in v t do 20 s
sol = solve_ivp(dot_y, (0, 20), y_0, args=(b, c, v_c), max_step=0.01)
t, (x, v) = sol.t, sol.y

# Grafa x(t) in v(x)
fig, ax = plt.subplots(1, 1, figsize=[5, 4])
ax.plot(t, x)

ax.set_xlabel(r'$t\,[\mathrm{s}]$')
ax.set_ylabel(r'$v\,[\mathrm{m/s}]$')
fig.tight_layout()
fig.savefig('x-t.pdf')

fig, ax = plt.subplots(1, 1, figsize=[5, 4])
ax.plot(x, v, color='tab:green')

ax.set_xlabel(r'$x\,[\mathrm{m}]$')
ax.set_ylabel(r'$v\,[\mathrm{m/s}]$')
fig.tight_layout()
fig.savefig('v-x.pdf')

# Reševanje za moč trenja t do 50 s
sol = solve_ivp(dot_y, (0, 50), y_0, args=(b, c, v_c), max_step=0.01)
t, (x, v) = sol.t, sol.y

# Moč trenja
P_tr = b * np.tanh(v/v_c) * m*v

# Popravek za minimume
mins, _ = find_peaks(-P_tr)
P_tr[mins] = 0

# Graf P_tr(t)
fig, ax = plt.subplots(1, 1, figsize=[6, 4])
ax.plot(t, 1e3 * P_tr,
        label=r'moč trenja $P_{tr}$',
        color='cadetblue')
ax.plot(t, 0.5 * np.exp(-b*t / v_c),
        label=r'ovojnica $\frac{1}{2} e^{-\frac{bt}{v_c}}$',
        color='black', linestyle=':')

ax.set_yscale('log')
ax.set_ylim(1e-11, 1)
ax.set_xlabel(r'$t\,[\mathrm{s}]$')
ax.set_ylabel(r'$P_{tr}\,[\mathrm{mW}]$')
ax.legend()

fig.tight_layout()
fig.savefig('P-t.pdf')
