% !TEX program = xelatex

% Layout and styling.
\documentclass{article}
\usepackage[a4paper, margin=1in]{geometry}

% Locale
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Bibliography
\usepackage[backend=biber,style=numeric]{biblatex}
\addbibresource{literatura.bib}

% Other
\usepackage{amsmath}
\usepackage{siunitx}
\usepackage{graphicx}

% Scientific notation
\newcommand\scinot[2]{\num[round-precision=#1,round-mode=figures,exponent-product=\ensuremath{\cdot}]{#2}}

\title{Nevarni asteroid}
\author{Martin Šifrar}

\begin{document}

\maketitle

\section{Motivacija}

V 17. stoletju je Newtonov zakon gravitacije astronomijo povzdignil na nov nivo -- naenkrat je bilo mogoče postavljati napovedi o kasnejših opazovanjih. Danes je astrodinamika področje, kjer ne pričakujemo novih odkritij, a še vedno predstavlja izvrsten problem na katerem lahko ostrimo svoje fizikalne kremplje.

Obravnavali bomo Sonce, Zemljo, Luno in asteroid, ki ta sistem preleti. S pomočjo računalnika si bomo ogledali nekaj scenarijev, ki so rezultat preleta masivnega objekta skozi sistem. Še pred tem bomo nekaj povedali tudi o reševanju samega problema.

\section{Metoda}

\subsection{Orodja}

Za reševanje problema sem uporabil Python v JupyterLab okolju. Slike in animacije so izdelane s knjižnico Matplotlib. 

\subsection{Enote}

Ko opisujemo astronomski sistem, je bolje, da količine izrazimo v enotah, ki ne zahtevajo izjemno velikih številk. Čas tako opisujemo v dnevih $\si{d}$, dolžino v astronomskih enotah $\si{AU}$ in maso v Sončevih masah $\si{M_\odot}$. Njihove pretvorbe v SI enote so~\autocite{astroconstants-2021}

\begin{align*}
    1\,\si{d} &= 24 \cdot 60^2\,\si{s} = 86400\,\si{s},\\
    1\,\si{AU} &= 1.49597870700 \cdot 10^{11}\,\si{m},\\
    1\,\si{M_\odot}. &= 1.9884 \cdot 10^{30}\,\si{kg}.
\end{align*}

\subsection{Sistem}

Sonce, Zemljo, Luno in asteroid bomo v nadaljevanju zaznamovali z indeksi $\odot$, $Z$, $L$ in $ast$. Ta sistem je 3-dimenzionalen, a za naše potrebe zadostuje tudi njegov 2-dimenzionalen približek v ekliptični ravnini. Na vsako od teles delujejo privlačne sile drugih teles. Te povzročijo, da se giblje s pospeškom

\begin{equation} \label{eq:a}
    \mathbf{a}_i = \sum_{j \neq i} \mathbf{a}_{ij} = G \sum_{j \neq i} m_j \frac{\mathbf{r}_j - \mathbf{r}_i} {\left|\, \mathbf{r}_j - \mathbf{r}_i \,\right|^3}.
\end{equation}

Ker je sonce v primerjavi z ostalimi telesi v sistemu izjemno masivno, lahko njegov pospešek zaradi gravitacije drugih teles zanemarimo. Smiselno je tudi, da vanj postavimo izhodišče našega koordinatnega sistema, torej $\mathbf r_\odot = (0, 0)$. Preostale krajevne vektorje in hitrosti zaradi priročnosti zložimo v matriko

\begin{equation}
    Y = \begin{bmatrix} \mathbf r_Z & \mathbf r_L & \mathbf r_{ast} & \mathbf v_Z & \mathbf v_L & \mathbf v_{ast} \end{bmatrix}.
\end{equation}

Njen odvod (po komponentah) $\dot Y$ je znana funkcija $F(t, Y)$, saj iz enačbe \ref{eq:a} poznamo pospeške vseh teles, hitrosti pa so preprosto vsebovane v spremenljivki $Y$.

\subsection{Podatki in začetni pogoji}

Za izračun pospeška potrebujemo mase teles. Masa Sonca je točno $1\,\si{M_\odot}$ -- za maso Zemlje in Lune uporabimo vrednosti~\autocite{astroconstants-2021}

\begin{align*}
    M_Z &= \frac{1\,\si{M_\odot}}{332946.0487},\\
    M_L &= \frac{1\,\si{M_\odot}}{27068703}.
\end{align*}

Za gravitacijsko konstanto uporabimo vrednost~\autocite[56]{codata-2016}

\begin{equation*}
    G = 6.67408 \cdot 10^{-11}\,\si{m^3/kg s^2}.
\end{equation*}

Da sistem numerično rešimo potrebujemo začetne položaje vseh teles. Za Zemljo in Luno te pridobimo iz JPL HORIZONS~\autocite{horizons} sistema efemerid nebesnih teles. Uporabimo efemeride z začetka 22. julija, ob 00:00. Ker sistem obravnavamo v 2 dimenzijah, dimenzijo, ki je pravokotna na ekliptiko, preprosto izpustimo.

\begin{table}[ht]
    \centering
    \begin{tabular}{r|c}
        telo &  $\mathbf r_0\,[\si{AU}]$\\
        \hline
        Zemlja & $(0.4867600671579733, -0.8828010515327250)$\\
        Luna & $(0.4867830372379103, -0.8852377188886607)$\\
             & $\mathbf v_0\,[\si{AU/d}]$\\
        \hline
        Zemlja & $(0.01473523426849295, 0.008303938820248347)$\\
        Luna & $(0.01535626467951449, 0.008306578661235062)$
    \end{tabular}
    \caption{Začetna položaja in hitrosti Zemlje in Lune 22. julija ob 00:00. Navedene so vse števke, ki jih sistem ponudi.}
\end{table}

Začetni pogoji ter masa asteroida bodo določeni v posameznih scenarijih.

\subsection{Metoda integracije}

Za numerično integriranje navadne diferencialne enačbe $\dot Y = F(t, Y)$ uporabimo eksplicitno Runge-Kutta metodo 4. reda. Napredujemo po korakih $\mathrm{d}t$ in vrednosti posodabljamo kot

\begin{equation}
    Y(t + dt) = Y(t) + \frac{k_1 + 2k_2 + 2k_3 + k_4}{6} \mathrm{d}t,
\end{equation}

pri čemer so koeficienti

\begin{align*}
    k_1 &= F\left( t, Y \right),\\
    k_2 &= F\left( t + \frac{\mathrm{d}t}{2}, Y + k_1\frac{\mathrm{d}t}{2} \right),\\
    k_3 &= F\left( t + \frac{\mathrm{d}t}{2}, Y + k_2\frac{\mathrm{d}t}{2}\right),\\
    k_4 &= F\left( t + \mathrm{d}t, Y + k_3\mathrm{d}t\right).
\end{align*}

\subsection{Izbira koraka} \label{sub:step}

Numerična rešitev je približek, njegova napaka pa je v prvi vrsti odvisna od dolžine koraka $\mathrm{d}t$. Manjši korak pomeni manjšo napako. A poleg napake zaradi velikosti koraka (ang. \textit{truncation error}), je rešitev netočna tudi zaradi končne natančnosti števil s plavajočo vejico (ang. \textit{roundoff error}). Pri zelo majhnih korakih $\mathrm{d}t$ ta drugi tip napake prevlada, tako da nadaljnje zmanjševanje koraka napake ne zmanjša, temveč jo kvečjemu poveča. Da se temu izognemo, a še vedno dosežemo čim manjšo napako, bi radi našli točko, kjer napaka zaradi končne natančnosti prevlada nad napako zaradi velikosti koraka.

Enačbo $\dot Y = F(Y, t)$ rešujemo na intervalu od 0 do nekega končnega časa $t_f$. Za konkretno rešitev $Y_k(t) \approx Y(t)$ interval $k$-krat prepolovimo, tako da je dolžina posameznega koraka

\begin{equation*}
    \mathrm{d}t = \frac{t_f}{2^k}.
\end{equation*}

Poljuben element rešitve $y_k = (Y_k)_{ij}$ ima napako $\varepsilon_k$ zaradi dolžine koraka, tako da velja

\begin{equation*}
    y_k = y + \varepsilon_k,
\end{equation*}

pri čemer je seveda tudi $\varepsilon_k$ funkcija časa. Po besedah bolj učenih ljudi~\autocite[125-148]{hubbard-1991} je napaka $\varepsilon_k$ navzgor omejena, pri dovolj majhnih korakih $\mathrm{d}t$ pa celo blizu $C(\mathrm{d}t)^p$. Pri tem je $C$ neka konstanta, $p$ pa stopnja metode integracije.

\begin{equation*}
    y_k \approx y + C \left( \frac{t_f}{2^k} \right)^p.
\end{equation*}

Osredotočimo se na napako pri času $t_f$, kjer lahko pričakujemo, da bo največja. Ker točne rešitve ne poznamo, resnične vrednosti $\varepsilon_k(t_f)$ ne moramo izračunati, lahko pa izračunamo razliko med konkretnima rešitvama zaporednih stopenj

\begin{align*}
    D_k &= y_k(t_f) - y_{k - 1}(t_f)\\
        &\approx \left[ y(t_f) + C \left( \frac{t_f}{2^k} \right)^p \right] - \left[ y(t_f) + C \left( \frac{t_f}{2^{k - 1}} \right)^p \right]\\
        &= C \left( \frac{t_f}{2^k} \right)^p (1 - 2^p).
\end{align*}

Ker ne poznamo konstante $C$, lahko pogledamo razmerje med zaporednima razlikama $\frac{D_{k - 1}}{D_k} \approx 2^p$. Dvojiški logaritem tega razmerja imenujemo $R_k$

\begin{equation*}
    R_k = \log_2 \frac{D_{k - 1}}{D_k} \approx p.
\end{equation*}

\begin{figure} \label{fig:R_k-by-k-and-dt}
    \centering
    \includegraphics{R_k-by-k-and-dt.pdf}
    \caption{$R_k$ pri različnih stopnjah delitve $k$ in dolžini koraka $\mathrm{d}t$. Napako gledamo z začetnimi podatki scenarija A, pri času $t_f = 1000\,\mathrm{d}$. V različnih barvah je prikazanih 12 elementov $Y_k$. Ko postane korak dovolj majhen, opazimo območje konvergence, kjer je $R_k$ blizu 4. Napaka zaradi končne natančnosti prevlada malo po $1\,\si{h}$. Odločimo se, da za korak vzamemo $1\,\si{h}$.}
\end{figure}

Ko je korak dovolj majhen in metoda konvergira, je $R_k$ blizu stopnji metode integracije $p$. V našem primeru je ta 4, saj uporabljamo Runge-Kutta metodo 4. stopnje. Ko manjšamo korak, postane napaka zaradi končne natančnosti večja v primerjavi z $\varepsilon_k$. Ko ta napaka prevlada, je območja konvergence konec in $R_k$ se oddalji od $p$. Smiselno je torej, da vzamemo najkrajši korak, ki je še znotraj območja konvergence. S tem v mislih lahko iz slike \ref{fig:R_k-by-k-and-dt} utemeljeno izberemo korak

\begin{equation*}
    \mathrm{d}t = 1\,\si{h}.
\end{equation*}

\textbf{Komentar.} Bolje bi si želel razumeti, zakaj je v nekaterih primerih $R_k$ v območju konvergence celo višji od dejanskega reda metode. To je omenjeno v \autocite[132]{hubbard-1991}, obnašanje pa je mogoče opaziti tudi v območju konvergence na sliki \ref{fig:R_k-by-k-and-dt}. Nisem prepričan, a sumim, da je to povezano s periodičnostjo gibanja po orbiti.

\subsection{Energija sistema}

Kot v vsakem fizikalnem sistemu, se tudi v našem ohranja energija. Smiselno je torej, da poračunamo energijo sistema in pogledamo, kako se spreminja s časom. Če izračunamo, da se energija znatno spreminja, to kaže na napako v metodi.

Energija našega sistema je vsota potencialne in kinetične energije. Za potencialno energijo si predstavljamo, da so na začetku vsa telesa neskončno narazen -- potencialna energija je 0. Ko telo prinesemo v okolico drugih, sile gravitacije drugih teles opravijo delo. Če vzamemo, da je $\odot, Z, L, ast$ naravni vrstni red indeksov, lahko potencialno energijo celotnega sistema zapišemo kot

\begin{equation*}
    W_p = -G \sum_{i < j} \frac{m_i m_j}{\left| \mathbf r_j - \mathbf r_i \right|}.
\end{equation*}

Kinetična energija je preprosto vsota posameznih kinetičnih energij teles.

\begin{equation*}
    W_k = \sum_i \frac{m_i |\mathbf v_i|^2}{2}.
\end{equation*}

Zanima nad, če se skupna energija $W = W_p + W_k$ spreminja. Kot dva indikatorja zdrsa energije pogledamo spremembo $\Delta W = W(t_f) - W(0)$ in standardni odklon od povprečja $\sigma_W$. V tabeli \ref{tab:W-drift} vidimo njuni relativni vrednosti pri različnih dolžinah korakov za 1000 dni scenarija A. Vidimo, da se z manjšanjem koraka zdrs sprva manjša. Pri korakih, manjših od $1\,\si{h}$, se manjšanje ustavi -- pri še manjših korakih pa se zdrs celo nekoliko poveča. Iz tega zaključimo, da je rešitev najbolj natančna pri koraku okoli $1\,\si{h}$. To sovpada z ugotovitvijo v pododdleku \ref{sub:step}.

\begin{table}[ht] \label{tab:W-drift}
    \centering
    \begin{tabular}{r|r r}
        \multicolumn{1}{r|}{$\mathrm{d}t\,[\si{h}]$} & \multicolumn{1}{c}{$\frac{\Delta W}{\overline W}$} & \multicolumn{1}{c}{$\frac{\sigma_W}{\overline W}$}\\
        \hline
        168 & \scinot{4}{-9.49362869e-05} & \scinot{4}{-1.83800848e-05}\\
        48 & \scinot{4}{-1.76614961e-03} & \scinot{4}{-8.11717137e-04}\\
        14 & \scinot{4}{ 7.16468468e-08} & \scinot{4}{-2.06780078e-08}\\
        8 & \scinot{4}{ 2.86031463e-10} & \scinot{4}{-8.24385985e-11}\\
        4 & \scinot{4}{ 8.93118629e-12} & \scinot{4}{-2.57141696e-12}\\
        2 & \scinot{4}{ 2.89135057e-13} & \scinot{4}{-8.35254276e-14}\\
        1 & \scinot{4}{-8.81991961e-15} & \scinot{4}{-5.58825468e-15}\\
        0.5 & \scinot{4}{-4.07073213e-15} & \scinot{4}{-7.27433637e-15}\\
        0.25 & \scinot{4}{-5.94779194e-14} & \scinot{4}{-1.91626135e-14}\\
        0.125 & \scinot{4}{ 1.92229017e-14} & \scinot{4}{-1.25306768e-14}
    \end{tabular}
    \caption{Relativna sprememba in relativni standardni odklon pri različnih korakih $\mathrm{d}t$. Minimalen zdrs opazimo za korake okoli $1\,\si{h}$. Zdrs računamo z začetnimi podatki scenarija A, do končnega časa $t_f = 1000\,\mathrm{d}$.}
\end{table}

\subsection{Animacije}

Za vse scenarije, ki so v nadaljevanju predstavljeni, sem izdelal tudi animacije. Vsako telo ima poleg pike, ki označuje trenutno lokacijo, tudi rep, ki označuje njihovo pot v zadnjih $\sim 42$ dneh. Animacije so priložene v \verb|avi| datotekah. Vsa imena vsebujejo oznaka scenarija in čas trajanje simulacije. 2000 dni dolga animacija od začetka scenarija A je tako poimenovana \verb|A-2000d.avi|. Če sta v imenu navedena dva časa, je prvi čas začetka, drugi pa čas konca animacije. Animacije imajo 30 sličic na sekundo. Izračuni za animacije uporabljajo korak $2\,\si{h}$. Odstopanje je namreč veliko premajhno, da bi ga opazili s prostim očesom.

\section{Scenariji}

\subsection{Realistični asteroid}

Kot scenarij A si pogledamo asteroid z realistično maso. Za to vzamemo $2.86 \cdot 10^{19}\,\si{kg}$, kar je podobno asteroidu 3 Juno~\autocite{wiki-3-juno}. Asteroid pot začne na premici, ki $1\,\si{AU}$ oddaljena od Sonca. Njegova začetna hitrost je $17.93\,\si{km/s}$, kar je tipična orbitalna hitrost prej omenjenega asteroida. Kot to vidimo na sliki \ref{fig:A}, asteroid prekriža Zemljin tir in se ustali v eliptično orbito okoli Sonca.

\begin{figure} \label{fig:A}
    \centering
    \includegraphics{A-600D.pdf}
    \caption{Tiri Zemlje, Lune in asteroida 600 dni po začetku scenarija A. Luna je ves čas preblizu Zemlje, da bi ju lahko razločili.}
\end{figure}

\begin{figure} \label{fig:A-r}
    \includegraphics{A-r.pdf}
    \caption{Spreminjanje oddaljenosti asteroida od Sonca v scenariju A. Spreminjanje je periodično, kar nakazuje, da je asteroid v stabilni eliptični orbiti. Ostre doline označujejo orbito z visoko ekscentričnostjo.}
\end{figure}

V scenariju B asteroid enake mase pot začne (približno) vzporedno z Zemljino tirnico -- s hitrostjo, ki je nekoliko manjša od povprečne Zemljine tirne hitrosti. Na sliki \ref{fig:B} vidimo, da se ujame v orbito okoli Sonca. Ker sta tira asteroida in Zemlje podobna, a ne popolnoma enaka, razdalja med njima tudi nekoliko oscilira, kot to vidimo na sliki \ref{fig:B-r}. Ker je Zemlja hitrejša, se razdalja med Zemljo in asteroidom počasi zmanjšuje; po malo manj kot 4000 dneh Zemlja ujame in prehiti asteroid.

\begin{table}
    \centering
    \begin{tabular}{r|c c c}
        scenarij & $M\,[\si{kg}]$ & $\mathbf r_0\,[\si{AU}]$ & $\mathbf v_0\,[\si{km/s}]$\\
        \hline
        A & $2.86 \cdot 10^{19}$ & $2.5(\cos 20\si{\degree}, \sin 20\si{\degree}) + (-\sin 20\si{\degree}, \cos 20\si{\degree})$ & $17930(-\cos 20\si{\degree}, -\sin 20\si{\degree})$\\
        B & $2.86 \cdot 10^{19}$ & $(0.99, 0)$ & $(0, 30000)$\\
        C & $2.86 \cdot 10^{19}$ & $(4, 1)$ & $(-30000, 0)$\\
    \end{tabular}
    \caption{Začetni položaji in hitrosti asteroida.}
\end{table}

\begin{figure} \label{fig:B}
    \centering
    \includegraphics{B-300d.pdf}
    \caption{Tiri Zemlje, Lune in asteroida 300 dni po začetku scenarija. Luna je ves čas preblizu Zemlje, da bi ju lahko razločili.}
\end{figure}

\begin{figure} \label{fig:B-r}
    \centering
    \includegraphics{B-r.pdf}
    \caption{Spreminjanje oddaljenosti asteroida od Zemlje v scenariju B. Vidimo, da razdalja ves čas oscilira. Poleg tega oddaljenost pada vse do trenutka, kjer Zemlja ujame ter prehiti asteroid, takrat se razdalja začne povečevati.}
\end{figure}

Scenarij C asteroid spet enake mase začne na ravnem tiru, ki je od Sonca oddaljen $1\,\si{AU}$. Njegova hitrost je $30000\,\si{km/s}$. Sprva se asteroid podobno kot v scenariju A približa Soncu, a ne preide v eliptično orbito, temveč po hiperboličnem tiru obide Sonce in zapusti sistem.

\begin{figure} \label{fig:C}
    \includegraphics{C-400d.pdf}
    \caption{Tira Zemlje, Lune in asteroida 400 dni po začetku scenarija C. Luna je ves čas preblizu Zemlje, da bi ju lahko razločili.}
\end{figure}

\begin{figure} \label{fig:C-r}
    \includegraphics{C-r.pdf}
    \caption{Spreminjanje oddaljenosti asteroida od Sonca v scenariju C. Vidimo, da se asteroid približa Soncu, nato pa nadaljuje pot po hiperboli stran od Sonca.}
\end{figure}

\subsection{Manj verjetni scenariji}

\begin{figure} \label{fig:D}
    \centering
    \includegraphics{D-600d.pdf}
    \caption{Tira Zemlje, Lune in asteroida 600 dni po začetku scenarija D.}
\end{figure}

Poleg teles, ki bi jih tehnično označili kot asteroid, je mogoče, čeprav malo verjetno, da bi sistem preletelo tudi večje telo, kot je na primer pobegli planet. V scenariju D sistem preleti asteroid oz. pobegli planet z maso PSO J318.5--22, t. j. 6.5 mas Jupitra~\autocite{wiki-PSO}. Na sliki \ref{fig:D} vidimo, da na svoji poti Zemljo potegne iz njene orbite okoli Sonca. Prelet tudi povzroči, da Luna zapusti orbito okoli Zemlje in nadaljuje po tiru, ki se oddaljuje od Zemljinega. Asteroid nadaljuje po hiperboličnem tiru. Na sliki \ref{fig:D-closeup} od bližje vidimo, na kakšen način Zemlja in z njo Luna spremenita tir, ko asteroid leti blizu.

\begin{figure} \label{fig:D-closeup}
    \centering
    \includegraphics{D-392d.pdf}
    \caption{Bližji pogled na spremembo tira Zemlje in Lune ob preletu asteroida.}
\end{figure}

V scenariju E skozi sistem spet leti pobegli planet iste mase. Ob tem Zemljo potegne v novo orbito večje ekscentričnosti in bližje Soncu. Če je v prejšnjem scenariju življenje na zemlji zmrznilo, tokrat zgori na poti blizu Sonca. Asteroid svojo pot nadaljuje po hiperboličnem tiru. Kot to vidimo na grafu \ref{fig:E-phi}, ki prikazuje spreminjanje kota $\varphi(t)$ med Zemljo in $x$-osjo našega koordinatnega sistema, se po preletu dolžina leta na Zemlji znatno skrajša. Poleg tega Luna ni več na tiru kroži okoli Zemlje, temveč kroži okoli Sonca, vedno nekoliko za Zemljo -- to vidimo na sliki \ref{fig:E-closeup}, še lepše pa v animacijah \verb|E-1000d.avi| in \verb|E-1000d-closeup.avi|.

\begin{figure} \label{fig:E}
    \centering
    \includegraphics{E-420d.pdf}
    \caption{Tira Zemlje, Lune in asteroida 420 dni po začetku scenarija E.}
\end{figure}

\begin{figure} \label{fig:E-closeup}
    \centering
    \includegraphics{E-450d.pdf}
    \caption{Bližji pogled na tira Zemlje in Lune 450 dni po začetku scenarija E.}
\end{figure}

\begin{table}
    \centering
    \begin{tabular}{r|c c c}
        scenarij & $M\,[\si{M_\odot}]$ & $\mathbf r_0\,[\si{AU}]$ & $\mathbf v_0\,[\si{km/s}]$\\
        \hline
        D & $0.00621$ & $(-6, -1.8)$ & $(25000, 0)$\\
        E & $0.00621$ & $6(\cos 23\si{\degree}, \sin 23\si{\degree}) + 0.9(-\sin 23\si{\degree}, \cos 23\si{\degree})$ & $35000(-\cos 23\si{\degree}, -\sin 23\si{\degree})$
    \end{tabular}
    \caption{Začetni položaji in hitrosti asteroida.}
\end{table}

\begin{figure} \label{fig:E-phi}
    \centering
    \includegraphics{E-phi.pdf}
    \caption{Kot med Zemljo in $x$-osjo $\varphi(t)$ s preletom in brez preleta asteroida. Vidimo, da se po preletu čas Zemljinega obhoda znatno skrajša. Dolžina novega leta je $\sim 157$ dni.}
\end{figure}

\section{Zaključek}

Z obravnavo problema sem zadovoljen. Sistem sem spoznal in v rešitvah našel nekaj tipičnih in nekaj manj verjetnih, a bolj zanimivih scenarijev. Predlog izboljšave, ki bi bila za implementacijo preprosta, bi bilo obravnavanje problema v 3 dimenzijah. Kljub temu mislim, da trenutne rešitve odražajo dejansko fiziko in ponujajo celo nekaj točnosti.

\nocite{*}
\emergencystretch=1em
\printbibliography

\end{document}
