% !TEX program = xelatex

\documentclass{article}

% Layout and styling.
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{float}

% Locale and bibliography
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Math and physics typesetting.
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Numbers from sections.
\numberwithin{equation}{section}

% Images and Matplotlib plots (PGF backend).
\usepackage{graphicx}
\usepackage{pgf}

% Title, author and date.
\title{
    Vaja 13\\
    \large Bernoullijeva enačba
}
\author{Martin Šifrar}

\begin{document}

\maketitle

\noindent\textbf{Namen.} Določiti prostorninski tok vode z Venturijevo cevjo in ga primerjaj z direktno izmerjenim.

\section{Virtualne meritve}

\textbf{Pribor.} Rezervoar z dovodno in dvema odvodnima cevema, Venturijeva cev z dvema manometroma, steklena menzura, štoparica.

\subsection{Venturijeva cev}

Venturijeva cev ima dva preseka, večjega \SI{6.0}{mm} in manjšega \SI{12.8}{mm}. Sicer tega nismo izmerili mi, a predvidevamo, da je bila meritev opravljena s kljunastim merilom, torej ju poznamo na \(\pm\SI{0.05}{mm}\).

\subsection{Tlačne razlike}

\begin{figure}
    \centering
    \includegraphics[width=3in]{fig/sketch.png}
    \caption{Skica manometra.}
\end{figure}
Rezervoar pritrdimo v najnižjo lego in počasi odpiramo pipo, da voda iz rezervoarja začne teči skozi cev 3. Na manometru Venturijeve cevi odčitamo višino stolpcev, iz katere bomo izračunali tlačno razliko. Odčitamo jih z napako \(\pm\SI{0.05}{cm}\).
\begin{table}
    \centering
    \begin{tabular}{r|c c}
        položaj & \(h_1\,[\si{cm}]\)& \(h_2\,[\si{cm}]\)\\
        \hline
        1 & 8.20 & 5.10\\
        2 & 7.40 & 5.70\\
        3 & 7.00 & 6.20
    \end{tabular}
    \caption{Višine gladin manometra v treh položajih posode.}
\end{table}

\subsection{Pretok}

Volumski pretok nato izmerimo še direktno. Pustimo, da voda odteka v menzuro in štopamo, v koliko časa skozi cev priteče (približno) \SI{1000}{ml}.
\begin{table}
    \centering
    \begin{tabular}{r|c c c}
         & \(t\,[\si{s}]\) & &\\
        \hline
        položaj & 1. meritev & 2. meritev & 3. meritev\\
        \hline
        1 & 14.84 & 15.08 & 15.06\\
        2 & 21.92 & 21.74 & 21.54\\
        3 & 34.73 & 34.65 & 34.95
    \end{tabular}
    \caption{Časi polnjenja litrske menzure.}
\end{table}

\section{Račun}

\subsection{Izračunan pretok}

Za vodoraven segment cevi velja Bernoullijeva enačba brez členov potencialne energije
\begin{equation}
    p_1 + \frac{1}{2} \rho v_{S_1}^2 =
    p_2 + \frac{1}{2} \rho v_{S_2}^2.
\end{equation}
Ko jo prepišemo kot
\begin{equation}
    p_2 - p_1 = \frac{\rho}{2}\left(\frac{1}{S_1^2} - \frac{1}{S_2^2} \right) \Phi^2,
\end{equation}
lahko na desni strani vse razen \(\Phi^2\) označimo za faktor \(k\), saj se ta skozi naš eksperiment ne spreminja. Če je gostota vode \SI{997}{kgm^{-3}}, izračunamo, da je \(k\) \SI[exponent-product=\ensuremath{\cdot}]{5.93e11}{kgm^{-5}}. Če propagiramo napako dveh premerov, dobimo relativno napako \(\pm0.018\).

Pretok lahko zdaj izrazimo kot
\begin{equation}
    \Phi = K \sqrt{\Delta h},
\end{equation}
pri čemer je \(K\) konstanta, v katero skrijemo vse, kar ni odvisno od \(\Delta h\).
\begin{equation}
    K = \sqrt{\frac{g(\rho' - \rho_v)}{k}}.
\end{equation}
Če vemo, da je gostota živega srebra \SI{13534}{kgm^{-3}}, izračunamo, da je \(K\) \SI[exponent-product=\ensuremath{\cdot}]{4.55e-4}{m^{5/2}s^{-2}}, z relativno napako \(\pm0.009\). Nenavadno je, da enote vsebujejo \si{m^{5/2}}, a ko \(K\) to pomnožimo s korenom razdalje, dobimo naravno in primerno enoto za volumski pretok, t.j. \si{m^3s^{-1}}.
\begin{table}
    \centering
    \begin{tabular}{r|c c}
        položaj & \(\Phi\,[\si{mls^{-1}}]\) & \(\delta\Phi\)\\
        \hline
        1 & 80.15 & 0.03\\
        2 & 59.36 & 0.04\\
        3 & 40.72 & 0.07
    \end{tabular}
    \caption{Izračunani pretoki in relativne napake.}
\end{table}

\subsection{Izmerjen pretok}

Direktno izmerjeni pretok je preprosto volumen vode, deljeno s časom, v katerem je pretekel skozi cev. Iz treh meritev časa izračunamo povprečni čas in ocenimo napako. Volumen \SI{1000}{ml} zadanemo na\(\pm\SI{50}{ml}\) natančno. Izračunamo pretoke z precej velikimi napakami.

\begin{table}
    \centering
    \begin{tabular}{r|c c c c}
        položaj & \(\overline{t}\,[\si{s}]\) & \(\Delta t\,[\si{s}]\) & \(\Phi\,[\si{mls^{-1}}]\) & \(\delta\Phi\)\\
        \hline
        1 & 15.00 & 0.09 & 66.70 & 0.06\\
        2 & 21.73 & 0.19 & 46.01 & 0.06\\
        3 & 34.78 & 0.13 & 28.75 & 0.05
    \end{tabular}
    \caption{Povprečni časi, pretoki in napake.}
\end{table}

Narišemo graf. Grafa izračunanih in izmerjenih pretokov sta vzporedna, a izmerjeni pretoki oz. njihovi kvadrati ne grejo skozi izhodišče -- so zamaknjeni za konstanto. Naklon premice izmerjenih je \(K^2\),tako izračunamo, da je \(K\) \SI[exponent-product=\ensuremath{\cdot}]{4.00e-4}{m^{5/2}s^{-2}}, kar je podobno izračunanemo \(K\).

Izmerjeni rezultati se v okviru napake ne ujemajo -- sklepam, da je v meritvah neka napaka, a ker jih sam nisem opravljal, težko povem več.

\begin{figure}
    \centering
    \input{fig/plot.pgf}
    \caption{Kvadrati izračunanih in izmerjenih (zelena) pretokov v odvisnosti od \(\Delta h\).}
\end{figure}

\end{document}
