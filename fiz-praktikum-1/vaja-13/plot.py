import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({
    'pgf.texsystem': 'xelatex',
    'font.family': 'serif',
    'text.usetex': True
})
matplotlib.use('pgf')

import numpy as np
from reader import measurement_dict

msr = measurement_dict('./data')

Δh = msr['D_h']
Φ_izr, Φ = msr['Phi_izr'], msr['Phi']

K_sq, c = np.polyfit(Δh, Φ**2, 1)
print(np.sqrt(K_sq))
print(c)

fig, ax = plt.subplots(1, 1, figsize=[3, 4])

ax.scatter(1e2* Δh, (1e6 * Φ)**2, color='tab:green')
ax.plot(1e2 * Δh, 1e12 * K_sq * Δh + 1e12 * c, color='black', linestyle='--', zorder=-1)
ax.scatter(1e2 * Δh, (1e6 * Φ_izr)**2, color='tab:blue')

ax.set_xlabel(r'$\Delta h\,[cm]$')
ax.set_ylabel(r'$\Phi\,[mls^{-1}]$', labelpad=7)

fig.tight_layout()
fig.savefig('fig/plot.pgf')
