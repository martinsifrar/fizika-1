% !TEX program = xelatex

\documentclass{article}

% Layout and styling.
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{float}

% Locale and bibliography
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Math and physics typesetting.
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Numbers from sections.
\numberwithin{equation}{section}

% Images and Matplotlib plots (PGF backend).
\usepackage{graphicx}
\usepackage{pgf}

% Title, author and date.
\title{
    Vaja 22\\
    \large Viskoznost
}
\author{Martin Šifrar}

\begin{document}

\maketitle

\noindent\textbf{Namen.} Izmeriti koeficient viskoznosti dane viskozne tekočine in določi navor trenja v ležajih viskozimetra.

\begin{figure}
    \centering
    \includegraphics[width=4.5in]{fig/sketch.png}
    \caption{Skica viskozimetra z dimenzijami.}
    \label{fig:skica}
\end{figure}

\section{Virtualne meritve}

\textbf{Pribor.} Koaksialni viskozimeter, neznana tekočina, uteži, vrvica, merilnik časovnih intervalov, računalnik z merilnim vmesnikom.

\subsection{Splošni pogoji in dimenije viskozimetra}

V tabelo \ref{tab:splošno} zapišemo temperaturo, tlak in določimo vrednost gravitacijskega pospeška.
\begin{table}
    \centering
    \begin{tabular}{c c c}
        \(T_0\,[\si{\celsius}]\) & \(T\,[\si{hPa}]\) & \(g\,[\si{ms^{-2}}]\)\\
        20.7 & 1027 & 9.807
    \end{tabular}
    \caption{Začetni splošni pogoji.}
    \label{tab:splošno}
\end{table}
Pomembne dimenzije viskozimetra so označene in podane na skici \ref{fig:skica}.

\subsection{Navor trenja}

Vključimo merilnik časov in viskozimeter prosto zavrtimo v zraku. Navor trenja v ležaju viskozimeter počasi ustavi.
\begin{figure}
    \centering
    \includegraphics[width=5in]{fig/trenje.png}
    \caption{Meritev zaustavljanja prosto zavrtenega viskozimetra.}
    \label{fig:trenje}
\end{figure}

\subsection{Vztrajnosti moment}

Na gred z premerom \(\SI{53.25}{mm} \pm \SI{0.03}{mm}\) navijemo vrvico z utežjo mase \(\SI{10}{g} \pm \SI{0.5}{g}\). Vključimo merilnik časov in spustimo, da utež iz gredi odvije vrvico.

\begin{figure}
    \centering
    \includegraphics[width=5in]{fig/vztrajnostni-moment.png}
    \caption{Meritev vztrajnostnega momenta viskozimetra.}
    \label{fig:moment}
\end{figure}

\subsection{Končna hitrost v stiku s tekočino}

Ko valj potopimo v tekočino -- tako, da je v stiku z valjem \(\SI{2}{cm} \pm \SI{1}{mm}\) tekočine -- deluje nanj navor viskoznega trenja. Ta narašča s hitrostjo in po dovolj časa se pri kotni hitrosti \(\omega(\infty)\) uravnovesi s silo uteži na vrvici, ki naprej pada enakomerno. Uprabljama utež z isto maso kot pri merjenju vztrajnostnega momenta.
\begin{figure}
    \centering
    \includegraphics[width=5in]{fig/viskoznost.png}
    \caption{Meritev končne kotne hitrosti valja v stiku z viskozno tekočino.}
    \label{fig:viskoznost}
\end{figure}

\section{Račun}

\subsection{Navor trenja in vztrajnostni moment}

Navor pri meritvi \ref{fig:trenje} je
\begin{equation}
    M_tr = J\alpha_{tr},
    \label{eq:1}
\end{equation}
pri čemer je \(J\) vztrajnostni moment viskozimetra, \(\alpha_tr\) pa pojemek, t.j. negativni naklon premice skozi meritve časov, \SI{6.63}{s^{-1}}. Ocenimo (iz meritev drugega sošolca), da pri vseh meritvah vzorcev približno 50, torej je efektivna napaka približno \(\pm\SI{0.01}{s^{-1}}\).

Pri meritvi vztrajnostnega momenta \ref{fig:moment} vrtenje valja opisuje
\begin{equation}
    (J + mr_g^2)\alpha = mgr_g - M_tr,
\end{equation}
pri čemer je \(m\) masa uteži, ki vrti gred in \(r_g\) polmer gredi. Pospešek \(\alpha\) je naklon premice v meritvi \ref{fig:moment}, \SI{1.16}{s^{-1}}. Meritev ima spet približno 50 vzorcev, to pomeni efektivno napako \(\pm\SI{0.06}{s^{-1}}\). Skupaj z izrazom \ref{eq:1} lahko izrazimo vztrajnostni moment
\begin{equation}
    J = \frac{mgr_g - mr_g^2\alpha}.
\end{equation}
Polovica premera -- polmer gredi je \(\SI{53.25}{mm} \pm \SI{0.03}{mm}\). Za vztrajnosti moment izračunamo \SI{0.33}{gm^2} z relativno napako \(\pm0.06\).

Za kasneje izračunamo še vrednost \(M_tr\) -- \(2.22(1 \pm 0.06)\si{mNm}\).

\subsection{Viskoznost}

Navor viskoznega trenja je podan kot
\begin{align}
    M_{vt} &= -4 \pi\eta\omega h \frac{R_1^2 R_2^2}{R_2^2 - R_1^2}\\
    M_{vt} &= -k \eta\omega.
\end{align}
Izračnunamo, da je izpostavljen koeficient \(k\) \SI[exponent-product=\ensuremath{\cdot}]{9.28e-4}{m^3} z relativno napako \(\pm0.06\). Ko je vrtenje enakomerno, velja, da je koeficient viskoznosti
\begin{equation}
    \eta = \frac{mgr_g}{k\omega(\infty)},
\end{equation}
pri čemer enakomerno hitrost \(\omega(\infty)\) določimo iz prilagojene konstante iz meritve \ref{fig:viskoznost}. Ta je \SI{0.89}{s^{-1}} z efektivno napako \(\pm\SI{0.01}{s^{-1}}\). Končno izračunamo koeficient viskoznosti naše tekočine, \SI{3.17}{Pas} s kar veliko napako. Relativna je \(\pm0.12\), absolutna pa \SI{0.40}{Pas}.

\end{document}
