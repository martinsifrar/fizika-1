import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({
    'pgf.texsystem': 'xelatex',
    'font.family': 'serif',
    'text.usetex': True
})
matplotlib.use('pgf')

import numpy as np
from reader import measurement_dict

msr = measurement_dict('./data')

t = msr['t']
T_Fe = msr['T_Fe']
T_med = msr['T_med']
T_Al = msr['T_Al']

print(T_Fe_max := np.mean(T_Fe[600:]))
print(T_med_max := np.mean(T_med[280:600]))
print(T_Al_max := np.mean(T_Al[440:]))
print(T_Fe_min := np.mean(T_Fe[200:320]))
print(T_med_min := np.mean(T_med[:200]))
print(T_Al_min := np.mean(T_Al[:260]))

fig, ax = plt.subplots(3, 1, figsize=[3.5, 9])

ax[0].plot(t, T_Fe)
ax[0].plot(t, np.full_like(t, T_Fe_max), color='black', linestyle=':')
ax[0].plot(t, np.full_like(t, T_Fe_min), color='black', linestyle=':')
ax[0].margins(y=0.2)
ax[1].plot(t, T_med)
ax[1].plot(t, np.full_like(t, T_med_max), color='black', linestyle=':')
ax[1].plot(t, np.full_like(t, T_med_min), color='black', linestyle=':')
ax[1].margins(y=0.2)
ax[2].plot(t, T_Al)
ax[2].plot(t, np.full_like(t, T_Al_max), color='black', linestyle=':')
ax[2].plot(t, np.full_like(t, T_Al_min), color='black', linestyle=':')
ax[2].margins(y=0.2)

ax[0].set_xlabel('$t\,[s]$')
ax[0].set_ylabel('$T_{Fe}\,[C^\circ]$', labelpad=7)
ax[1].set_xlabel('$t\,[s]$')
ax[1].set_ylabel('$T_{med}\,[C^\circ]$', labelpad=7)
ax[2].set_xlabel('$t\,[s]$')
ax[2].set_ylabel('$T_{Al}\,[C^\circ]$', labelpad=7)

fig.tight_layout()
fig.savefig('fig/plot.pgf')
