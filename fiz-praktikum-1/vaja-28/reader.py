from pandas import read_csv
import os


def measurement_dict(path='./'):
    '''Returns a dict with measurements from files in current directory.
    In order that measurements are in base units, the second row of every
    measurement column is a number indicating the unit prefix.'''
    measurements = dict()

    def to_base_units(array):
        return 10**array[0] * array[1:]

    for filename in os.listdir(path):
        try:
            csv_data = read_csv(os.path.join(path, filename), comment='#')
            measurements.update(
                    {k: to_base_units(v.to_numpy())
                        for (k, v) in csv_data.iteritems()})
        except:
            print(f'Skipped `{filename}`.')
    return measurements
