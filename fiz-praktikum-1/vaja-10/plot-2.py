import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({
    'pgf.texsystem': 'xelatex',
    'font.family': 'serif',
    'text.usetex': True
})
matplotlib.use('pgf')

from matplotlib.ticker import FormatStrFormatter

import numpy as np
from reader import measurement_dict

msr = measurement_dict('./data')

t = 1e3 * msr['t']
t_model = np.linspace(np.min(t) - 0.05, np.max(t) + 0.05, num=101)

print(np.mean(t))
print(np.std(t))
print(np.std(t) / np.sqrt(t.size))

def gaussian(x, x_mean, σ):
    return (1 / (np.sqrt(2 * np.pi) * σ)) * np.exp(-(x - x_mean)**2 / (2 * σ**2))

fig, ax = plt.subplots(1, 1, figsize=[5, 3.5])

ax.hist(t, bins=10, density=True)
ax.plot(t_model, gaussian(t_model, np.mean(t), np.std(t)), color='tab:red')

ax.set_xlabel(r'$t\,[ms]$', labelpad=7)
ax.set_ylabel(r'$\omega\,[1/ms]$')

fig.tight_layout()
fig.savefig('fig/plot-2.pgf')
