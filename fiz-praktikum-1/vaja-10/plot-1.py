import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({
    'pgf.texsystem': 'xelatex',
    'font.family': 'serif',
    'text.usetex': True
})
matplotlib.use('pgf')

import numpy as np
from scipy.optimize import curve_fit
from reader import measurement_dict

msr = measurement_dict('./data')

H, h_1, h_2 = msr['H'], msr['h_1'], msr['h_2']
h = np.array([0, (H - h_1)[0], (H - h_2)[0], H[0]])
n_0, n_1 = msr['n_0'] , msr['n_1']
t_0, t_1 = (n_0 - n_0[0]) / 60, (n_1 - n_1[0]) / 60

t = np.mean(np.array([t_0, t_1]), axis=0)
t_model = np.linspace(0, 0.5)

def parabola_0(x, a):
    return a * (x)**2

popt, pcov = curve_fit(parabola_0, t, h)

print(t_0)
print(t_1)

print(g := 2 * popt[0])
print(2 * np.sqrt(np.diag(pcov))[0])

fig, ax = plt.subplots(1, 1, figsize=[4, 5])

Δt = 2 / 60
#ax.errorbar(t_1, 1e2 * h, xerr=Δt, fmt='o', color='tab:green', ecolor='black')
ax.errorbar(t_0, 1e2 * h, xerr=Δt, fmt='o', color='tab:blue', ecolor='black')
ax.plot(t_model, 1e2 * parabola_0(t_model, g) / 2, linestyle='-', color='tab:red')
ax.plot(t_model, 1e2 * parabola_0(t_model + Δt/2, 9.807) / 2, linestyle='--', color='black')

ax.set_xlabel(r'$t\,[s]$')
ax.set_ylabel(r'$h\,[cm]$', labelpad=7)

fig.tight_layout()
fig.savefig('fig/plot-1.pgf')
