% !TEX program = xelatex

\documentclass{article}

% Layout and styling.
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{float}

% Locale and bibliography
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Math and physics typesetting.
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Numbers from sections.
\numberwithin{equation}{section}

% Images and Matplotlib plots (PGF backend).
\usepackage{graphicx}
\usepackage{pgf}

% Title, author and date.
\title{
    Vaja 10\\
    \large Težni pospešek
}
\author{Martin Šifrar}

\begin{document}

\maketitle

\noindent\textbf{Namen.} Preveriti, da je prosto padanje enakomerno pospešeno, izračunati težni spospešek in preveriti, ali meritve časov res ustrezajo normalni porazdelitvi.

\section{Meritve}

\textbf{Pribor.} Plastična kocka, trakovno merilo, kamera (telefon).

\subsection{Razmike oznak}

Višina poličke, iz katere skustimo plastično kocko, je \SI{89.5}{cm}. Pod njo na steno z pastelinom pritrdimo dve leseni letvici -- eno na \SI{30}{cm} nad tlemi, drugo na \SI{60}{cm} nad tlemi. Merive opravimo s trakovnim merilom, torej na \(\pm
\SI{0.1}{cm}\) natančno.
\begin{figure}
    \centering
    \includegraphics[width=3in]{fig/H.png}
    \caption{Višina, iz katere spustimo kocko.}
\end{figure}

\subsection{Posnetek in časi}

Telefon postavimo na stol in posnamemo, kako kocko spustimo iz poličke mimo letvic. Nato posnetek uvozimo v Blender in najdemo trenutek, v katerem kocko spustimo, v katerem je gornji rob kocke najbližje gornjemu robu posamezne letvice in v katerem kocka pade na tla.
\begin{figure}
    \centering
    \includegraphics[width=4in]{fig/frames.png}
    \caption{Sličica, kjer je gornji rob kocke najbližje gornjemu robu prve letvice.}
\end{figure}
Ker so časovni razmiki v sličicah enaki (nekako pričakovano), lahko za vsak položaj izračunamo kar en čas \(t\). Posnetek ima 60 sličic na sekundo, zato število sličic delimo z \SI{60}{s^{-1}}.
\begin{table}
    \centering
    \begin{tabular}{r|c c c}
        položaj & \(n_0\) [sličic] & \(n_1\) [sličic] & \(t\,[\si{s}]\)\\
        \hline
        vrh & 870 & 1203 & 0\\
        1 & 883 & 1216 & 0.22\\
        2 & 889 & 1222 & 0.32\\
        tla & 894 & 1227 & 0.40
    \end{tabular}
    \caption{Sličice in časi položajev iz posnetka. \(n_0\) in \(n_1\) sta prva in druga meritev.}
\end{table}
Če čase razberemo na vsaj dve sličici natančno, je napaka v času \(\pm\SI{0.03}{s}\).

\section{Račun}

\subsection{Gravitacijski pospešek}

Narišemo odmike v odvisnosti od razbranih časov in skozi prilagodimo parabolo, ki gre skozi izhodišče \(at^2\). Časom dodamo tudi nepako -- napaka v razmikih je v primerjavi z njimi majhna in je ne narišemo. Ker pot od vrha opisuje
\begin{equation} \label{eq:pad}
    s = \frac{gt^2}{2},
\end{equation}
je koeficient \(a\) ravno polovica gravitacijskega pospeška \(g\). Izračunamo, da je \SI{11.45}{ms^{-1}}. Napako tako izračunanega koeficienta težje ocenimo, zato skozi graf potegnemo še parabolo oblike
\begin{equation}
    s = \frac{g_0(t - t_0)^2}{2},
\end{equation}
pri čemer je \(g_0\) tipična vrednost gravitacijskega pospeška, t.j. \SI{9.807}{ms^{-2}}. Pospešek je znotraj napake, če obstaja parabola, ki se prilega skozi intervale negotovosti, tako da je \(t_0\) manjši ali enak od napake časov. Taka parabola res obstaja, torej je naš rezultat smiselen.
\begin{figure}
    \centering
    \input{fig/plot-1.pgf}
    \caption{Graf odmikov po časih, razbranih iz posnetka in skozi prilagojena parabola (v rdeči). Vodoravne črte na podatkih označujejo napako v razbiranju časov. Zraven je s črno črtkano narisana tudi parabola z gravitacijskim pospeškom \SI{9.807}{ms^{-2}}}
\end{figure}

\section{Virtualne meritve}

\textbf{Pribor.} Elektronska ura, dve optični stikali, elektromagnet, stojalo, jeklena kroglica, dva izvira enosmerne napetosti.

\subsection{Razdalje}

\begin{figure}
    \centering
    \includegraphics[width=4in]{fig/sketch.jpg}
    \caption{Skica z označenimi pomembnimi dimenzijami.}
    \label{fig:skica}
\end{figure}
Izmerimo pomembne višine, debelino optičnih stikal in premer kroglice.
\begin{table}
    \centering
    \begin{tabular}{c c c|c|c c c|c}
        \(h_1\,[\si{cm}]\) & \(h_2\,[\si{cm}]\) & \(h_3\,[\si{cm}]\) & \(\Delta h_{12}\,[\si{cm}]\) & \(h_4\,[\si{cm}]\) & \(h_5\,[\si{cm}]\) & \(d\,[\si{cm}]\) & \(\Delta h_{45}\,[\si{cm}]\) in \(\Delta d\,[\si{cm}]\)\\
        15.95 & 32.55 & 50.50 & \(\pm0.05\) & 1.215 & 1.220 & 1.205 & \(\pm0.005\)
    \end{tabular}
    \caption{Meritve dimenzij, označenih na sliki \ref{fig:skica}}
\end{table}

\subsection{Časi}

Izmerimo čase od zaznave v prvem do zaznave v drugem optičnem stikalu. Iskreno mislim, da bi za konkretno izvedbo vaje morali meriti čase drugače, optimalno bi merili tudi več časov -- a to so meritve, ki sem jih dobil v Dropboxu.

\begin{table}
    \centering
    \begin{tabular}{c}
        \(t\,[\si{ms}]\)\\
        \hline
        78.185\\
        78.165\\
        78.194\\
        78.223\\
        78.231\\
        78.211\\
        78.302\\
        78.232\\
        78.182\\
        78.252\\
        78.130\\
        78.118\\
        78.170\\
        78.244\\
        78.150\\
        78.184\\
        78.171\\
        78.205\\
        78.193\\
        78.168\\
        78.245\\
        78.185\\
        78.220\\
        78.257\\
        78.120\\
        78.229\\
        78.162\\
        78.186\\
        78.121\\
        78.100\\
        78.220\\
        78.180\\
        78.204\\
        78.181\\
        78.139\\
        78.140\\
        78.187\\
        78.295\\
        78.189\\
        78.264\\
        78.142\\
        78.237\\
        78.164\\
        78.134\\
        78.154\\
        78.186\\
        78.245\\
        78.240\\
        78.248\\
        78.247
    \end{tabular}
    \caption{Meritve časov.}
\end{table}

\subsection{Komentar k meritvam}

Negotovost, ki jo bo razdalja doprinesla k končnemu rezultatu se z manjšimi razdaljami večja, saj se relativna napaka poveča. Če bi merili več časov, bi bilo optimalno, da se razmiki povečujejo.

\section{Račun}

\subsection{Normalna porazdelitev}

Pričakujemo, da so porazdelitev časov okoli povprečja približno ustreza normalni porazdelitvi. Interval od največjega do najmanjšega razdelimo v deset predalčkov in narišemo histogram. Da lahko zraven narišemo še krivuljo normalne razporeditve, je y-os verjetnostna gostota, ne število ali verjetnost. Porazdelitev je osredotočena okoli \SI{78.195}{ms} z standardno deviacijo \SI{0.05}{ms}, torej je efektivna napaka \(\pm\SI{0.007}{ms}\).

\begin{figure}
    \centering
    \input{fig/plot-2.pgf}
    \caption{Histogram meritev časa z 10 stolpci in pričakovana normalna porazdelitev.}
\end{figure}

\subsection{Gravitacijski pospešek}

Kroglica najprej pade do prvega optičnega stikala in opravi pot \(s_1\). Če vzamemo, da se stikali sproži na sredini (zares ne vem), je ta pot \(\SI{17.35}{cm} \pm \SI{0.11}{cm}\).
\begin{equation*}
    s_1 = h_3 - h_2 - d + \frac{h_4}{2}.
\end{equation*}
Iz \ref{eq:pad} je hitrost po tem prvem delu padca
\begin{equation}
    v_1 = \sqrt{2gs_1}
\end{equation}
Nato kroglica pade do naslednjega stikala in opravi pot \(s_2\) \(\SI{16.60}{cm} \pm \SI{0.11}{cm}\).
\begin{equation*}
    s_2 = h_2 - h_1 + \frac{h_4}{2} - \frac{h_5}{2}.
\end{equation*}
To opisuje izraz
\begin{equation}
    s_2 = v_1t + \frac{gt^2}{2},
\end{equation}
ki pa je pravzaprav kvadratna enačba, iz katere lahko izračunamo \(\sqrt{g}\).
\begin{align*}
    \frac{t^2}{2} g + t \sqrt{2s_1} \sqrt{g} - s_2 &= 0\\
    g &= 2\left(\frac{-\sqrt{s_1} + \sqrt{s_1 + s_2}}{t}\right)^2.
\end{align*}
Tako izračunamo, da je gravitacijski pospešek \SI{9.02}{ms^{-2}}. Če propagiramo napako, je relativna napaka pospeška \(0.04\). To se popolnoma ne strinja z tipično vrednostjo \SI{9.807}{ms{-2}}, tako da je bil v meritvah gotovo tudi človeška napaka.

\end{document}
