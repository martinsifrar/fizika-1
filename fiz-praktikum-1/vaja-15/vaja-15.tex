% !TEX program = xelatex

\documentclass{article}

% Layout and styling.
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{float}

% Locale and bibliography
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Math and physics typesetting.
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Numbers from sections.
\numberwithin{equation}{section}

% Images and Matplotlib plots (PGF backend).
\usepackage{graphicx}
\usepackage{pgf}

% Title, author and date.
\title{
    Vaja 15\\
    \large Težnostno nihalo
}
\author{Martin Šifrar}

\begin{document}

\maketitle

\noindent\textbf{Namen.} Z merjenjem nihajnega časa določimo težnostni pospešek \(g\) na \(\SI{0.1}{\percent}\) natančno.

\section{Meritve}

\textbf{Pribor.} Nihalo, merilec odmikov, kljunasto merilo, trakovno merilo, štoparica (telefon).

\subsection{Nihalo}

Izmerimo odmika na ravni podlagi (mizi) in na površini krogle. Merilec odmikov, ki ga uporabimo, je natančen na \(\pm\SI{0.1}{mm}\). Meritvi ponovimo na treh različnih koncih mize oz. krogle. Povprečeni vrednosti z sta \SI{0.44}{mm} in \SI{5.80}{mm}.

S kljunastim merilom na \(\pm\SI{0.05}{mm}\) natančno izmerimo tudi stranice trikotnika, ki ga oblikujejo noge merilca odmikov in jih povprečimo -- dobimo \SI{42.87}{mm}.

Nato izmerimo dolžino nihala. S trakovnim merilom izmerimo dolžino od sredine ležaja do središča uteži \(\SI{220.0}{cm} \pm \SI{0.1}{cm}\). Debelino žice izmerimo na treh točkah. Dobimo povprečje \(\SI{1.67}{} \pm \SI{0.05}{mm}\).

Izmerimo vodoravni odmik od ravnovesne lege. Izmerimo tudi višino nad temenom, na kateri smo merili vodoravni odmik. Vodoravni odmik je \(\SI{11.7}{cm} \pm \SI{0.1}{cm}\), ravnina, kjer smo ga izmerili, pa je \(\SI{2.5}{cm} \pm \SI{0.1}{cm}\) nad temenom uteži.

\subsection{Časi}

Nihalo odmaknemo od ravnovesne lege in ga spustimo. Z enim očesom opazujemo in na štoparici zabeležimo vmesni čas vsakega petega nihaja. Sam sem moral poskus ponoviti trikrat, saj sem se zmotil pri štetju oz. zgrešil gumb štoparice.

\begin{table}
    \centering
    \begin{tabular}{c|l}
        \(i\) & \(t_i [\si{s}]\)\\
        \hline
        5 & 14.82\\
        10 & 14.91\\
        15 & 14.80\\
        20 & 14.83\\
        25 & 14.90\\
        30 & 14.84\\
        35 & 14.96\\
        40 & 14.85\\
        45 & 14.89\\
        50 & 14.84\\
        55 & 14.98\\
        60 & 14.85\\
        65 & 14.78\\
        70 & 14.94\\
        75 & 14.90\\
        80 & 14.86\\
        85 & 14.88\\
        90 & 14.78\\
        95 & 14.90\\
        100 & 14.94\\
        105 & 14.85\\
        110 & 14.94\\
        115 & 14.90\\
        120 & 14.81\\
        125 & 14.86\\
        130 & 14.87\\
        135 & 14.94\\
        140 & 14.89\\
        145 & 14.85\\
        150 & 14.97\\
    \end{tabular}
    \caption{Meritve vmesnih časov -- časov prejšnjih 5 nihajev.}
\end{table}

\section{Račun}

\subsection{Radij krogle}

Radija okrogle uteži nismo neposredno izmerili, zato posežemo po nekaj trigonometrije. Konice nog merilca so oglišča enakostraničnega trikotnika s stranico \(a\). Razdalja med osjo, po kateri smo merili odmik in enim od oglišč je \(\frac{2}{3}\) višine trikotnika, torej \(\frac{a}{\sqrt{3}}\). Zapišemo Pitagorov izrek, pri čemer je \(h\) razlika \(h_1\) in \(h_0\)
\begin{equation*}
    \left(
        (r) - h
    \right)^2 +
    \frac{a^2}{3} = r^2,
\end{equation*}
izrazimo \(r\)
\begin{equation*}
    r = \frac{a^2 + 3h^2}{6h},
\end{equation*}
ter izračunamo, da je radij uteži \(\SI{5.98}{cm}\).

\subsection{Začetni odmik}

Začetni kot \(\alpha\) izračunamo iz meritve vodoravnega odmika \(o\). Velja:
\begin{equation*}
    \sin\alpha = \frac{o}{l - r - h'},
\end{equation*}
pri čemer je \(h'\) razdalja med temenom uteži in nosilko odmika. Ker je kot majhen, lahko za približek sinusa vzamemo kar kot v radianih. Člen, ki ga bomo kasneje uporabili v popravku, je
\begin{equation}
    \frac{1}{2} \sin^2\frac{\alpha}{2} =
    \frac{1}{8}
    \left(
        \frac{o}{l - r - h'}
    \right)^2.
\end{equation}
Zanj izračunamo vrednost \(0.0004\).

\subsection{Nihajni časi}

Med nihaji smo merili vmesne čase. Da zmanjšamo vpliv napake nenatančnega merjenja časa, izračunamo skupne čase 100 nihajev in jih povprečimo.
\begin{table}
    \centering
    \begin{tabular}{c|l l}
        \(i\) & \(\Sigma_t [\si{s}]\) & \(T_i [\si{s}]\)\\
        \hline
        5 & 297.45 & 2.97\\
        10 & 297.48 & 2.97\\
        15 & 297.51 & 2.98\\
        20 & 297.61 & 2.98\\
        25 & 297.59 & 2.98\\
        30 & 297.55 & 2.98\\
        35 & 297.58 & 2.98\\
        40 & 297.56 & 2.98\\
        45 & 297.60 & 2.98\\
        50 & 297.56 & 2.98
    \end{tabular}
    \caption{Vsota in povprečje časov od \(t_i\) do \(t_{i + 100}\).}
\end{table}
Varianca tako pridobljenih period je \(\SI{0.0005}{s}\). Uporabljamo 10  meritev, torej je efektivna napaka izmerjenega nihajnega časa \(\SI{0.0002}{s}\).

Ker statistike še popolnoma ne razumemo, lahko o napaki razmislimo nekoliko bolj praktično. Ker se nihalo ne ustavi, se napake v merjenju časa znotraj posameznih \(\Sigma_t\) ne seštevajo. Tipičen reakcijski čas za preproste odzive je okoli \SI{100}{ms}. Recimo, da se med eno in drugo meritvijo celoten čas, v katerem opravimo meritev, spreminja za desetino reakcijskega časi, torej \(\pm\SI{10}{ms}\). Ta napaka nastopa na koncu in začetku \(\backsim\SI{297}{s}\) dolge meritve -- tako je relativna napaka \(\pm0.00007\). Napaka posameznega izračunanega nihajnega časa je torej \(\pm\SI{0.0002}{s}\). A to je le zelo groba ocena -- vrednost \(\pm\SI{10}{ms}\) je bila namreč izbrana precej arbitrarno.

\subsection{Nihalo pri majhnih odmikih}

Perioda matematičnega nihala pri majhnih odmikih je
\begin{equation} \label{eq:approx}
    T =
    2\pi\sqrt{\frac{l}{g}}
\end{equation}

Če izmerimo dolžino nihala in nihajne čase, lahko določimo težnostni pospešek \(g\). Ker nihalo ni matematično, moramo za večjo natančnost upoštevati še nekaj popravkov. Poskusno lahko \(g\) poračunamo že zdaj. Iz \ref{eq:approx} izrazimo \(g\)
\begin{equation*}
    g =
    \left(
        \frac{2\pi}{T}
    \right)^2 l
\end{equation*}
ter za pospešek dobimo \SI{9.810}{m.s^{-2}}; z relativno napako \(\pm0.0008\). V poštev torej pridejo le popravki, ki so večji ali podobno veliki tej napaki.

\subsection{Nihalo pri večjih odmikih}

Začetni odmik našega nihala je sicer majhen, a pri želeni natančnosti še vedno dovolj velik, da uporabimo natančnejšo rešitev za matematično nihalo. Dovolj sta prva dva člena:
\begin{equation*}
    g =
    \left(
        1 + \left(\frac{1}{2}\right)^2 \sin^2\frac{\alpha}{2}
    \right)^2
    \left(
        \frac{2\pi}{T}
    \right)^2 l.
\end{equation*}
Zapišemo popravni faktor. Ker je blizu 1, ga kvadriramo po binomskem približku
\begin{equation}
    \beta_1 =
    \left(
        1 + \frac{1}{2} \sin^2\frac{\alpha}{2}
    \right).
\end{equation}

\subsection{Vztrajnostni moment}

Poleg tega utež na nihalu, ki ga obravnavamo, ni točkasta -- tudi krogla sama ima vzrajnostni moment. Enačbo \ref{eq:approx} preoblikujemo v
\begin{equation} \label{eq:moment}
    T = 2\pi\sqrt{\frac{J}{mgl^*}},
\end{equation}
nato pa vztrajnostne momente točkaste mase, krogle in valja (žice) seštejemo po Steinerjevemu izreku:
\begin{equation*}
    J = m_kl^2 + \frac{2}{5}m_kr^2 + \frac{1}{3}m_z(l - r)^2.
\end{equation*}
Vztrajnostni moment in \(ml^*\) -- izraz za težišče nihala, pomnožen s skupno maso -- vstavimo v \ref{eq:moment} in ven izrazimo \(g\)
\begin{align*}
    g
    &=
    \frac{1}{m_kl + m_z\frac{l - r}{2}}
    \left(
        m_kl + \frac{2m_kr^2}{5l} + \frac{m_z(l - r)^2}{3l}
    \right)
    \left(
        \frac{2\pi}{T}
    \right)^2 l \approx\\
    \intertext{Ker sta oba faktorja blizu 1, uporabimo binomski približek:}
    &\approx
    \left(
        1 - \frac{m_z}{2m_k} + \frac{m_zr}{2m_kl} 
    \right)
    \left(
        1 + \frac{2r^2}{5l^2} + \frac{m_z}{3m_k} - \frac{2m_zr}{3m_kl} + \frac{m_zr^2}{3m_kl^2}
    \right)
    \left(
        \frac{2\pi}{T}
    \right)^2 l \approx\\
\end{align*}
Še enkrat izkoristimo, da sta oba faktorja blizu 1. Prav tako se znebimo členov, ki so pod našo natančnostjo:
\begin{equation*}
    \approx
    \left(
        1 + \frac{2r^2}{5l^2} - \frac{m_z}{6m_k}
    \right)
    \left(
        \frac{2\pi}{T}
    \right)^2 l
\end{equation*}
Ker sta materiala uteži in žice dovolj podobna, za razmerje mas vzamemo kar razmerje volumnov, torej je popravni faktor
\begin{equation}
    \beta_2 =
    \left(
        1 + \frac{2r^2}{5l^2} - \frac{d^2(l - r)}{32r^3}
    \right).
\end{equation}

\subsection{Izračun pospeška}
Ko poenostavimo (oz. ocenimo) produkt popravnih faktorjev, dobimo končni izraz za popravljen pospešek \(g\)
\begin{equation}
    g =
    \left(
        1 + \frac{1}{2} \sin^2\frac{\alpha}{2} + \frac{2r^2}{5l^2} - \frac{d^2(l - r)}{32r^3}
    \right)
    \left(
        \frac{2\pi}{T}
    \right)^2 l.
\end{equation}
Izračunamo, da je težnostni pospešek \(9.816(1 \pm 0.0008)\,\si{m.s^{-2}}\). Njegova prava vrednost je nekje med \SI{9.808}{m.s^{-2}} in \SI{9.824}{m.s^{-2}}.

\end{document}
