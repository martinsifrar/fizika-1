% !TEX program = xelatex

\documentclass{article}

% Layout and styling.
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{float}

% Locale and bibliography
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Math and physics typesetting.
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Numbers from sections.
\numberwithin{equation}{section}

% Images and Matplotlib plots (PGF backend).
\usepackage{graphicx}
\usepackage{pgf}

% Title, author and date.
\title{
    Vaja 32\\
    \large Sklopljeno nihanje
}
\author{Martin Šifrar}

\begin{document}

\maketitle

\noindent\textbf{Namen.} Izmeriti in izračunati lastne krožne frekvence \(\omega_0\) \(\omega_1\) ter \(\omega'\) in \(\omega_u\). Določi koeficient vzmeti, izračunaj \(D'\) in faktor sklopitve

\begin{equation}
    \label{eq:faktor}
    K = \frac{D'}{D + D'}.
\end{equation}

\section{Virtualne meritve}

\textbf{Pribor.} Nihali na stojalu, vzmeti za sklopitev, merilo za določevanje koeficient vzmeti, centimetrsko merilo, kljunasto merilo, tehtnica, uteži, štoparica (telefon).

\subsection{Nihali}

Izmerimo dolžino \(d_0\) od vpetja do težišča. Na \(\pm\SI{0.1}{cm}\) izmerimo \SI{79.3}{cm}. Masa nihala je \(\SI{1268}{g} \pm \SI{20}{g}\).

\subsection{Koeficent vzmeti}

Opravimo meritve raztezkov vzmeti, obremenjene z znanimi masami.
\begin{table}
    \centering
    \begin{tabular}{c c c}
        \(m\,[\si{g}]\) & \(x\,[\si{cm}]\)\\
        \hline
        0 & 79.8\\
        50 & 81.7\\
        100 & 83.7\\
    \end{tabular}
    \caption{Meritve raztezkov vzmeti.}
\end{table} 
Za lažje računanje v nadaljevanju koeficient vzmeti izračunamo kar zdaj. Povprečimo koeficente, izračunane iz raztegov in njihovih pripadajočih mas, pomnoženih z gravitacijskim pospeškom \SI{9.81}{ms^{-1}}. Povprečimo jih in dobimo koeficient \SI{4.8}{Nm^{-1}} z relativno napako \(\pm0.03\).

\subsection{Nihanji časi}

Najprej meritve opravimo z nesklopljenimi nihali. Prvo nihalo 20 nihajev opravi v \SI{37.19}{s}, drugo pa v \SI{37.25}{s}.

Nihali nato sklopimo z vzmetjo na \SI{12}{cm} od osi nihala. Nihali enako odklonimo v isto smer in izmerimo čas 30 nihajev za eno in drugo nihalo. Enako ponovimo z nasprotnima začetnima odklona, nato pa izmerimo še čas 15 nihajov tako, da v začetku odklonimo le eno od nihal.
\begin{table}
    \centering
    \begin{tabular}{c|c c c c}
        nihalo & \(30t_+\,[\si{s}]\) & \(30t_-\,[\si{s}]\) & \(15t'\,[\si{s}]\) & \(4T\,[\si{s}]\)\\
        \hline
        1 & 55.88 & 50.77 & 25.54 & 74.57\\
        1 & 55.65 & 50.62 & 25.34 & 74.55\\
        2 & 55.78 & 50.63 & 25.67 & 74.12\\
        2 & 55.52 & 50.66 & 25.64 & 75.28
    \end{tabular}
    \caption{Meritve 30 nihajnih časov za nihali v prvi in drugi lastni frekvenci, 15 nihajnih časov v načinu utripanju ter časi 4 mirovanj posameznega nihala.}
    \label{tab:meritve}
\end{table}

\section{Račun}

\subsection{Nihajni časi}

Iz meritev 20 nihajev nesklopljenega nihala izračunamo nihajni čas \SI{1.86}{s}. Ker sta meritvi samo dve, napako ocenimo kot polovico razlike med njima, torej \(\pm\SI{0.03}{s}\).

Iz tabele \ref{tab:meritve} izračunamo prvi nihajni čas \(\SI{1.86}{s} \pm \SI{0.005}{s}\) in drugi nihajni čas \(\SI{1.69}{s} \pm \SI{0.003}{s}\). Pripadajoči frekvenci sta \(3.38(1 \pm 0.003)\,\si{s^{-1}}\) in \(3.72(1 \pm 0.002)\,\si{s^{-1}}\).

Nihajni čas \(t'\) je \(\SI{1.70}{s} \pm \SI{0.007}{s}\), pripadajoča frekvenca pa \(3.69(1 \pm 0.004)\,\si{s^{-1}}\). Čas utripanja je \(\SI{18.66}{s} \pm \SI{0.12}{s}\), frekvenca utripanja pa \(0.084(1 \pm 0.002)\,\si{s^{-1}}\).

\subsection{Izračunani nihajni časi}

Iz dimenzij nihala izračunamo pričakovani lastni frekvenci. Prva je
\begin{equation}
    \omega_+ = \sqrt{\frac{D}{J}},
\end{equation}
pri čemer je \(D = mgd_0\) in vztrajnostni moment \(J = md_0^2\). Slednje sicer ni res, nihalo ima tudi lastni vzrajnostni moment. To se bo odražalo tudi v tem, da so vsi izračunani nihajni časi nekoliko krajši od izmerjenih, a ker nikjer nisem našel podatkov o dejanskih dimenzijah nihala, ne morem pomagati, zato torej nadaljujmo. Drugo krožno frekvenco izrazimo kot
\begin{equation}
    \omega_- = \sqrt{\frac{D + 2D'}{J}},
\end{equation}
pri čemer je \(D' = kd^2\). Tako izračunamo prvo frekvenco \(3.52(1 \pm 0.018)\,\si{s^{-1}}\) in drugo frekvenco \(3.64(1 \pm 0.018)\,\si{s^{-1}}\). Iz tega izračunamo nihajna časa \(1.79(1 \pm 0.018)
,\si{s}\) in \(1.72(1 \pm 0.018)\,\si{s}\).

Nihajni čas \(t'\) dobimo po izrazu
\begin{equation*}
    t' = \frac{2t_+t_-}{t_+ + t_-}.
\end{equation*}
Izračunamo, da je \(1.75(1 \pm 0.054)\,\si{s}\), frekvenca pa \SI{3.58}{s^{-1}}, seveda z isto relativno napako.

Čas utripanja je
\begin{equation*}
    T = \frac{1}{\frac{1}{t_-} - \frac{1}{t_+}}.
\end{equation*}
Izračunamo, da je \(49.5(1 \pm 1.03)\,\si{s}\), kar je neuporaben rezultat.

\subsection{Faktor sklopitve}

Izraz za faktor sklopitve \ref{eq:faktor} lahko zapišemo kot
\begin{equation}
    K = \frac{1 - \left(\frac{t_-}{t_+}\right)^2}{1 + \left(\frac{t_-}{t_+}\right)^2},
\end{equation}
in tako iz izmerjenih nihajnih časov izračunamo faktor \(0.095(1 \pm 0.05)\). Iz izračunanih nihajnih časov izračunamo faktor \(0.035(1 \pm 1.01)\), kar spet ni ravno upraben rezultat.

\end{document}
