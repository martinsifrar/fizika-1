% !TEX program = xelatex

\documentclass{article}

% Layout and styling.
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{float}

% Locale and bibliography
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Math and physics typesetting.
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Numbers from sections.
\numberwithin{equation}{section}

% Images and Matplotlib plots (PGF backend).
\usepackage{graphicx}
\usepackage{pgf}

% Title, author and date.
\title{
    Vaja 16\\
    \large Vztrajnostni moment
}
\author{Martin Šifrar}

\begin{document}

\maketitle

\noindent\textbf{Namen.} Preveriti, da je vrtenje pri konstantnem navoru enakomerno pespešeno. Določiti želimo vztrajnostni moment praznega kolesa in kolesa z manjšimi kolesi -- vpetimi najprej togo in nato prosto vrtljivo. Iz ugotovitev preverimo še veljavnost izreka o kinetični energiji.

\begin{figure}[h]
    \centering
    \includegraphics[width=2.5in]{fig/sketch.jpg}
    \caption{Skica vztrajnika, s katerega se odvija utež.}
\end{figure}
\section{Virtualne meritve}

\textbf{Pribor.} Kolo z jermenom, 2 para manjših koles, uteži, vrvica, optična vrata, računalnik z vmesnikom za zapisovanje meritev.

\subsection{Vztrajnik}

Najprej izmerimo premer gredi, iz katere se odvija utež. To razberemo iz fotografije oz. posnetka, ki na žalost ni preveč oster. Razberemo, da je premer \SI{1.9}{cm}, z napako (vsaj) \(\pm\SI{1}{mm}\). Relativna napaka je torej ponosnih \(\pm0.05\).

\begin{figure}
    \centering
    \includegraphics[width=3in]{fig/1-premer-gredi.png}
    \caption{Slika je nekoliko zamegljena, zato je napaka večja.}
\end{figure}

Uteži, ki jih v meritvah uporabljamo, so težke \SI{50}{g}. Za napako vzamemo \(\pm\SI{1}{g}\), torej \(\pm0.02\).

\subsection{Meritve časov}

Edini podatki, do katerih imam dostop, so posnetki zaslona iz programa Logger Pro. V računu opisal tudi kako pridemo do vrednosti, ki so na slikah že izračunane.

\section{Račun}

\subsection{Vztrajnik brez koles}

Prvo si ogledamo meritve z eno utežjo. Kot pričakovano, odmik (kot) izgleda kot parabola, hitrost premica in pospešek konstantna funkcija -- seveda s primerno več šuma, ki je posledica numeričnega odvajanja.
\begin{figure}
    \centering
    \includegraphics[width=5in]{fig/3-1-utez.png}
    \caption{Ena od meritev z eno utežjo.}
    \label{fig:1-utez}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=5in]{fig/4-2-utezi.png}
    \caption{Ena od meritev z dvema utežema.}
\end{figure}
Ker je drugi numerični odvod v večji meri šum, pospešek izračunamo tako, da grafu hitrosti prilagodimo premico -- pospešek je njen naklon. Kot poprečje treh meritev dobimo pospešek \SI{0.82}{s^{-2}}. Da ocenimo napako, imamo iz vsake slike na voljo koren povprečja kvadratov odmikov, ta je \SI{0.15}{s^{-2}}. Po oceni, da je v vsaki meritvi okoli 100 vzorcev, to napako delimo z \(10\) in dobimo efektivno napake \(\pm0.015\).
\begin{table}
    \centering
    \begin{tabular}{c|l l}
         & \(\alpha_1 [\si{s^{-2}}]\) & \(\alpha_2 [\si{s^{-2}}]\)\\ 
        \hline
        1. & 0.8223 & 1.717\\
        2. & 0.8189 & 1.711\\
        3. & 0.8100 & 1.706
    \end{tabular}
    \caption{Nakloni premice pri eni in pri dveh utežeh.} 
\end{table}
Pospešek je koeficient spremembe hitrosti \(v\) in dolžine časovnega intervala \(t\). V najslabšem možnem primeru je pospešek naklon skozi točko, ki je zamaknjena za efektivno napako \(\Delta v\) navzdol in točko, ki je zamaknjena za \(\Delta v\) navzgor. Naklon skozi ti dve točki je koeficient \(v + 2\Delta v\) in \(t\). Napako pospeška torej ocenimo kot \(\frac{2\Delta v}{t}\) in dobimo \(\pm\SI{0.003}{s^{-2}}\).

Iz pospeška \(\alpha\) zdaj lahko izračunamo vztrajnostni moment vztrajnika \(J\) po Newtonovem zakonu za vrtenje
\begin{equation}
    mg\frac{d}{2} = J\alpha,
\end{equation}
pri čimer je \(m\) masa uteži in \(d\) premer gredi. Vztrajnostni moment je torej
\begin{equation}
    J = \frac{mgd}{2\alpha}.
\end{equation}

Za vztrajnostni moment izračunamo \SI{5.70}{gm^2} in relativno napako \(\pm0.07\). Če vse z napakami ponovimo še za meritve s dvemi utežmi, dobimo \(5.44(1 \pm 0.07)\,\si{gm^2}\) -- vztrajnostna momenta se znotraj naše natančnosti skladata.

\subsection{Vztrajnik s togo vpetimi kolesi}

\begin{figure}
    \centering
    \includegraphics[width=5in]{fig/5-1-utez-kolesa-toga.png}
    \caption{Ena od meritev s togo vpetimi kolesi in eno utežjo.}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=5in]{fig/6-2-utezi-kolesa-toga.png}
    \caption{Ena od meritev s togo vpetimi kolesi in dvema utežema.}
\end{figure}
Celoten račun z napakami ponovimo z meritvami s togo vpetimi kolesi. Dobimo \(9.21(1 \pm 0.07)\,\si{gm^2}\) za eno in \(8.81(1 \pm 0.07\,\si{gm^2})\).
\begin{table}
    \centering
    \begin{tabular}{c|l l}
         & \(\alpha_1 [\si{s^{-2}}]\) & \(\alpha_2 [\si{s^{-2}}]\)\\ 
        \hline
        1. & 0.5139 & 1.085\\
        2. & 0.4990 & 1.045\\
        3. & 0.5045 & 1.044
    \end{tabular}
    \caption{Nakloni premice pri eni in pri dveh utežeh.} 
\end{table}

\subsection{Vztrajnik z vrtljivo vpetimi kolesi}

\begin{figure}
    \centering
    \includegraphics[width=5in]{fig/7-1-utez-kolesa-vrtljiva.png}
    \caption{Ena od meritev z wrtljivo vpetimi kolesi in eno utežjo.}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=5in]{fig/8-2-utezi-kolesa-vrtljiva.png}
    \caption{Ena od meritev z vrtljivimi vpetimi kolesi in dvema utežema.}
\end{figure}
Spet ponovimo račun z meritvami z gibljivo vpetimi kolesi. Dobimo \(9.07(1 \pm 0.07)\,\si{gm^2}\) za eno in \(8.71(1 \pm 0.07\,\si{gm^2})\).
\begin{table}
    \centering
    \begin{tabular}{c|l l}
         & \(\alpha_1 [\si{s^{-2}}]\) & \(\alpha_2 [\si{s^{-2}}]\)\\ 
        \hline
        1. & 0.5072 & 1.058\\
        2. & 0.5072 & 1.090\\
        3. & 0.5072 & 1.061
     \end{tabular}
     \caption{Nakloni premice pri eni in pri dveh utežeh.} 
\end{table}

\subsection{Ohranitev energije}

Preveriti moramo, da se začetna potencialna energija uteži od končne energije razlikuje za delo sile oz. navora trenja\footnote{Pri natančnosti, s katero lahko iz grafov razberemo \(\varphi\) in \(\omega\), je navor trenja pravzaprav zanemarljiv.}. Zapišemo torej energijski zakon
\begin{equation}
    mgh - M_t\varphi = \frac{J\omega^2}{2} + \frac{mv^2}{2}.
\end{equation}
Ker sta položaj uteži in kot vztrajnika \(\varphi\) sorazmerna z polmerom gredi, to prepišemo kot
\begin{equation}
    (mgd - M_t)\varphi =
    \left(
        J + \frac{md}{4}
    \right)\omega^2.
    \label{eq:energija}
\end{equation}
Preden to enakost preverimo, potrebujemo še navor trenja. Izračunamo ga iz meritve z vztrajnikom, ki ga brez uteži zavrtimo in pustimo, da se ustavi. Iz pojemka \(0.052(1 \pm 0.001)\,\si{s^{-2}}\) izračunamo navor sile trenja \(0.30(1 \pm 0.09)\,\si{mNm}\).

Obrat \(\varphi\) in kotno hitrost \(\omega\) razberemo iz slike \ref{fig:1-utez}. Pri 7 sekundi je kot \(24\) in kotna hitrost \SI{6}{s^{-1}}. Relativna razlika med levo in desno stranjo \ref{eq:energija} je \(\sim0.01\), kar je pod relativno napako \(J\), torej gotovo znotraj intervala negotovosti celotne enačbe.
\begin{figure}
    \centering
    \includegraphics[width=5in]{fig/2-brez-utezi.png}
    \caption{Meritev brez uteži.}
\end{figure}
\% Locale and bibliography
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}
end{document}
