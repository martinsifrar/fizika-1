% !TEX program = xelatex

\documentclass{article}

% Layout and styling.
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{float}

% Locale and bibliography
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Math and physics typesetting.
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Numbers from sections.
\numberwithin{equation}{section}

% Images and Matplotlib plots (PGF backend).
\usepackage{graphicx}
\usepackage{pgf}

% Title, author and date.
\title{
    Vaja 31\\
    \large Torzijsko nihalo
}
\author{Martin Šifrar}

\DeclareMathOperator{\arctantwo}{arctan2}

\begin{document}

\maketitle

\noindent\textbf{Namen.} Izmeriti in izračunati resonančno krivuljo pri dveh različnih dušenjih.

\section{Virtualne meritve}

\textbf{Pribor.} Torzijsko nihalo, elektromotor z vzvodom, štoparica.

\subsection{Nihajni časi}

Opazujemo nihalo brez magneta, duši ga torej lastno trenje. Posnetek uvozimo v Blender in na sličico natančno razberemo čas, v katerem nihalo opravi 5 nihajev. Z orodji v programu razberemo tudi kot, za katerega je glava nihala odmaknjena pred in po tem času. Oboje meritev opravimo za nihaje v desno in nihaje v levo.
\begin{table}[h]
    \centering
    \begin{tabular}{r|c c c c}
         & \(5t_0\) & \(t_0\,[\si{s}]\) & \(A_0\,[\si{\degree}]\) & \(A_5\,[\si{\degree}]\)\\
        \hline
        v levo & 374 sličic & 2.49 & 105.9 & 23.4\\
        v desno & 375 sličic & 2.50 & 106.0 & 21.5
    \end{tabular}
    \caption{Meritve petih nihajnih časov in začetnih ter končnih amplitud.}
\end{table}
Meritve časov iz posnetka so precej natančne. Iz njih lahko izračunamo nihajne čase, a zato potrebujemo še hitrost video, ta je 30 sličic na sekundo. Če smatramo, da smo bili v meritvah posnetka natančni na tri sličice, je nihajni čas \(\SI{2.50}{s} \pm {0.02}{s}\). Meritev amplitud vsebuje nekoliko večjo napako, predvsem zaradi perspektive, v kateri je nihalo posneto.
\begin{figure}
    \centering
    \includegraphics[width=4in]{fig/blender.png}
    \caption{Posnetek zaslona v programu, v katerem na posnetku opravljamo meritve. Za amplitude najprej najdemo ravnovesni kot in nato merimo od tam.}
\end{figure}

\subsection{Resonančna krivulja}

Zdaj nihalo vzbujamo. Pri različnih frekvencah iz posnetka na enak način kot prej razberemo amplitudo vzbujenega nihanja.
\begin{table}
    \centering
    \begin{tabular}{c c}
        \(\nu\,[\si{Hz}]\) & \(B\,[\si{\degree}]\)\\
        \hline
        0.1 & 4.0\\
        0.2 & 8.0\\
        0.3 & 14.7\\
        0.4 & 34.8\\
        0.42 & 37.5\\
        0.43 & 30.8\\
        0.44 & 26.6\\
        0.46 & 24.1\\
        0.5 & 14.7\\
        0.7 & 7.8\\
        1.0 & 5.1
    \end{tabular}
    \caption{Meritve raztezkov vzmeti.}
\end{table}

\subsection{Fazni zamik}

Pri frekvencah vzbujanja, manjših od lastne, je fazni zamik majhen, kar pomeni, da se nihalo in vzvod sočasno premikata v isto smer. pri resonanci nihalo zaostaja ravno četrt nihaja oz. pol nihaja v eno smer. Pri frekvencah nad lastno pa nihalo začne zaostajali pol nihaja za vzbujanjem.

\section{Račun}

\subsection{Dušenje}

Gibanje nevzbujenega nihala gibanje opisuje
\begin{equation}
    \varphi = A_0e^{-\beta t}\sin(\omega_d t).
\end{equation}
Zanj nihanje poznamo nihajni čas \(\omega_d\), torej je krožna frekvenca \(\omega_d\) \SI{2.52}{s^{-1}}. Izračunamo lahko tudi koeficient dušenja, ki ga dobimo iz upadanja amplitude
\begin{equation}
    \beta = \frac{\omega_d}{10\pi}
    \ln\left(
        \frac{A_0}{A_5}
    \right).
\end{equation}
Iz amplitud \(A_0\) in \(A_5\) pri meritvah v levo in desno izračunamo dve vrednosti \(\beta\), njuno povprečje je \SI{0.124}{s^{-1}}, za napako pa vzamemo kar njuno razliko, t.j. \(\pm\SI{0.007}{s^{-1}}\).

Za lastno frekvenco nihala velja zveza
\begin{equation}
    \omega_0^2 = \omega_d^2 + \beta^2,
\end{equation}
in na \(\pm\SI{0.02}{s^{-1}}\) natančno izračunamo, da je \SI{2.52}{s^{-1}}.

\section{Resonančna krivulja}

Gibanje vzbujenega nihala gibanje opisuje
\begin{equation}
    \varphi = B\sin(\omega t + \delta).
\end{equation}
Pri tem se amplituda \(B\) spreminja po izrazu
\begin{equation}\label{eq:amplituda}
    B = \frac{B_0}{\sqrt{\left[1 - (\omega/\omega_0)^2\right]^2 + a^2(\omega/\omega_0)^2}},
\end{equation}
pri čemer je \(a\)
\begin{equation}
    a = \frac{2\beta}{\omega_0}.
\end{equation}
Izračunamo, da je \(a\) \(0.098(1 \pm 0.06)\). Po izrazu \ref{eq:amplituda} je amplituda v resonanci enaka
\begin{equation}
    B_r = \frac{B_0}{a},
\end{equation}
iz česar lahko iz resonančne amplitude izračunamo \(B_0\). Izračunamo, da je \(3.70(1 \pm 0.06)\,\si{\degree}\) in s tem lahko narišemo resonančno krivuljo. Meritve amplitud se približno prilegajo predvideni krivulji, a napaka je precej velika. Predvidevam, da je največji vir napake težavnost razbiranja iz posnetka ter to, da se vzbujeno nihanje ni umirilo.

Fazni zamik med vzbujalnim navorom in nihanjem je
\begin{equation}
    \delta = \arctantwo\left(
        a\frac{\omega}{\omega_0},
        1 -
        \left(
            \frac{\omega}{\omega_0}
        \right)^2
    \right),
\end{equation}
pri čemer je \(\arctantwo\) funkcija, ki vrne kot med \(\pi\) in \(-\pi\) glede na predznak obeh argumentov (t.j. v katerem kvadrantu se kot nahaja). Graf, ki ga narišemo, se ujema z opazanji.

\begin{figure}
    \centering
    \input{fig/plot.pgf}
    \caption{Amplitude pri različnih frekvencah vzbujanja, fazni zamik \(delta\) in povprečna moč vzbujanja, deljena z navorom \(M_0\). Graf amplitud za dvakrat večji koeficient dušenja (v zelenem) je normaliziran z istim \(B_0\). Pri faznem zamiku in moči vbujanja je dvojni koeficient dušenja v rdeči.}
\end{figure}

\end{document}
