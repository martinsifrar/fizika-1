import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({
    'pgf.texsystem': 'xelatex',
    'font.family': 'serif',
    'text.usetex': True
})
matplotlib.use('pgf')

import numpy as np
from reader import measurement_dict

msr = measurement_dict('./data')

ω, ω_0 = 2*np.pi * msr['nu'], 2.521
B, B_0 = msr['B'], 3.702

ω_model = np.linspace(np.min(ω), np.max(ω), num=501)

def B_model(ω, β):
    a = 2*β / ω_0
    return B_0 / np.sqrt(
        (1 - (ω/ω_0)**2)**2 + a**2 * (ω/ω_0)**2)

def δ_model(ω, β):
    a = 2*β / ω_0
    return np.arctan2(a*(ω / ω_0), 1 - (ω / ω_0)**2)

def P_mean_M_0_normalized(ω, β):
    return ω/2 * B_model(ω, β) * np.sin(δ_model(ω, β))

fig, ax = plt.subplots(3, 1, figsize=[4, 9])

ax[0].plot(ω_model / ω_0, B_model(ω_model, 2*0.124) / B_0, color='tab:green')
ax[0].plot(ω_model / ω_0, B_model(ω_model, 0.124) / B_0, color='tab:blue')
ax[0].scatter(ω / ω_0, B / B_0, marker='x')

ax[1].plot(ω_model / ω_0, np.degrees(δ_model(ω_model, 0.124)), color='black')
ax[1].plot(ω_model / ω_0, np.degrees(δ_model(ω_model, 2*0.124)), color='tab:red')

ax[2].plot(ω_model, P_mean_M_0_normalized(ω_model, 0.124), color='black')
ax[2].plot(ω_model, P_mean_M_0_normalized(ω_model, 2*0.124), color='red')

ax[0].set_xlabel(r'$\omega/\omega_0$')
ax[0].set_ylabel(r'$B/B_0$', labelpad=7)

ax[1].set_xlabel(r'$\omega/\omega_0$')
ax[1].set_ylabel(r'$\delta\,[^\circ]$', labelpad=7)

ax[2].set_xlabel(r'$\omega\,[s^{-1}]$')
ax[2].set_ylabel(r'$\overline{P}/M_0\,[s^{-1}]$', labelpad=7)

fig.tight_layout()
fig.savefig('fig/plot.pgf')
