% !TEX program = xelatex

\documentclass{article}

% Layout and styling.
\usepackage[a4paper, margin=1in]{geometry}
\usepackage{float}

% Locale and bibliography
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

% Math and physics typesetting.
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{siunitx}

% Numbers from sections.
\numberwithin{equation}{section}

% Images and Matplotlib plots (PGF backend).
\usepackage{graphicx}
\usepackage{pgf}

% Title, author and date.
\title{
    Vaja 20\\
    \large Prožnostni modul
}
\author{Martin Šifrar}

\begin{document}

\maketitle

\noindent\textbf{Namen.} Določiti prožnostni modul in mejo natezne trdnosti za bakreno in jekleno žico.

\section{Virtualne meritve}

\textbf{Pribor.} Pomični merili, priviti na zid, bakrena in jeklena žica, podstavek za uteži in uteži, mikrometer.

\subsection{Uteži}

Masa podstavka uteži je \SI{60}{g}, masa posameznih uteži pa \SI{100}{g}. Maso poznamo na \(\pm\SI{1}{g}\) natančno.

\subsection{Žica}

\begin{figure}
    \centering
    \includegraphics[width=3in]{fig/micrometer.png}
    \caption{Meritev premera jeklene žice.}
\end{figure}
Z trakovnim merilom izmerimo dolžino žice. Izmerimo dolžino od vrha do kazalca pomičnega merila in od tega odštejemo razdaljo od ničle do vrha merila. Ker je pomično merilo veliko bolj natančno od trakovnega, upoštevamo le napako trakovnega merila. Tako na desetino centimetra poznamo dolžini -- \SI{213.5}{cm} za bakreno in \SI{208.4}{cm} za jekleno žico.

Z mikrometrom, ki je natančen na \(\pm\SI{0.01}{mm}\) izmerimo še premera. Izmerimo \SI{0.56}{mm} za bakreno in \SI{0.30}{mm} za jekleno žico. Izmerimo še premer žice, s katero bomo kasneje ocenili natezno trdnost bakra. Ta je \SI{0.125}{mm}

\subsection{Raztezki}

\begin{figure}
    \centering
    \includegraphics[width=2.5in]{fig/sketch.jpg}
    \caption{Skica pomičnega merila, s katerim merimo raztezke.}
\end{figure}
Na bakreno žico postopoma obešamo uteži in beležimo raztezke. Meritev ponovimo z jekleno žico.
\begin{table}
    \centering
    \begin{tabular}{c|l l c|l l}
        \(i\) & \(x_i\,[\si{mm}]\) & \(\Delta x\,[\si{mm}]\) & \(j\) & \(x_j\,[\si{mm}]\) & \(\Delta x\,[\si{mm}]\)\\
        \hline
        0 & 30.50 & 0 &      0 & 35.60 & 0\\
        1 & 30.70 & 0.20 &   1 & 36.35 & 0.75\\
        2 & 30.90 & 0.20 &   2 & 37.00 & 0.65\\
        3 & 31.20 & 0.30 &   3 & 37.20 & 0.20\\
        4 & 31.35 & 0.15 &   4 & 37.40 & 0.20\\
        5 & 31.60 & 0.25 &   5 & 37.60 & 0.20\\
        6 & 31.75 & 0.15 &   6 & 37.80 & 0.20\\
        7 & 31.95 & 0.20 &   7 & 38.15 & 0.35\\
        8 & 32.15 & 0.20 &   8 & 38.30 & 0.15\\
        9 & 32.40 & 0.25 &   9 & 38.50 & 0.20\\
        10 & 32.50 & 0.10 &  10 & 38.70 & 0.20\\
        11 & 32.70 & 0.20 &  11 & 38.90 & 0.20\\
        12 & 32.95 & 0.25 &     &       &\\            
        13 & 33.10 & 0.15 &  10 & 38.70 & -0.20\\
        14 & 33.30 & 0.20 &  9 & 38.55 & -0.15\\
        15 & 33.50 & 0.20 &  8 & 38.35 & -0.20\\
        16 & 33.75 & 0.25 &  7 & 38.20 & -0.15\\
           &       &      &  6 & 38.00 & -0.20\\
        15 & 33.55 & -0.20 & 5 & 37.80 & -0.20\\
        14 & 33.30 & -0.25 & 4 & 37.60 & -0.20\\
        13 & 33.10 & -0.20 & 3 & 37.30 & -0.30
    \end{tabular}
    \caption{Meritve leg spodnjega konca merila za bakreno in jekleno žico in posamezni prirastki.}
\end{table}

\subsection{Natezna trdnost}

Na podstavek dodajamo uteži, dokler se žica ne pretrga. V dveh poskusih se nam vedno pretrga, ko dodamo drugo utež.

\section{Račun}

\subsection{Prožnostni modul}

Koeficient raztezeka \(x\) in dolžine žice \(l\) je relativni raztezek. Odvisen je od sile, preseka žice in prožnostnega modula \(E\)
\begin{equation}
    \frac{x}{l}
    =
    \frac{1}{E}
    \left(
        \frac{F}{S}.
    \right)
\end{equation}
Če koeficient raztezka in mase uteži -- \(F\) je namreč sila teže, torej preprosto \(mg\) -- označimo kot \(\beta\), lahko prožnostni modul izrazimo kot
\begin{equation}
    E = \frac{1}{\beta}
    \left(
        \frac{gl}{S}.
    \right)
\end{equation}
Koeficient \(\beta\) lahko neposredno izračunamo kot naklon premice, prilagojene skozi raztezke pri različnih masah.
\begin{figure}
    \centering
    \input{fig/plot.pgf}
    \caption{Meritve raztezkov pri različnih utežeh in skozi njih prilagojeni premici. Jasno se vidi, kako pri jekleni žice prvi dve meritvi odstopata.}
\end{figure}
Za bakreno žico dobimo koeficient \SI{1.99}{mmkg^{-1}} z efektivno napako \(\pm\SI{0.017}{mmkg^{-1}}\). Za jekleno žico opazimo, da prvi dve meritvi nepričakovano izstopata iz premice, ki jo jasno črtajo ostale meritve -- zato ju izločimo in dobimo koeficient \SI{2.06}{mmg^{-1}} z efektivno napako \(\pm\SI{0.073}{mmg^{-1}}\)

Za izračun prožnostnega modula potrebujemo še presek žic. Iz premerov izračunamo presek bakrene žice \SI{0.25}{mm^2} z relativno napako \(\pm0.036\) in presek jeklene žice \SI{0.071}{mm^2} z napako \(\pm0.067\).

Ko propagiramo napake, za prožnostni modul bakra dobimo \SI{40.65}{GPa} z relativno napako \(\pm0.037\) in za prožnostni modul jekla \SI{132.75}{GPa} z relativno napako \(\pm0.069\). Če te vrednosti primerjam z vrednostmi, ki sta tipični za materiala, sta dobljena prožnostna modula nekoliko premajhna. Predvidevam, da je to, ker sta žici že precej izrabljeni. Prav tako s podatki, ki jih imam na volji, ni mogoče določiti meje linearnosti -- razen prvih dveh raztezkov pri jekleni žici vsi izmerjeni raztezki ležijo na oz. blizu premice.

\subsection{Natezna trdnost}

Vemo, da bakrena žica zdrži eno utež, ne pa dveh. Natezna trdnost je torej večja od napetosti, ki jo povzroča masa \(m_1\) -- podstavek z ena utežjo -- in manjša od napetosti, ki jo povzroča masa \(m_2\) -- podstavek z dvema utežema. Obe izračunamo po
\begin{equation}
    P_n = \frac{mg}{S}.
\end{equation}
Za spodnjo mejo dobimo \(127.86(1 \pm 0.17)\,\si{MPa}\), za zgornjo pa \(207.78(1 \pm 0.16)\,\si{MPa}\), torej je natezna trdnost med \SI{106.61}{MPa} in \SI{241.82}{MPa}, kar je kar veliko področje.

\end{document}
