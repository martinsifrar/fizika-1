import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({
    'pgf.texsystem': 'xelatex',
    'font.family': 'serif',
    'text.usetex': True
})
matplotlib.use('pgf')

import numpy as np
from reader import measurement_dict

msr = measurement_dict('./data')

x_i, m_i = 1e3 * msr['x_i'], 60 + msr['i'] * 100
x_j, m_j = 1e3 * msr['x_j'], 60 + msr['j'] * 100

k_b, c_b = np.polyfit(m_i, x_i, 1)
k_j, c_j = np.polyfit(m_j[3:], x_j[3:], 1)

print((k_b, k_b))
print((k_j, k_j))

fig, ax = plt.subplots(1, 1, figsize=[5, 4])

ax.scatter(m_i, x_i, marker='x')
ax.scatter(m_j, x_j, marker='x')
ax.plot(m_i, k_b * m_i + c_b, color='black', zorder=-1)
ax.plot(m_j, k_j * m_j + c_j, color='black', zorder=-1)

ax.set_xlabel('$m_i$ in $m_j\,[g]$')
ax.set_ylabel('$x_i$ in $x_j\,[mm]$', labelpad=7)

fig.tight_layout()
fig.savefig('img/plot.pgf')
