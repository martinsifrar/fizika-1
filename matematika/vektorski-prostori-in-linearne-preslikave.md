# Vektorski prostori in linearne preslikave

**Vaja.** Za nek $n \in \mathbb N$ naj bo $U$ $n$-razrežen podprostor $n + 1$-razsežnega prostora $V$. Pokaži, da obstaja baza $V$, ki ne vsebuje nobenega vektorja iz $U$.

**Dokaz.** Naj bo $\mathscr  = \begin{bmatrix} v_1 & v_2 & \cdots & v_n \end{bmatrix}$ baza $U$. Z vektorjem $w \in V \setminus \mathop{\mathrm{Span}}\{v_1, v_2, \dots, v_n\}$ jo dopolnimo do $\mathscr B'$, baze vektorskega prostora $V$. Zdaj vsak vektor iz $\mathscr B$ zamaknemo za $w$, tako da dobimo vektorje $v_1 + w, v_2 + w, \dots, v_n + w$. Ker $w$ ni iz $U$, tudi ti vektorji niso iz $U$. Vidimo, da so med seboj linearno neodvisni. Za vsak $v_i$ iz baze $\mathscr B$ namreč velja, da ni element $\mathop{\mathrm{Span}}\{v_1, v_2, \dots, v_{i - 1}, v_{i + 1}, \dots, v_n\}$. Podobno, ker z vektorji $v_1, v_2, \dots, v_n$ ne moremo zapisati netrivalne ničelne linearne kombinacije, je vektor $w$ od njih linearno neodvisen. Vektorji $v_1 + w, v_2 + w, \dots, v_n + w, w$ niso iz $U$, so linearno neodvisni, njihova linearna ogrinjača $\mathop{\mathrm{Span}}\{v_1 + w, v_2 + w, \dots, v_n + w, w\}$ pa je enaka linearni ogrinjači $\mathop{\mathrm{Span}}\{v_1, v_2, \dots, v_n, w\}$, torej so baza prostora $V$.

$\square$

**Trditev.** Linearna preslikava $T: V \to W$ je monomorfizem, če, in samo če, je $\mathop{\mathrm{ker}}(T) = \{0\}$.

**Dokaz.** Vsaka linearna preslikava vektor $0$ preslika samega vase. Ker je $T$ monomorfizem, v $0$ preslika samo vektor $0$. Obratno, če je $0$ edini vektor, ki ga $T$ preslika v $0$, to za vektorja $Tv, Tv'$, ki sta enaka, pomeni, da je $v = v'$. Ista sliki torej pomenita, da sta $v, v'$ isti vektor iz $V$.

omo

**Trditev.** Naj bo $T: V \to W$ linearna preslikava iz $V$ v $W$.

1. Če vektorji $v_1, v_2, \dots, v_r$ sestavljajo ogrodje vektorskega prostora $V$, potem vektorji $Tv_1, Tv_2, \dots, Tv_r$ sestavljajo ogrodje vektorskega prostora $\mathop{\mathrm{Im}}(T)$.
2. Če je $T$ monomorfizem (bijektiven) in so vektorji $v_1, v_2, \dots, v_k$ linearno neodvisni, so tudi $Tv_1, Tv_2, \dots, Tv_k$ linearno neodvisni.
3. Če je $T$ izomorfizem in če vektorji $v_1, v_2, \dots, v_n$ sestavljajo bazo $V$, potem $Tv_1, Tv_2, \dots, Tv_n$ sestavljajo bazo $W$.
4. Če obstaja takšna baza $\begin{bmatrix} v_1 & v_2 & \cdots & v_n \end{bmatrix}$ prostora $V$, da vektorji $Tv_1, Tv_2, \dots, Tv_n$ sestavljajo bazo $W$, potem je $W$ izomorfizem.

**Dokaz.**

1. Vsak $v \in V$ lahko zapišemo kot linearno kombinacijo

   $$
   v = \alpha_1 v_1 + \alpha_2 v_2 + \dots + \alpha_r v_r.
   $$
   Zaradi linearnosti je vsak $Tv \in \mathop{\mathrm{Im}}(T)$ preprosto
   
   $$
   Tv = \alpha_1 Tv_1 + \alpha_2 Tv_2 + \dots + \alpha_r Tv_r.
   $$

2. Gledamo, če ima linearno kombinacija kakšno netrivialno ničlo
   $$
   \alpha_1 Tv_1 + \alpha_2 Tv_2 + \dots + \alpha_k Tv_k = 0.
   $$
   Po linearnosti je to enakovredno
   $$
   T(\alpha_1 v_1 + \alpha_2 v_2 + \dots + \alpha_r v_r) = 0.
   $$
   Ker je $T$ monomorfizem, se v $0$ preslika le vektor $0$. Ta je ničelna linearna kombinacija neodvisnih vektorjev. Taka kombinacija je lahko le trivialna, torej so $\alpha_1, \alpha_2, \dots, \alpha_k$ vsi $0$.
   
3. Vektorji $Tv_1, Tv_2, \dots, Tv_n$ sestavljajo ogrodje $\mathop{\mathrm{Im}}(T)$, ki je zaradi surjektivnosti kar $W$. Zaradi injektivnosti linearna neodvisnost $v_1, v_2, \dots, v_n$ pomeni linearno neodvisnost $Tv_1, Tv_2, \dots, Tv_n$.

4. Če vektorji $Tv_1, Tv_2, \dots, Tv_n$ sestavljajo ogrodje $W$, je $\mathop{\mathrm{Im}}(T) = W$, torej je $T$ epimorfizem. Za bijektivnost poglejmo ničelno linearno kombinacijo vektorjev, ki sestavljajo bazo $W$. Ker so linearno neodvisni, je ta ničelna linearna kombinacija trivialna, torej po linearnosti

   
   $$
   \begin{align}
   T(\alpha_1 v_1 + \alpha_2 v_2 + \dots + \alpha_r v_r) = T(0) = 0.
   \end{align}
   $$
   Jedro $T$ je torej trivialno, s tem pa je $T$ monomorfizem in s skupaj s prvo ugotovotvijo tudi izomorfizem.

