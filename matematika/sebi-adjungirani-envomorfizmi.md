**Trditev.** Naj bo $T \in \mathop{\mathrm{End}}(V)$. Če je za vektorje v podprostoru $U$ $T$ pozitivno definiten, potem velja $\sum_{\lambda > 0} \mathop{\mathrm{akr}_T}(\lambda) \ge \mathop{\mathrm{dim}}U$.

**Dokaz.** Označimo $p = \sum_{\lambda > 0} \mathop{\mathrm{akr}_T}(\lambda)$. Izberimo ortonomirano bazo $\mathscr B = \begin{bmatrix} v_1 & v_2 & \cdots & v_n \end{bmatrix}$ prostora $V$, sestavljeno iz lastnih vektorjev $T$. Za vsak $i = 1, 2, \dots, n$ naj bo $\lambda_i$ tista lastna vrednost, za katero je $Tv_i = \lambda_i v_i$ . Vektorje v bazi lahko uredimo tako, da je $\lambda_i > 0$ za vsak index $i$, za katerega je $i \le p$ in da je $\lambda_i \le 0$ za vsak index $i$, za katerega velja $i > p$.

Predpostavimo, da je $p < \mathop{\mathrm{dim}}U$

