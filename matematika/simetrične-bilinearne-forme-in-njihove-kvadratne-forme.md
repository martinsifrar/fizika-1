# Simetrične bilinearne forme in njihove kvadratne forme

V realnem vektorskem prostoru $V$ je bilinearna forma preslikava $g: V \times  V \to \mathbb R$, za katero velja

1. $$
   g(u + v, w) = g(u, w) + g(v, w),
   $$

2. $$
   g(u, v + w) = g(u, v) + g(u, w),
   $$

3. $$
   g(\alpha u, v) = g(u, \alpha v) = \alpha g(u, v).
   $$

Bilinearna forma je simetrična, če zanjo velja
$$
g(u, v) = g(v, u).
$$
