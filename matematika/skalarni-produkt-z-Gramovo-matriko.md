V prostoru $\mathbb C^n$ prostoru s skalarnim produktom izberemo poljubno bazo $\mathscr B = \begin{bmatrix} e_1 & e_2 \cdots e_n \end{bmatrix}$. Izberemo poljubna vektorja $v, w \in V$ in zapišemo njun skalarni produkt
$$
\begin{align}
\langle v, w \rangle &= \langle \alpha_1 e_1 + \dots + \alpha_n e_n, \beta_1 e_1 + \dots + \beta_n e_n \rangle\\
&= \sum_{i = 1}^n \sum_{j = 1}^n \alpha_i \overline{\beta_j} \langle e_j, e_j \rangle.
\end{align}
$$
To lahko naprej zapišemo z Gramovo matriko $G_{ij} = \langle e_i, e_j\rangle$.
$$
\begin{align}
\langle v, w \rangle &= \sum_{i = 1}^n \sum_{j = 1}^n v_{i1} \overline{w_{j1}} G_{ij}\\
&= \sum_{i = 1}^n \left( v^T \right)_{1i} \sum_{j = 1}^n   G_{ij} \overline{w}_{j1}\\
&= \sum_{i = 1}^n \left( v^T \right)_{1i} \left(G \overline w \right)_{i1}\\
&= v^T G \overline w
\end{align}
$$
Če si nato izberemo matriko endomorfizma $S \in \mathop{\mathrm{Mat}}(n \times n, \mathbb C)$, je njegov adjungirana preslikava $S^*$ implicitno podana z
$$
\langle Sv, w \rangle = \langle v, S^*w \rangle.
$$
Po prej pokazanem lahko skalarna produkta zapišemo kot
$$
\begin{align}
(Sv)^T G\overline{w} &= v^T G \overline{S^*w}\\
v^T S^T G\overline{w} &= v^T G \overline{S^*} \overline{w}.
\end{align}
$$
Ker to velja za poljubna vektorja $v, w \in \mathbb C^n$, sledi
$$
\begin{align}
S^T G &= G \overline{S^*}\\
\overline{S^*} &= G^{-1} S^T G\\
S^* &= \overline{G^{-1}} S^H \overline{G}.\\
\end{align}
$$

