% !TEX program = xelatex

% Layout and styling
\documentclass{article}
\usepackage[a4paper, margin=1in]{geometry}

% Locale and bibliography
\usepackage{polyglossia}
\setdefaultlanguage[localalph=true]{slovenian}
\usepackage[autostyle]{csquotes}
\DeclareQuoteAlias{german}{slovene}

\usepackage{amsmath}
\usepackage{amssymb}

\DeclareMathOperator{\Sym}{Sym}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\sub}{sub}

\title{
    Vaja 3.14\\
    \large Determinanta bločno trikotne matrike\\
    in Vandermondova determinanta
}
\author{Martin Šifrar}

\begin{document}

\maketitle

(1) Imamo bločne zgornje-trikotno matriko
\begin{equation*}
    A = 
    \begin{pmatrix}
        F_{11} & F_{12} & \cdots & F_{1N}\\
        0 & F_{22} & \cdots & F_{2N}\\
        \vdots  & \vdots  & \ddots & \vdots\\
        0 & 0 & \cdots & F_{NN},
    \end{pmatrix}
\end{equation*}
pri čemer je vsaka izmed $F_{ij} \in Mat(n_i \times n_j, \mathbb{F})$ kvadratna matrika za vse $i,j \in \{1, 2, \cdots, N\}$, tako da je vsota $n_1 + n_2 + ... + n_N$ enaka $N$.

Determinanta matrike $A$ je po definiciji
\begin{equation*}
    \det(A) = \sum_{\sigma \in \Sym(n)} \sgn(\sigma) A_{1\sigma(1)} A_{2,\sigma(2)} \cdots A_{r\sigma(r)} A_{(r + 1)\sigma(r + 1)} \cdots A_{N\sigma(N)}.
\end{equation*}
Označimo $r = n_1 + n_2 + \dots + n_N$. Izpostavimo lahko komponente iz $F_{NN}$, torej za vse stolpce od $r + 1$ do $N$. Nato permutacijo $\sigma$ zapišemo kot $\sigma = \delta \circ \epsilon$, tako da je $\delta(n) = n$ za vse $n > r$ in $\epsilon(n) = n$ za vse $n \le r$. Zdaj lahko determinanto zapišemo kot
\begin{align*}
    \det(A) &= \sum_{\delta \in \Sym(r)} A_{1\delta(1)} A_{2,\delta(2)} \cdots A_{r\delta(r)} \left(\sum_{\epsilon \in \Sym(n_N)} \sgn(\delta \circ \epsilon) A_{(r + 1)\epsilon(r + 1)} \cdots A_{N\epsilon(N)}\right)\\
            &= \left(\sum_{\epsilon \in \Sym(n_N)} \sgn(\epsilon) A_{(r + 1)\epsilon(r + 1)} \cdots A_{N\epsilon(N)}\right) \sum_{\delta \in \Sym(r)} \sgn(\delta) A_{1\delta(1)} A_{2,\delta(2)} \cdots A_{r\delta(r)}\\
            &= \det(F_{NN}) \cdot \det\left(\sub_{\{1, 2, \cdots, r\}\{1, 2, \cdots, r\}}(A)\right).
\end{align*}
Kar dobimo je rekurzivna relacija, s katero zložimo matriko in dobimo končni izraz za determinanto
\begin{equation*}
    \det(A) = \prod_{i = 1}^N \det(F_{ii}).
\end{equation*}

(2) Vandermondova matrika je matrika oblike
\begin{equation*}
    V = 
    \begin{pmatrix}
        1 & \alpha_1 & \alpha_1^2 & \cdots & \alpha_1^{n - 1}\\
        1 & \alpha_2 & \alpha_2^2 & \cdots & \alpha_2^{n - 1}\\
        \vdots  & \vdots & \vdots & \ddots & \vdots\\
        1 & \alpha_n & \alpha_n^2 & \cdots & \alpha_n^{n - 1}
    \end{pmatrix}.
\end{equation*}
Stolpčno prištevanje in odštevanje stolpcev, pomnoženih s skalarjem, determinante ne spremeni. Tako vsem stolpcem $V_{\bullet j}$ za vse $j > 1$ odštejemo prejšnji stolpec, pomnožen z $\alpha_1$.
\begin{equation*}
    V = 
    \begin{pmatrix}
        1 & 0 & 0 & \cdots & 0\\
        1 & \alpha_2 - \alpha_1 & \alpha_2^2 - \alpha_1\alpha_2 & \cdots & \alpha_2^{n - 1} - \alpha_1\alpha_2^{n - 2}\\
        \vdots  & \vdots & \vdots & \ddots & \vdots\\
        1 & \alpha_n - \alpha_1 & \alpha_n^2 - \alpha_1\alpha_n & \cdots & \alpha_n^{n - 1} - \alpha_1\alpha_n^{n - 2}
    \end{pmatrix}.
\end{equation*}
Če zdaj matriko razvijemo po prvi vrstici, nam ostane matrika $\sub^{11}(V)$
\begin{equation*}
    \sub^{11}(V) = 
    \begin{pmatrix}
        \alpha_2 - \alpha_1 & \alpha_2^2 - \alpha_1\alpha_2 & \cdots & \alpha_2^{n - 1} - \alpha_1\alpha_2^{n - 2}\\
        \alpha_3 - \alpha_1 & \alpha_3^2 - \alpha_1\alpha_2 & \cdots & \alpha_2^{n - 1} - \alpha_1\alpha_3^{n - 2}\\
        \vdots & \vdots & \ddots & \vdots\\
        \alpha_n - \alpha_1 & \alpha_n^2 - \alpha_1\alpha_n & \cdots & \alpha_n^{n - 1} - \alpha_1\alpha_n^{n - 2}
    \end{pmatrix}.
\end{equation*}
Iz vsake vrstice lahko zdaj faktoriziramo $\alpha_{i + 1} - \alpha_1$. Determinanta se pri tem, ko vrstico z tem faktorjem, spremeni za isti faktor. Torej je determinanta nek faktor, pomnožen z determinanto za eno manjše Vandermonove matrike
\begin{align*}
    \det(V) = \det(\sub^{11}) &=
    \left(
        \prod_{i = 2}^n \alpha_i - \alpha_1
    \right)
    \det 
    \begin{vmatrix}
        1 & \alpha_2 & \alpha_2^2 & \cdots & \alpha_2^{n - 2}\\
        1 & \alpha_3 & \alpha_3^2 & \cdots & \alpha_3^{n - 2}\\
        \vdots  & \vdots & \vdots & \ddots & \vdots\\
        1 & \alpha_n & \alpha_n^2 & \cdots & \alpha_n^{n - 2}
    \end{vmatrix} =\\
                              &=
                              \left(
                                  \prod_{i = 2}^n \alpha_i - \alpha_1
                              \right)
                              \left(
                                  \prod_{i = 3}^n \alpha_i - \alpha_2
                              \right)
                              \cdots
                              \left(
                                  \prod_{i = n - 1}^n \alpha_i - \alpha_{n - 2}
                              \right)
                              \left(
                                  \alpha_n - \alpha_{n - 1}
                              \right) =\\
                              &=
                              \prod_{j = 2}^{n}
                              \left(
                                  \prod_{i = j}^n \alpha_i - \alpha_{j - 1}
                              \right),
\end{align*}
V zadnjem izrazu vidimo, da produkt preteče vse $i, j$, za katere je $n \ge i > j - 1 \ge 1$, kar lahko končno zapišemo kot
\begin{align*}
    \det(V) = \prod_{1 \le j < i \le n} \alpha_i - \alpha_j
\end{align*}

\end{document}
