**Izrek.** Naj bo $Ax = b$ sistem $m$ linearnih enačb za $n$ neznank in naj bo $C = \begin{bmatrix} A & b \end{bmatrix}$ razširjena matrika sistema. Tedaj velja:

1.  Če je $\mathop{\mathrm{rank}}(C) = \mathop{\mathrm{rank}}(A) + 1$, potem sistem nima rešitev.
2.  Če je $\mathop{\mathrm{rank}}(C) = \mathop{\mathrm{rank}}(A) = n$, ima sistem natanko eno rešitev.
3.  Če je $\mathop{\mathrm{rank}}(C) = \mathop{\mathrm{rank}}(A) < n$, ima sistem neskončno rešitev, ki jih lahko parametriziramo z $n - \mathop{\mathrm{rank}}(C)$ neodvisnimi parametri.

**Vaja.** Naj bo $A \in \mathbb R^{m \times n}$ matrika, pri čemer je $m \le n$ in $\mathop{\mathrm{rank}}(A) = m$. Pokaži, da obstaja njen desni inverz, t. j. matrika B, za katero velja $AB = I_m$.

**Rešitev.** Ker je rank $A$ enak $m$, ima sistem $Ax = b$ rešitev za vsak $b \in \mathbb R^m$. Če zdaj pogledamo sistem za enotske vektorje $\mathbb R^m$, imensko $e_1, e_2, \dots, e_m$, vidimo, da ima vsak sistem $Ax_i = e_i$ vsaj eno rešitev $x_i \in \mathbb R^n$. Te rešitve lahko zložimo v matriko in celo pomnožimo z $A$
$$
A \begin{bmatrix} x_1 & x_2 & \cdots & x_m \end{bmatrix} = \begin{bmatrix} Ax_1 & Ax_2 & \cdots & Ax_m \end{bmatrix} = I_m.
$$
$\square$

**Trditev.** Naj bo $A \in \mathbb F^{m \times n}$ neničelna matrika z rangom $r$. Tedaj obstaja neničelna $(r \times r)$-poddeterminanta $A$. Poleg tega so za vsak $k > n$ vse $(k \times k)$ poddeterminante enako $0$.

**Trditev.** Za poljubni matriki $A \in \mathbb F^{m \times n}$ in $B \in \mathbb F^{n \times r}$ velja
$$
(AB)^T = B^T A^T.
$$
**Dokaz.**
$$
\left( (AB)^T \right)_{ji} = \sum_{k = 1}^n A_{ik} B_{kj} = \sum_{k = 1}^n \left( A^T \right)_{ki} \left( B^T \right)_{jk} = \left( B^T A^T \right)_{ji}.
$$
$\square$

**Komentar.** Naj bo $V$ prostor dimenzije $n$ in $U$ njegov podprostor dimenzije $m < n$. Naj bo potem $\mathscr B = \begin{bmatrix} u_1 & u_2 & \cdots & u_m \end{bmatrix}$ baza $U$, ki jo do baze $V$ dopolnimo z vektorji $v_1, v_2, \dots, v_{n - m}$. Zdaj lahko definiramo endomorfizem $\mathop{\mathrm{pr^U}}: V \to V$, ki vektorje $u_1, u_2, \dots, u_m$ preslika v $0$, vektorje $v_1, v_2, \dots, v_{n - m}$ pa same vase. Podprostor $U$ je torej jedro te preslikave. Ker pa smo to definirali za poljuben $U$ podprostor $V$, je vsak podprostor jedro nekega endomorfizma $V$.

**Trditev.** Naj bo $V$ prostor dimenzije $n$ in $T: V \to V$ nilpotenten endomorfizem. Red endomorfizma, t. j. najmanjše število $k$, za katero velja $T^k = 0$, je manjši ali enak od $n$.

**Dokaz.** Naj bo $i < k$.  Dimenzija $\mathop{\mathrm{Im}}(T^{i + 1})$ ne more biti večji od dimenzije $\mathop{\mathrm{Im}}(T^{i})$, saj linearna preslikava vektor iz $\mathop{\mathrm{Im}}(T^{i})$ preslika v neko linearno kombinacijo vektorjev iz $\mathop{\mathrm{Im}}(T^{i})$. Za naraščajoče indekse je $a_i = \mathop{\mathrm{rank}}(T^i)$ torej padajoče zaporedje. Če za kak $i$ velja, da je $\mathop{\mathrm{rank}}(T^{i + 1}) = \mathop{\mathrm{rank}}(T^i)$, to pomeni, da je
$$
T(\mathop{\mathrm{Im}}(T^{i})) = \mathop{\mathrm{Im}}(T^{i}).
$$
To nato pomeni, da je zaporedje $a_i$ konstantno od tu naprej. Nikoli ne dosežemo preslikave z rangom $0$, torej nikoli ne dosežemo ničelne preslikave. Da je $T$ res nilpotenten, $a_{i + 1} = a_i$ ne sme veljati nikoli. Zaporedje $a_i$ je tako strogo padajoče in ker je $a_0 \le n$, to pomeni, da ničelno preslikavo dosežemo v $n$ ali manj korakih.

**Komentar.** nek vektor $T^iv \in U$. Če je $\mathop{\mathrm{rank}}(T^{i + 1}) = \mathop{\mathrm{rank}}(T^i)$, pomisli, kaj bi pomenilo, da $T(T^iv)$ ni element $U$. To bi pomenilo, da obstaja nek  $T^iv$, ki se preslika izven $U$. Ampak z njim se preslika tudi vsak $\lambda T^iv$, torej cel vektorski podprostor $U$ – torej si v preslikavi izgubil dimenzijo in $\mathop{\mathrm{rank}}(T^{i + 1}) \ne \mathop{\mathrm{rank}}(T^i)$.

**Trditev.** Naj vo $\mathcal O \sub V$ ogrodje vektorskega prostora $V$ nad $\mathbb F$ in naj bodo $v_1, v_2, \dots, v_k \in V$ med seboj linearno neodvisni vektorji v vektorskem prostoru $V$. Če so za vsak vektor $u \in \mathcal O \setminus \{v_1, v_2, \dots, v_k\}$ vektorji med seboj linearno odvisni v $V$, potem ti vektorji sestavljajo bazo vektorskega prostora V.

**Dokaz.** Linearna odvisnost zahteva netrivalno ničlo linearna kombinacije
$$
\alpha_1 v_1 + \alpha_2 v_2 + \dots + \alpha_3 v_3 + \beta u = 0.
$$
To je mogoče le če je $\beta \ne 0$, saj so preostali vektorji linearno neodvisni. Zato lahko vektor $u$ izrazimo kot linearno kombinacijo preostalih in je torej element $\mathrm{Span}\{v_1, v_2, \dots, v_k\}$. Poljuben $v \in \mathcal O$ je element torej element $\mathrm{Span}\{v_1, v_2, \dots, v_k\}$. Iz tega sledi
$$
V = \mathrm{Span}(\mathcal O) \sub \mathrm{Span}(\mathrm{Span}\{v_1, v_2, \dots, v_k\}) = \mathrm{Span}\{v_1, v_2, \dots, v_k\}.
$$
$\square$

