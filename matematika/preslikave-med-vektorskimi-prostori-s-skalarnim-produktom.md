# Preslikave med vektorskimi prostori s skalarnim produktom

## Adjungirana preslikava

$$
\langle Tv, w \rangle = \langle v, T^*w \rangle
$$

**Trditev.** Naj bodo $V$, $W$ in $Z$ vektorski prostori nad $\mathbb F$ s skalarnim produktom. Za poljubne preslikave $T, T' \in \mathop{\mathrm{Lin}}(V, W)$ in $S \in \mathop{\mathrm{Lin}}(W, Z)$ ter za vsak skalar $\alpha \in \mathbb F$ velja

1. $$
   (T + T')^* = T^* + T'^*,
   $$

2. $$
   (\alpha T)^* = \overline\alpha(T^*)
   $$

3. $$
   (T^*)^* = T,
   $$

4. $$
   (ST)^* = T^* S^*,
   $$

5. $$
   \mathop{\mathrm{id}^*} = \mathop{\mathrm{id}},
   $$

6. $$
   \mathop{\mathrm{ker}}(T^*) = \left( \mathop{\mathrm{im}}(T) \right)^\perp \:{in}
   $$

7. $$
   \mathop{\mathrm{im}}(T^*) = \left( \mathop{\mathrm{ker}}(T) \right)^\perp.
   $$

**Dokaz.**

6. Če je vektor $w$ iz jedra $\mathop{\mathrm{ker}}(T^*)$, je $T^*w = 0$. Ker drži
   $$
   \langle v, T^*w \rangle = 0 = \langle Tv, w \rangle,
   $$
   je  $w \perp Tv$ za poljuben $v \in V$, torej je $w$ element komplementa $\left( \mathop{\mathrm{im}}(T) \right)^\perp$. Obratno, če je vektor $w$ iz komplementa slike $\left( \mathop{\mathrm{im}}(T) \right)^\perp$,  je $w \perp Tv$ za poljuben $v \in V$. Ker za poljuben $v \in V$ drži
   $$
   \langle Tv, w \rangle = 0 = \langle v, T^*w \rangle,
   $$
    je $T^*w = 0$, torej je $w$ element $\mathop{\mathrm{ker}}(T^*)$.

7. Enakost, ki jo dokazujemo, je enakovredna enakosti
   $$
   \left( \mathop{\mathrm{im}}(T^*) \right)^\perp = \mathop{\mathrm{ker}}(T).
   $$
   Če je vektor $v$ iz komplementa slike $\left( \mathop{\mathrm{im}}(T^*) \right)^\perp$, je $v \perp T^*w$ za poljuben $w \in W$ . Ker za poljuben $W \in W$ drži
   $$
   \langle v, T^*w \rangle = 0 = \langle Tv, w \rangle,
   $$
    je $Tv = 0$, torej je $v$ element $\mathop{\mathrm{ker}}(T)$. Obratno, če je je vektor $v$ iz jedra $\mathop{\mathrm{ker}}(T)$, je $Tv = 0$. Ker drži
   $$
   \langle Tv, w \rangle = 0 = \langle v, T^*w \rangle,
   $$
   je  $v \perp T^*w$ za poljuben $w \in W$, torej je $v$ element komplementa $\left( \mathop{\mathrm{im}}(T^*) \right)^\perp$. 

**Trditev.** naj bosta V in W vekotrska prostora nad F. Naj bo B ortonormirana baza V in naj bo B‘ ortonormirana baza W. Za vsako linearno preslikavno $T \in \mathop{\mathrm{Lin}}(V, W)$, tedaj velja
$$
\begin{bmatrix} T \end{bmatrix}^{\mathscr B}_{\mathscr B'} = \left( \begin{bmatrix} T \end{bmatrix}^{\mathscr B}_{\mathscr B} \right)^H
$$
**Zgled.**
$$
\phi: V \to \mathbb F
$$

$$
\phi(v) = \phi(v) \cdot 1 = \langle \phi(v), 1 \rangle = \langle 1, \phi^*(1) \rangle
$$

**Posledice.**

1. $$
   p_{T^*} = \overline{p_{T}},
   $$

2. $$
   \mathop{\mathrm{det}}(T^*) = \overline{\mathop{\mathrm{det}}(T)},
   $$

3. $$
   \mathop{\mathrm{tr}}(T^*) = \overline{\mathop{\mathrm{tr}}(T)},
   $$

4. $$
   \sigma(T^*) = \overline{\sigma(T)},
   $$

5. $$
   \mathop{\mathrm{E_{T^*}}}(\lambda) =  \left( \mathop{\mathrm{im}}\left( T - \overline\lambda \mathop{\mathrm{id}} \right) \right)^\perp,
   $$

6. 

**Trditev.**

**Dokaz**



Preslikava $T\in \mathop{\mathrm{Lin}}(V, W)$ je linearna izometrija, če za poljubna $v, v' \in V$ velja
$$
\| Tv - Tv' \| = \| v - v' \|.
$$
**Trditev.** Za neko preslikavo $T\in \mathop{\mathrm{Lin}}(V, W)$ so naslednje trditve ekvivalentne

1. $T$ je linearna izometrija
2. $\| v \| = \| Tv \|$ za poljuben $v \in V$
3. $\langle Tv, Tv' \rangle = \langle v, v'\rangle$ za poljubna $v, v' \in V$

