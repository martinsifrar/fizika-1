
# Skalarni produkt

**Oznaka.** Razen če je dogovorjeno drugače, je $V$ vektorski prostor nad $\mathbb F$. Ko kasneje govorimo o skalarnem produktu, ima ta prostor tudi skalarni produkt.

**Definicija.** Naj bo $V$ skalarni prostor nad $\mathbb F$. Preslikava
$$
V \times V \to \mathbb F, \quad (v, w) \mapsto \langle v, w \rangle,
$$

je skalarni produkt na $V$, če za vse vektorje $u, v, w \in V$ in skalarje $\alpha \in \mathbb F$ velja

1. $$
   \langle u + v, w \rangle = \langle u, w \rangle + \langle v, w \rangle\\
   \langle \alpha v, w \rangle = \alpha \langle v w \rangle,
   $$

2. $$
   \langle v, w \rangle = \overline{\langle v, w \rangle},
   $$

5. $$
   \langle v, v \rangle \ge 0\\
   \langle v, v \rangle = 0 \implies v = 0.
   $$

**Komentar.** Skalarni produkt je linearen v prvem faktorju (1). Poleg tega je konjugirano simetričen (2) – kar pomeni, da je na $\mathbb R$ simetričen.  Končno je še pozitivno definiten (3).

Produkt je aditiven tudi v drugem faktorju

$$
\langle u, v + w \rangle = \overline{\langle v + w, u \rangle} =
= \overline{\langle v, u \rangle} + \overline{\langle w, u \rangle} = \langle u, v \rangle + \langle u, w \rangle.
$$

A ker je

$$
\langle v, \beta w \rangle = \overline{\beta \langle w, v \rangle } = \overline\beta \langle v, w \rangle,
$$

rečemo, da je v drugem faktorju konjugirano linearen. Iz tega in iz 3. lastnosti sledi, da je skalarni produkt z enim od faktorjev $0$ enak $0$.

Vektorski prostor nad $\mathbb F$ s skalarnim produktom je vektorski prostor nad $\mathbb F$, opremljen z nekim skalarnim produktom. Za vsak tak prostor je število

$$
\| v \| = \sqrt{\langle v \rangle} \ge 0
$$

norma ali dolžina vektorja $v$. To je očitno nenegativno število, ki je $0$ zgolj za ničelni vektor. Vidimo, da velja

$$
\| \alpha v \| = \sqrt{\alpha \overline\alpha \langle v, v \rangle} = |v| \| v \|.
$$

---

Pravokotnost dveh vektorjev v $V$ definiramo glede na nek skalarni produkt na $V$

$$
\langle v, w \rangle = 0 \iff v \perp w. 
$$

Relacija je simetrična in ničelni vektor je pravokoten na vse vektorje iz $V$, tudi sam nase.

Naj bosta $\mathcal O$ in $\mathcal O'$ podmnožici $V$. Množici sta med seboj pravokotni, če je vsak vektor iz množice $\mathcal O$ pravokoten na vsak vektor iz množice $\mathcal O'$.

---

Naj bo $U$ vektorski podprostor $V$. Zožitev $V \times V \to \mathbb F$ na $U \times U$ je skalarni produkt na $U$.

**Vaja 3.3.** Naj bo $\mathcal O$ podmnožica vektorskega prostora $V$ s skalarnim produktom in naj bo vektor $v \in V$ pravokoten na vsak vektor iz podmnožice $\mathcal O$. Pokaži, da je tedaj vektor $v$ pravokoten na vsak vektor iz vektorskega podprostora $\mathop{\mathrm{Span}}(\mathcal O)$.

**Rešitev.** Vsak vektor $u \in \mathop{\mathrm{Span}}(\mathcal O)$ lahko zapišemo kot
$$
u = \sum_{i = 1}^n \alpha_i u_i,
$$

kjer so $\alpha_i \in \mathbb F$ in $u_i \in \mathcal O$. Skalarni produkt $u$ s poljubnim $v \in V$ je torej

$$
\langle u, v \rangle = \sum_{i = 1}^n \alpha_i \langle u_i, v \rangle = 0,
$$

ker so $u_i \perp v$. Ker pa je $\langle u, v \rangle = 0$, sta $u$ in $v$ pravokotna.

---

Na vsakem vektorskem prostoru $\mathbb R^n$ imamo t. i. standardni skalarni produkt, ki je za vektorja $v = (\alpha_1, \alpha_2, \dots, \alpha_n)$ in $w = (\beta_1, \beta_2, \dots, \beta_n)$ dan s predpisom

$$
\langle v, w \rangle = \alpha_1\beta_1 + \alpha_2\beta_2 + \dots + \alpha_n\beta_n.
$$

Ta skalarni produkt označimo tudi kot $v \cdot w$ in če vektorje gledamo kot elemente $\mathop{\mathrm{Mat}}(n \times 1, \mathbb R)$ in matriko z enim elementom gledamo kot skalar, ga lahko ekvivalento zapišemo kot

$$
v \cdot w = v^Tw = w^Tv
$$

---

Prav tako imamo na $\mathbb C^n$ standardni skalarni produkt, ki je za vektorja $v = (\alpha_1, \alpha_2, \dots, \alpha_n)$ in $w = (\beta_1, \beta_2, \dots, \beta_n)$ dan s predpisom

$$
\langle v, w \rangle = \alpha_1\overline\beta_1 + \alpha_2\overline\beta_2 + \dots + \alpha_n\overline\beta_n.
$$

Ali ekvivalentno

$$
v \cdot w = v^T\overline w = w^Hv
$$

---

Naj bo $\rho: [a, b] \to \mathbb R$ povsod pozitivna realna zvezna funcija. Za poljubni realni zvezni funkciji $f, g: [a, b] \to \mathbb R$ naj bo skalarni produkt

$$
\langle f, g \rangle = \int_a^b \rho fg \mathop{\mathrm d t}.
$$

Vidimo, da je definirano res skalarni produkt na vektorskem prostoru realnih zveznih funkcij na intervalu $[a, b]$. Glede na ta skalarni produkt je norma

$$
\| f \| = \sqrt{\int_a^b \rho f^2 \mathop{\mathrm d t}}.
$$

## Osnovne lastnosti skalarnega produkta

**Trditev.** Če za poljubna vektorja $v, w \in V$ velja, da je $\langle u, v\rangle = \langle u, w\rangle$ za vsak $u \in V$, potem sta $v$ in $w$ enaka.

**Dokaz.** Razlika $\langle u, v \rangle$ in $\langle u, w \rangle$ je $\langle u, v - w \rangle$ in je enaka $0$. Posebej za $u$ vzamemo $v - w$, iz česar po pozitivni definitnosti sledi, da je $v - w = 0$, torej sta vektorja $v$ in $w$ enaka.

$\square$

**Trditev** (Pitagorov izrek). Če sta $v, w \in V$ pravokotna, potem je $\| v + w \|^2 = \| v \|^2 + \| w \|^2$.

**Dokaz.** Izkoristimo dejstno, da je skalarni produkt pravokotnih vektorjev enak $0$
$$
\begin{align}
\| v + w \|^2 &= \langle v + w, v + w \rangle\\
			&= \langle v, v + w \rangle + \langle w, v + w \rangle\\
			&= \langle v, v \rangle + \langle v, w \rangle + \langle w, v \rangle + \langle w, w \rangle\\
			&= \langle v, v \rangle + \langle w, w \rangle\\
			&= \| v \|^2 + \| w \|^2.
\end{align}
$$
$\square$

**Trditev.** Za poljubna vektorja $v, w \in V$ velja

2.  $$
    \| v + w \|^2 + \| v - w \|^2 = 2 \left(\| v \|^2 + \| w \|^2\right), \qquad \textrm{(paralelogramsko pravilo)}
    $$

3.  $$
    \Big| \| v \| - \| w \| \Big| \le \| v + w \| \le \| v \| + \| w \|, \qquad \textrm{(trikotniška neenakost)}
    $$

4.  $$
    | \langle v, w\rangle | \le \| v \| \| w \|, \qquad \textrm{(Cauchy-Schwarzova neenakost)}
    $$


**Dokaz.** Tako kot v dokazu Pitagorevega izreka azpišemo kvadrata norm
$$
\begin{align}
\| v + w \|^2 - \| v - w \|^2 &= \langle v + w, v + w \rangle - \langle v - w, v - w \rangle\\
						  &= \langle v, v \rangle + \langle v, w \rangle + \langle w, v \rangle + \langle w, w \rangle\\
						  & \qquad + \left( \langle v, v \rangle - \langle v, w \rangle - \langle w, v \rangle + \langle w, w \rangle \right) =\\
						  &= 2\langle v, v \rangle + 2\langle w, w \rangle\\
						  &= 2 \left(\| v \|^2 + \| w \|^2\right).
\end{align}
$$
Tako smo dokazali 1. točko. Za dokaz 3. točke vektor $v$ razstavimo na dve komponenti. Projekcija $v_\parallel$, je vzporedna z vektorjem $w$ in jo izračunamo kot
$$
v_\parallel = \frac{\langle v, w \rangle}{\langle w, w \rangle} w.
$$
Pravokotna nanjo je komponenta $v_\perp = v - v_\parallel$. Zdaj zapišemo po Pitagorovem izreku
$$
\begin{align}
\| v \|^2 &= \| v_\perp + v_\parallel \|^2\\
		  &= \| v_\perp \|^2 + \| v_\parallel \|^2\\
		  &= \| v_\perp \|^2 + \left\| \frac{\langle v, w \rangle}{\langle w, w \rangle} w \right\|^2\\
		  &= \| v_\perp \|^2 + \left| \frac{\langle v, w \rangle}{\| w \|^2} \right|^2 \| w \|^2\\
		  &= \| v_\perp \|^2 + \frac{|\langle v, w \rangle|^2}{\| w \|^2}.
\end{align}
$$
Če ven izrazimo $| \langle v, w \rangle |^2$, dobimo
$$
| \langle v, w \rangle |^2 = \| w \|^2 \left( \| v \|^2  - \| v_\perp \|^2 \right),
$$
iz česar sledi neenakost v točki 3. Ta je pravzaprav enakost, če je pravokotna komponenta enaka $0$ – takrat sta vektorja $v$ in $w$ vzporedna. 

Za 2. točko zapišemo
$$
\begin{align}
\| v + w \|^2 &= \langle v + w, v + w \rangle\\
			  &= \langle v, v \rangle + \langle v, w \rangle + \langle w, v \rangle + \langle w, w \rangle\\
			  &= \langle v, v \rangle + \langle v, w \rangle + \overline{\langle v, w \rangle} + \langle w, w \rangle\\
			  &= \| v \|^2 + 2 \Re(\langle v, w \rangle) + \| w \|^2.
\end{align}
$$
Ker je $\big| Re(\langle v, w \rangle) \big| \le | \langle v, w \rangle |$ in po točki 3 dodatno $| \langle v, w \rangle | \le \| v \| \| w \|$, lahko zapišemo neenakost
$$
\begin{align}
\| v \|^2 + 2 \Re(\langle v, w \rangle) + \| w \|^2 &\le \| v \|^2 + 2 \| v \| \| w \| + \| w \|^2\\
\| v + w \|^2 &\le \left( \| v \| + \| w \| \right)^2\\
\| v + w \| &\le \| v \| + \| w \|.
\end{align}
$$
To je desni del neenakosti. Enako z negativnim predznakom v srednjem členu dobimo še njen levi del
$$
\begin{align}
\| v \|^2 + 2 \Re(\langle v, w \rangle) + \| w \|^2 &\ge \| v \|^2 - 2 \| v \| \| w \| + \| w \|^2\\
\| v + w \|^2 &\ge \left( \| v \| - \| w \| \right)^2\\
\| v + w \| &\ge \;\, \Big| \| v \| - \| w \| \Big|.
\end{align}
$$
Tako smo dokazali tudi 2. točko. 

$\square$

**Trditev** (Polarizacijska enačba).

1.  Naj bosta $v, w \in V$. Če je $V$ vektorski prostor nad $\mathbb R$, potem velja
    $$
    \langle v, w \rangle = \frac{1}{4} \left( \|v + w \|^2 - \|v - w \|^2 \right).
    $$
    
2.  Če pa je $V$ vektorski prostor nad $\mathbb C$, potem velja
    $$
    \langle v, w \rangle = \frac{1}{4} \left( \|v + w \|^2 - \|v - w \|^2 + \mathscr i\|v + \mathscr i w \|^2 - \mathscr i \|v - \mathscr i w \|^2 \right).
    $$

**Dokaz.** Za 1. točko nam račun poenostavi simetričnost skalarnega produkta nad realnim vektorskim prostorom
$$
\begin{align}
\| v + w \|^2 - \| v - w \|^2 &= \langle v + w, v + w \rangle - \langle v - w, v - w \rangle\\
			  &= \langle v, v \rangle + \langle v, w \rangle + \langle w, v \rangle + \langle w, w \rangle\\
			  & \qquad -  \left( \langle v, v \rangle - \langle v, w \rangle - \langle w, v \rangle + \langle w, w \rangle \right) =\\
              &= 2 \langle v, w \rangle + 2 \langle w, v \rangle =\\
              &= 4 \langle v, w \rangle
\end{align}
$$
Za 2. točko prejšnji račun do predzadnjega še vedno velja, a le za realno komponento 
$$
\begin{align}
\| v + w \|^2 - \| v - w \|^2 &= 2 \langle v, w \rangle + 2 \langle w, v \rangle =\\
							  &=2 \langle v, w \rangle + 2 \overline{\langle v, w \rangle} =\\
              &= 4 \Re(\langle v, w \rangle)
\end{align}
$$
Za imaginarno komponento podobno razpišemo
$$
\begin{align}
\mathscr i\|v + \mathscr i w \|^2 - \mathscr i \|v - \mathscr i w \|^2 &= \mathscr i \left( \langle v + \mathscr i w, v + \mathscr i w \rangle - \langle v - \mathscr i w, v - \mathscr i w \rangle \right)\\
																	   &= \mathscr i \big( \langle v, v \rangle + \langle v, \mathscr i w \rangle + \langle \mathscr i w, v \rangle + \langle \mathscr i w, \mathscr i w \rangle\\
			  & \qquad -  \left( \langle v, v \rangle - \langle v, \mathscr i w \rangle - \langle \mathscr i w, v \rangle + \langle \mathscr i w, \mathscr i w \rangle \right) \big) =\\
			  &= 2 \mathscr i \langle v, \mathscr i w \rangle + 2 \mathscr i \langle \mathscr i w, v \rangle =\\
			  &= 2 \left( -\mathscr i^2 \right) \langle v, w \rangle + 2 \mathscr i^2 \langle w, v \rangle =\\
			  &= 2 \langle v, w \rangle - 2 \overline{\langle v, w \rangle} =\\
			  &= \mathscr i 4 \Im(\langle v, w \rangle)
\end{align}
$$

Če enačbi sestejemo, dobimo 2. točko.

$\square$

## ortonormirane baze

Vektor $u \in V$ je normiran, če je $\| u \| = 1$. Poljuben neničelen vektor $v \in V$ normiramo tako, da ga deljimo z njegovo dolžino oz. normo
$$
u = \frac{v}{\| v \|}
$$
Vektorji $v_1, v_2, \dots, v_n$ sestavljajo ortonormiran sistem $V$, če so normirani in paroma pravokotni glede na skalarni produkt na $V$. Če hkrati sestavljajo tudi bazo $V$, rečemo, da sestavljajo ortonormirano bazo $V$. Baza $\mathscr B = \begin{bmatrix} v_1 & v_2  & \cdots & v_n \end{bmatrix}$ je torej ortonormirana.

**Komentar.** Glede na standardni skalarni produkt na $\mathbb F^n$ je standardna baza ortonormirana.

**Trditev.** Za vsak končno dimenzionalen $V$ nad $\mathbb F$ in bazo $\mathscr B$ obstaja natanko en skalarni produkt na $V$, glede na katerega je baza $\mathscr B$ ortonormirana.

**Dokaz.** Naj bo $\mathscr B = \begin{bmatrix} v_1 & v_2  & \cdots & v_n \end{bmatrix}$. Produkt vektorjev $v, w \in V$ zapišemo kot
$$
\begin{align}
\langle v, w \rangle &= \Bigg\langle \sum_{i}^n \left( \begin{bmatrix} v \end{bmatrix}_{\mathscr B} \right)_{i1} v_i, \ \sum_{j}^n \left( \begin{bmatrix} w \end{bmatrix}_{\mathscr B} \right)_{j1} v_j \Bigg\rangle\\
					 &= \sum_{i = 1}^n \sum_{j = 1}^n \Big\langle \left( \begin{bmatrix} v \end{bmatrix}_{\mathscr B} \right)_{i1} v_i, \ \langle \left( \begin{bmatrix} w \end{bmatrix}_{\mathscr B} \right)_{j1} v_j \Big\rangle\\
					 &= \sum_{i = 1}^n \sum_{j = 1}^n \left( \begin{bmatrix} v \end{bmatrix}_{\mathscr B} \right)_{i1} \overline{\left( \begin{bmatrix} w \end{bmatrix}_{\mathscr B} \right)_{j1}} \langle v_i, v_j \rangle\\
					 &= \sum_{i = 1}^n \left( \begin{bmatrix} v \end{bmatrix}_{\mathscr B} \right)_{i1} \overline{\left( \begin{bmatrix} w \end{bmatrix}_{\mathscr B} \right)_{i1}}.
\end{align}
$$
Ta skalarni produkt je enolično določen

$\square$

**Trditev.** Če vektorji $v_1, v_2, \dots, v_n \in V$ sestavljajo ortonormiran sistem $V$, so med seboj linearno neodvisni in za vsak $v \in \mathop{\mathrm{Span}}\{v_1, v_2, \dots, v_n\}$ velja
$$
v = \sum_{i = 1}^n \langle v, v_i \rangle v_i \quad \textrm{in} \quad \| v \|^2 = \sum_{i = 1}^n \, \big| \langle v, v_i \rangle \big|^2.
$$
**Dokaz.** Vektor $v$ zapišemo kot $\alpha_1 v_1 + \alpha_2 v_2 + \dots + \alpha_n v_n$. Ker je $\langle v_j, v_i \rangle$ za $i \ne j$ enako 0, je produkt
$$
\langle v, v_i \rangle = \ \Bigg\langle \sum_{j = 1}^n \alpha_i v_j, \ v_i \Bigg\rangle = \sum_{j = 1}^n \alpha_1 \langle v_j, v_i \rangle = \alpha_i.
$$
S tem smo pokazali prvo enakost.  Za ničelni vektor to pomeni
$$
0 = \sum_{i = 1}^n \alpha_i v_i = \sum_{i = 1}^n \langle 0, v_i \rangle v_i,
$$
ker je $\langle 0, v_i\rangle = 0$, smo pokazali, da so vektorji $v_1, v_2, \dots, v_n$ linearno neodvisni. Zdaj zapišemo
$$
\begin{align}
\| v \|^2 &= \ \Bigg\langle \sum_{i = 1}^n \langle v, v_i \rangle v_i, \ \sum_{j = 1}^n \langle v, v_j \rangle v_j \Bigg\rangle\\
		  &= \sum_{i = 1}^n \sum_{j = 1}^n \Big\langle \langle v, v_i \rangle v_i, \ \langle v, v_j \rangle v_j \Big\rangle\\
		  &= \sum_{i = 1}^n \langle v, v_i \rangle \overline{\langle v, v_j \rangle}\\
		  &= \sum_{i = 1}^n \, \big| \langle v, v_i \rangle \big|^2.
\end{align}
$$
Tako dokažemo tudi drugo enakost.

$\square$

**Komentar.** Če je $\mathscr B = \begin{bmatrix} v_1 & v_2  & \cdots & v_n \end{bmatrix}$ ortonormirana baza V, lahko komponente koordinatnega vektorja $\begin{bmatrix} v \end{bmatrix}_{\mathscr B}$ vektorja $v \in V$ zapišemo kot
$$
\left( \begin{bmatrix} v \end{bmatrix}_{\mathscr B} \right)_{i1} = \langle v, v_i \rangle.
$$
**Vaja.**

1. Naj bodo $v_1, v_2, \dots, v_n \in V$ neničelni vektorji, ki so paroma pravokotni. Pokaži, da so med seboj linearno neodvisni in da za vsak vektor $v \in \mathop{\mathrm{Span}}\{v_1, v_2, \dots, v_n\}$ velja
   $$
   v = \sum_{i = 1}^n \frac{\langle v, v_i \rangle} {\| v_i \|^2} v_i
   $$
   
2. Naj bo $\mathscr B = \begin{bmatrix} v_1 & v_2  & \cdots & v_n \end{bmatrix}$ baza $V$ nad $\mathbb F$ in naj bo $\mathscr O = \begin{bmatrix} w_1 & w_2  & \cdots & w_m \end{bmatrix}$ ortonormirana baza $W$ nad $\mathbb F$. Pokaži, da za vsako linearno preslikavo $T: V \to W$ velja $\left( \begin{bmatrix} T \end{bmatrix}^{\mathscr B} _{\mathscr O} \right)_{ij} = \big\langle Tv_j, w_i \big\rangle$ za vse $i \in \{1, 2, \dots, n\}$ in $j \in \{1, 2, \dots, m\}$.

**Rešitev.**

1. Vektor $v$ zapišemo kot $\alpha_1 v_1 + \alpha_2 v_2 + \dots + \alpha_n v_n$. Ker je $\langle v_j, v_i \rangle$ za $i \ne j$ enako 0, je
   $$
   \frac{\langle v, v_i\rangle}{\| v_i \|^2} = \frac{1}{\| v_i \|^2} \sum_{j = 1}^n \alpha_i \langle v_j, v_i \rangle = \alpha_i,
   $$
   torej smo dokazali enakost. Za ničelni vektor to pomeni
   $$
   0 = \sum_{i = 1}^n \alpha_i v_i = \sum_{i = 1}^n \frac{\langle 0, v_i\rangle}{\| v_i \|^2} v_i
   $$
   ker je $\langle 0, v_i\rangle = 0$, smo pokazali, da so vektorji $v_1, v_2, \dots, v_n$ linearno neodvisni.

2. Koordinatni vektor $\begin{bmatrix} Tv_j \end{bmatrix}_{\mathscr O}$ vektorja $Tv_j$ lahko zapišemo kot
   $$
   \begin{bmatrix} Tv_j \end{bmatrix}_{\mathscr O} = \begin{bmatrix} T \end{bmatrix}^{\mathscr B}_{\mathscr O} \begin{bmatrix} v_j \end{bmatrix}_{\mathscr B} = 
   \left( \begin{bmatrix} T \end{bmatrix}^{\mathscr B}_{\mathscr O} \right)_{\bullet j}.
   $$
   Če pa koordinatni vektor z leve pomnožimo z bazo $\mathscr O$, dobimo  vektor sam
   $$
   Tv_j = \sum_{k = 1}^m \left( \begin{bmatrix} T \end{bmatrix}^{\mathscr B}_{\mathscr O} \right)_{kj} w_k.
   $$
   S tem lahko zapišemo skalarni produkt
   $$
   \begin{align}
   \langle Tv_j, w_i\rangle &= \ \Bigg\langle \sum_{k = 1}^n \left( \begin{bmatrix} T \end{bmatrix}^{\mathscr B}_{\mathscr O} \right)_{kj} w_k, \ w_i \Bigg\rangle\\
   						 &= \left( \begin{bmatrix} T \end{bmatrix}^{\mathscr B}_{\mathscr O} \right)_{ij} \langle w_i, w_i\rangle\\
   						 &= \left( \begin{bmatrix} T \end{bmatrix}^{\mathscr B}_{\mathscr O} \right)_{ij}.
   \end{align}
   $$
   $\square$

## Gram-Schmidtova ortonormalizacija

**Trditev.** Naj vektorji $u_1, u_2, \dots, u_n \in V$ sestavljajo ortonormiran sistem $V$. Za vsak vektor $v \in V \setminus \mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_n\}$ je vektor
$$
v - \sum_{i = 1}^n \langle v, u_i \rangle u_i
$$
element množice $V \setminus \mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_n\}$ in je pravokoten na vse vektorje iz podprostora $\mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_n\}$.

**Dokaz.** Označimo
$$
u = \sum_{i = 1}^n \langle v, u_i \rangle u_i.
$$
Ta vektor je element linearne ogrinjače $\mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_n\}$. Ker je $v$ po predpostavki zu

naj te ogrinjače, enako velja za $v - u$. Ker je 
$$
\begin{align}
\langle v - u, u_j \rangle &= \langle v, u_j \rangle - \ \Bigg\langle \sum_{i = 1}^n \langle v, u_i \rangle u_i, \ u_j \Bigg\rangle\\
						   &= \langle v, u_j \rangle - \langle v, u_j \rangle \langle u_j, u_j \rangle\\
						   &= 0,
\end{align}
$$
je $v - u$ pravokoten na vse $u_1, u_2, \dots, u_n$ in s tem pravokoten na vse $v \in \mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_n\}$.

$\square$

**Oznaka.** Preslikavo neničelnega vektorja v njegov enotski vektor označimo kot
$$
\mathop{\mathrm{Unit}}: V \to V, \quad v \mapsto \frac{v}{\| v \|}.
$$
Dodatno rečemo, da ničelni vektor preslika samega vase. Ta preslikava je izomorfizem $V$.

**Izrek** (Gram-Schmidtova ortonormalizacija). Za poljubne med seboj linearno neodvisne vektorje $v_1, v_2, \dots, v_n \in V$ lahko rekurzivno izračunamo vektorje $u_1, u_2, \dots, u_n \in V$ s predpisom
$$
\begin{align}
u_1 &= \mathop{\mathrm{Unit}}(v_1),\\
u_i &= \mathop{\mathrm{Unit}}\left( v_i - \sum_{k = 1}^{i - 1} \langle v_i, u_k \rangle u_k \right).
\end{align}
$$
Dobljeni vektorji $u_1, u_2, \dots, u_n$ sestavljajo ortonormiran sistem in velja
$$
\mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_r\} = \mathop{\mathrm{Span}}\{v_1, v_2, \dots, v_r\}
$$
za vsak $r = 1, 2, \dots, n$.

**Dokaz.** Za $i = 1$ izrek velja. Naj bo torej $i \ge 2$ in predpostavimo, da smo po postopku že izračunali vektorje $v_1, v_2, \dots, v_{i - 1}$, ki po izreku, ki ga dokazujemo, sestavljajo ortonormiran sistem, s tem da velja
$$
\mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_r\} = \mathop{\mathrm{Span}}\{v_1, v_2, \dots, v_r\}
$$
za vsak $r = 1, 2, \dots, i - 1$. Zdaj označimo

$$
w_i = \sum_{k = 1}^{i - 1} \langle v_i, u_k \rangle u_k,
$$
ki je element $\mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_{i - 1}\}$. S to oznako je

$$
u_i = \mathop{\mathrm{Unit}}(v_i - w_i).
$$
Ker so $v_1, v_2, \dots, v_n$ med seboj neodvisni, vemo, da $v_i$ ni element linearne ogrinjače $\mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_{i-1}\}$. Ker je $w_i$ znotraj, $v_i$ pa izven te linearne ogrinjače, je tudi $v_i - w_i$ zunaj ogrinjače in je element $V \setminus \mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_{i-1}\}$. Po prej dokazani trditvi je $v_i - w_i$ pravokoten[^1] na vse vektorje v $\mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_{i-1}\}$. Na vektorje v je pravokoten tudi vektor $\mathop{\mathrm{Unit}}(v_i - w_i)$, ki je dodatno enotski vektor. Vektorji $u_1, u_2, \dots, u_{i - 1}, u_i$ so torej ortonormiran sistem $V$. Po indukciji na $i$ so torej končni vektorji $u_1, u_2, \dots, u_n$ ortonormiran sistem $V$.

Podprostora
$$
V_i = \mathop{\mathrm{Span}}\{v_1, v_2, \dots, v_{i - 1}, v_i\} \textrm{in}\\
U_i = \mathop{\mathrm{Span}}\{u_1, u_2, \dots, u_{i - 1}, u_i\}
$$
sta dimenzije $i$. Po predpostavki sta $U_{i-1}$ in $V_{i - 1}$ enaka. Ker je $u_i$ linearna kombinacija $u_1, u_2, \dots, u_{i - 1}$ in $v_i$, je
$$
U_{i - 1} + \mathbb F v_1 = U_i \sub V_i.
$$
Ker imata enaki dimenziji, sta podprostora $U_i$ in $V_i$ enaka.

$\square$

**Komentar.** Opisana Gram-Schmidtova ortonormalizacija vektorje ortonormiranega sistema preslika sama vase.

**Posledica.** Vsak prostor $V$ ima vsaj eno ortonormirano bazo. Izberemo lahko namreč poljubno bazo $V$ in jo ortonomiramo (ortonomiramo vektorje, ki jo sestavljajo).

Prav tako lahko vsak ortonormiran sistem $V$ dopolnimo do ortonormirane baze $V$. ortonormiran sistem namreč sestavljajo med seboj linearno neodvisni vektorji, ki jih lahko dopolnimo do baze. Dobljeno bazo lahko nato ortonomiramo.

## Ortogonalni komplement

Naj bo $\mathcal O$ poljubna podmožica prostora $V$. Ortogonalni komplement $\mathcal O$ v $V$ označimo $\mathcal O^\perp$. To je množica vseh vektorjev iz $V$, ki so pravokotni na vektorje v $\mathcal O$
$$
\mathcal O^\perp = \{ v \in V \mid \langle v, u \rangle = 0 \;\textrm{za vsak}\; u \in \mathcal O \}.
$$
Ker je skalarni produkt linearen v prvem faktorju, je ortogonalni komplement $\mathcal O$ v $V$  vektorski podprostor $V$. Če sta $v, v' \in \mathcal O^\perp$ in je $u$ poljuben vektor iz $\mathcal O$, velja namreč
$$
\langle v + v', u \rangle = \langle v, u \rangle + \langle v', u \rangle = 0 + 0 = 0,\\
\langle \alpha v, u \rangle = \alpha0 = 0.
$$
Zaradi linearnosti drži tudi $\mathcal O^\perp = \mathop{\mathrm{Span}}(\mathcal O)^\perp$. Ker pa je pravokotnost simetrična relacija, je $\mathcal O \sub \left( \mathcal O^\perp \right)^\perp$.

**Vaja.** Pokaži, da velja $\{0\}^\perp = V$ in $V^\perp = \{0\}$.

**Rešitev.** Ničelni vektor je pravokoten na vsak vektor. Obratno, vsi vektorju so pravokotni na ničelni vektor.

**Trditev.** Za poljuben $U$ podprostor $V$ velja
$$
V = U \oplus U^\perp \quad \textrm{in} \quad U = \left( U^\perp \right)^\perp.
$$
**Dokaz.** Če je nek vektor iz $U$ tudi element $U^\perp$, je pravokoten na vse vektorje v $U$, vključno s samim sabo. Ker je edini vektor, ki je pravokoten sam nase ničelni vektor, je presek $U \cup U^\perp$ enak $\{0\}$.

## Rieszov reprezentacijski izrek

Linearnim preslikavam $V \to \mathbb F$ pravimo tudi linearni funkcionali na $V$. Vektorski prostor vseh linearnih funkcionalov na $V$ običajno krajše označimo kot
$$
V^* = \mathop{\mathrm{Lin}}(V, \mathbb F) 
$$
in ga imenuje dualni vektorski prostor oz. dual vektorskega prostora $V$. Če je $V$ končno dimenzionalen, potem je tudi dualni $V^*$ končno dimenzionalen in velja $\mathop{\mathrm{dim}} V^* = \mathop{\mathrm{dim}} V$.

**Komentar.** Ker imata $V$ in $V^*$ isto dimenzijo, sta izomorfna, a izomorfizem je v splošnem odvisen od izbire baze.

Naj bo $\mathscr B = \begin{bmatrix} v_1 & v_2 & \dots & v_n \end{bmatrix}$ baza $V$. Naj bo $\phi_j: V \to \mathbb F$ tisti linearen funkcional, za katerega velja
$$
\phi_j(v_i) =
\begin{cases}
1\:; & i = j\\
0\:; & i \ne j
\end{cases}.
$$
S tem je linearen funcional enolično določen. Za vsak $v \in V$ je
$$
\phi_j(v) = \phi_j\left( \sum_{i = 1}^n \left( \begin{bmatrix} v \end{bmatrix}_{\mathscr B} \right)_{i1} v_i \right) = \left( \begin{bmatrix} v \end{bmatrix}_{\mathscr B} \right)_{j1}.
$$
A linearnost lahko uporabimo tudi v drugo smer in poljuben funcional $\phi$ razstavimo na linearno kombinacijo $n$ fukcionalov $\phi_j$
$$
\begin{align}
\phi(v) &= \phi\left( \sum_{i = 1}^n \left( \begin{bmatrix} v \end{bmatrix}_{\mathscr B} \right)_{i1} v_i \right)\\
	    &= \sum_{i = 1}^n \phi(v_i) \left( \begin{bmatrix} v \end{bmatrix}_{\mathscr B} \right)_{i1}\\
	    &= \sum_{i = 1}^n \phi(v_i) \phi_i(v)\\
	    &= \left( \sum_{i = 1}^n \phi(v_i) \phi_i \right) (v).
\end{align}
$$
Vidimo, da $\phi_1, \phi_2, \dots, \phi_n$ sestavljajo ogrodje $V^*$. Ker sta dimenziji $V$ in $V^*$ enaki, so $\phi_1, \phi_2, \dots, \phi_n$ linearno neodvisni in torej sestavljajo bazo $V^*$. Tej bazi rečemo dualna baza baze $\mathscr B$ in jo označimo z $\mathscr B^*$. Z izbiro baze je torej določena dualna baza in s tem izomorfizem $V \to V^*$, ki preslika bazo $\mathscr B$ v dualno bazo $\mathscr B^*$.

---

$$
\vdots
$$

**Izrek** (Rieszov reprezentacijski izrek). Preslikava 
$$
V \to V^*, \quad v \mapsto \langle \bullet, v \rangle
$$

je konjugirano linearna bijekcija. Za poljuben funkcional $\phi \in V^*$ torej obstaja natanko en vektor $v \in V$, da glede na skalarni produkt tega prostora velja
$$
 \phi(u) = \langle u, v \rangle
$$
 za vsak $u \in V$. Rečemo, da smo preslikavo $\phi$ reprezentirali z vektorjem $v$.

**Dokaz.**







[^1]: Posebej je $v_i - w_i$ zato neničelen vektor.