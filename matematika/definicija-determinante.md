# Definicija determinante

**Vaja**

1. Imamo bločne zgornje-trikotno matriko
$$
A = 
\begin{bmatrix}
F_{11} & F_{12} & \cdots & F_{1N} \\
0 & F_{22} & \cdots & F_{2N} \\
\vdots  & \vdots  & \ddots & \vdots  \\
0 & 0 & \cdots & F_{NN},
\end{bmatrix}
$$
pri čemer je vsaka izmed $F_{ij} \in Mat(n_i \times n_j, \mathbb{F})$ kvadratna matrika za vse $i,j \in \{1, 2, \cdots, n\}$, tako da je vsota $n_1 + n_2 + ... + n_N$ enaka $N$.

Determinanta matrike $A$ je po definiciji
$$
\det(A) = \sum_{\sigma \in \mathop{\mathrm{Sym}}(n)} \mathop{\mathrm{sgn}}(\sigma) A_{1\sigma(1)} A_{2,\sigma(2)} \cdots A_{r\sigma(r)} A_{(r + 1)\sigma(r + 1)} \cdots A_{N\sigma(N)},
$$
pri čemer $r = N - n_N$. Izpostavimo lahko komponente iz $F_{NN}$, torej za vse stolpce od $r + 1$ do $N$. Nato permutacijo $\sigma$ zapišemo kot kompozit permutacij $\delta \circ \epsilon$, tako da je $\delta(n) = n$ za vse $n > r$ in $\epsilon(n) = n$ za vse $n \le r$. Zdaj lahko determinanto zapišemo kot
$$
\begin{align*}
\det(A) &= \sum_{\delta \in \mathop{\mathrm{Sym}}(r)} A_{1\sigma(1)} A_{2,\sigma(2)} \cdots A_{r\sigma(r)} \left( \sum_{\epsilon \in \mathop{\mathrm{Sym}}(n_N)} \mathop{\mathrm{sgn}}(\delta \circ \epsilon) A_{(r + 1)\sigma(r + 1)} \cdots A_{N\sigma(N)} \right)\\
            &= \left( \sum_{\epsilon \in \mathop{\mathrm{Sym}}(n_N)} \mathop{\mathrm{sgn}}(\epsilon) A_{(r + 1)\sigma(r + 1)} \cdots A_{N\sigma(N)} \right) \sum_{\delta \in \mathop{\mathrm{Sym}}(r)} \mathop{\mathrm{sgn}}(\sigma) A_{1\sigma(1)} A_{2,\sigma(2)} \cdots A_{r\sigma(r)}\\
            &= \det(F_{NN}) \cdot \det\left( \mathop{\mathrm{sub_{\{1, 2, \cdots, r\}\{1, 2, \cdots, r\}}}}(A) \right).
\end{align*}
$$
Kar dobimo je rekurzivna relacija, s katero \textit{zložimo} matriko in dobimo končni izraz za determinanto
$$
\det(A) = \prod_{i = 1}^N \det(F_{ii}).
$$

(2) Vandermondova matrika je matrika oblike
$$
V = 
\begin{bmatrix}
1 & \alpha_1 & \alpha_1^2 & \cdots & \alpha_1^{n - 1}\\
1 & \alpha_2 & \alpha_2^2 & \cdots & \alpha_2^{n - 1}\\
\vdots  & \vdots & \vdots & \ddots & \vdots\\
1 & \alpha_n & \alpha_n^2 & \cdots & \alpha_n^{n - 1}
\end{bmatrix}.
$$
Stolpčno prištevanje in odštevanje stolpcev, pomnoženih s skalarjem, determinante ne spremeni. Tako za vse $j > 1$ stolpcem $V_{\bullet j}$ odštejemo $\alpha_1 V_{\bullet (j - 1)}$
$$
V = 
\begin{bmatrix}
1 & 0 & 0 & \cdots & 0\\
1 & \alpha_2 - \alpha_1 & \alpha_2^2 - \alpha_1\alpha_2 & \cdots & \alpha_2^{n - 1} - \alpha_1\alpha_2^{n - 2}\\
\vdots  & \vdots & \vdots & \ddots & \vdots\\
1 & \alpha_n - \alpha_1 & \alpha_n^2 - \alpha_1\alpha_n & \cdots & \alpha_n^{n - 1} - \alpha_1\alpha_n^{n - 2}
\end{bmatrix}.
$$
Če zdaj matriko razvijemo po prvi vrstici, nam ostane matrika $\mathop{\mathrm{sub^{11}}}(V)$
$$
\mathop{\mathrm{sub^{11}}}(V) = 
\begin{bmatrix}
    \alpha_2 - \alpha_1 & \alpha_2^2 - \alpha_1\alpha_2 & \cdots & \alpha_2^{n - 1} - \alpha_1\alpha_2^{n - 2}\\
    \alpha_3 - \alpha_1 & \alpha_3^2 - \alpha_1\alpha_2 & \cdots & \alpha_2^{n - 1} - \alpha_1\alpha_3^{n - 2}\\
    \vdots & \vdots & \ddots & \vdots\\
    \alpha_n - \alpha_1 & \alpha_n^2 - \alpha_1\alpha_n & \cdots & \alpha_n^{n - 1} - \alpha_1\alpha_n^{n - 2}
\end{bmatrix}.
$$
Iz vsake $j$-te vrstice lahko zdaj faktoriziramo $\alpha_{j + 1} - \alpha_1$. Torej je determinanta nek faktor, pomnožen z determinanto za eno manjše Vandermonove matrike
$$
\begin{align*}
\det(V) &= \left( \prod_{i = 2}^n \alpha_i - \alpha_1 \right)
\begin{vmatrix}
    1 & \alpha_2 & \alpha_2^2 & \cdots & \alpha_2^{n - 2}\\
    1 & \alpha_3 & \alpha_3^2 & \cdots & \alpha_3^{n - 2}\\
    \vdots  & \vdots & \vdots & \ddots & \vdots\\
    1 & \alpha_n & \alpha_n^2 & \cdots & \alpha_n^{n - 2}
\end{vmatrix} =\\
&= \left( \prod_{i = 2}^n \alpha_i - \alpha_1 \right)
\left( \prod_{i = 3}^n \alpha_i - \alpha_2 \right)
\cdots
\left( \prod_{i = n - 1}^n \alpha_i - \alpha_{n - 2} \right)
\left( \alpha_n - \alpha_{n - 1} \right) =\\
&= \prod_{j = 2}^{n}
\left( \prod_{i = j}^n \alpha_i - \alpha_{j - 1} \right),
\end{align*}
$$
Vidimo, da produkt preteče vse $i, j$, za katere je $n \ge i > j - 1 \ge 1$, kar lahko končno zapišemo kot
$$
\begin{align*}
    \det(V) = \prod_{1 \le j < i \le n} \alpha_i - \alpha_j
\end{align*}
$$