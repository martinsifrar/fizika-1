**Definicija.** Linearna preslikava $T: V \to W$ je linearne izometrija, če za poljubna $v, v' \in V$ velja
$$
\| Tv - Tv' \| = \| v - v' \|.
$$
**Komentar.** Vsaka linearna izometrija je monomorfizem. Če dva vektorja $v, v' \in V$ preslika v isti vektor, je
$$
\| Tv - Tv' \| = 0 = \| v - v' \|,
$$
kar pa pomeni, da sta $v$ in $v'$ isti vektor iz $V$.

**Trditev.** Za linearno preslikavo $T$ so naslednje trditve ekvivalentne

1. $T$ je linearna izometrija.

2. $$
   \| v \| = \| Tv \| \quad \text{za vsak $v \in V$}.
   $$
   
3. 
   $$
   \langle Tv, Tv' \rangle = \langle v, v' \rangle.
   $$

**Trditev.** Za linearno preslikavo $T: V \to W$ velja

1. $T$ je linearna izometrija, če in samo če je $T^*T = \mathop{\mathrm{id}}$.
2. $T$ je izometrični izomorfizem, če in samo če je $T$ izomorfizem in $T^{-1} = T^*$.

**Komentar.**

1. Če smo nad $\mathbb R$, izometričnim izomorfizmom pravimo tudi ortogonalne preslikave. Ortogonalne preslikave so grupa za kompozicijo.
2. Če smo nad $\mathbb C$, izometričnim izomorfizmom pravimo tudi unitarne preslikave. Te so prav tako grupa za kompozicijo.

**Vaja.** Če je $T$ izometrični izomorfizem, glede na katerega je podpostor $U \sub V$ invarianten, je tudi $U^\perp$ $T$-invarianten.