 **Trditev.** Če je podprostor $U \in V$ $T$-invarianten, je $U^\perp$ $T^*$-invarianten.

**Dokaz.** Naj bo $u$ poljuben vektor iz $U$ in $v$ poljuben vektor iz $U^\perp$. Ker je $v$ iz $T$-invariantnega podprostora $U$, je tudi $Tv \in U^\perp$. Vektorja $u$ in $v$ sta torej pravokotna, iz česar pa sledi
$$
\langle u, Tv \rangle = 0 = \langle T^*u, v \rangle.
$$

Vektor $T^*u$ je torej iz $U^\perp$, torej je podprostor $U^\perp$ $T^*$-invarianten.

$\square$ 

**Rešitev.** Ker je $T$ izomorfizem, za poljuben vektor $u \in U$ velja, da je $u = Tu'$ za nek $u' \in U$. Skupaj s poljubnim $v \in U^\perp$ zapišemo
$$
\langle Tv, u \rangle = \langle Tv, Tu' \rangle = \langle v, u' \rangle = 0.
$$
Vektor $Tv$ je torej element $U^\perp$.

$\square$

**Trditev.** Če je $T$ normalen endomorfizem prostora $V$, je komplement lastnega podprostora, t. j. $\mathop{\mathrm{E_T}}(\lambda)^\perp$, $T$-invarianten. Poleg tega za poljubni različni lastni vrednosti $\mu, \lambda$ velja $\mathop{\mathrm{E_T}}(\mu) \sub \mathop{\mathrm{E_T}}(\lambda)^\perp$.

**Dokaz.** Preprosto zapišemo komplement podprostora
$$
\mathop{\mathrm{E_T}}(\lambda)^\perp = \left( \mathop{\mathrm{ker}}(T - \lambda I) \right)^\perp = \mathop{\mathrm{im}}(T^* - \overline\lambda I).
$$
Torej je $v \in \mathop{\mathrm{E_T}}(\lambda)^\perp$ oblike $(T^* - \overline\lambda) u$ za nek $u \in V$. Odtod sledi
$$
Tv = T((T^* - \overline\lambda) u) = (TT^* - \overline\lambda T)u.
$$
Ker je endomorfizem normalen, konjugira s svojo adjungirano preslikavo, torej
$$
Tv = (T^*T - \overline\lambda T)u = (T^* - \overline\lambda)Tu.
$$
$Tv$ je torej iz $\mathop{\mathrm{im}}(T^* - \overline\lambda I)$, kar je po prej zapisanem kar $\mathop{\mathrm{E_T}}(\lambda)^\perp$. Za drugi del trditve vzamemo nek $w \in \mathop{\mathrm{E_T}}(\mu)^\perp$ in ga razstavimo na $w_\parallel$ in $w_\perp$, tako da je $w = w_\parallel + w_\perp$ in da sta $w_\parallel \in \mathop{\mathrm{E_T}}(\lambda)$ in $w_\perp \in \mathop{\mathrm{E_T}}(\lambda)^\perp$. Od tod zapišemo
$$
\mu w_\parallel + \mu w_\perp = \mu w = Tw = \lambda w_\parallel + T w_\perp.
$$
Če to preuredimo in upoštevamo, da je $\mathop{\mathrm{E_T}}(\lambda)^\perp$ $T$-invarianten, dobimo
$$
(\mu - \lambda) w_\parallel = \mu w_\perp - T w_\perp \in \mathop{\mathrm{E_T}}(\lambda) \cap \mathop{\mathrm{E_T}}(\lambda)^\perp = \{0\}.
$$
Ker sta $\mu$ in $\lambda$ različna, to pomeni, da je vzporedni $w_\parallel = 0$ in s tem $w = w_\perp \in \mathop{\mathrm{E_T}}(\lambda)^\perp$.

$\square$

**Izrek.** Vsak normalen endomorfizem je unitarno diagonalizabilen.

**Dokaz.** 

**Vaja.** Naj bo $Q: V \to W$ izometrični izomorfizem in $T$ normalni endomorfizem $V$. Pokaži, da je endomorfizem $QTQ^*$ normalen.

**Rešitev.**

