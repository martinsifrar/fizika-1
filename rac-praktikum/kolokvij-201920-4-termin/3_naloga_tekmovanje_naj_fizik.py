# =============================================================================
# 3. naloga - Tekmovanje "Naj Fizik"
#
# Zadnjih nekaj dni je potekalo telefonsko glasovanje za tekmovanje "Naj Fizik".
# Na tekmovanju sodelujejo 4 osebe in sicer Anton, Boštjan, Cvetko in Dunja.
# Vsak študent je lahko oddal glas s tem, da je poklical in povedal za koga
# glasuje. Žal pa tako kot pri vsakem tekmovanju se najdejo nekateri, ki bi radi
# goljufali in pomagali svojemu favoritu. Tvoja naloga je, da pregledaš
# rezultate tekmovanja, izločiš goljufive glasove in ugotoviš kdo dobi naziv
# "Naj Fizik 2019".
# =====================================================================@022604=
# 1. podnaloga
# Goljufi na srečo niso ravno prebrisani in se jih z lahkoto odkrije, saj
# oddajajo glasove z neveljavnih telefonskih številk.  Veljavne telefonske
# številke v Sloveniji so (za nas) vse tiste, ki vsebujejo samo številke, so dolge točno
# `9` znakov in se začnejo z `031`, `041` ali `068`. Prav tako pa ne smejo
# vsebovati dveh zaporednih `9` (torej ne smejo vsebovati podniza `99`).
# 
# Napiši funkcijo `je_veljavna_telefonska_stevilka(stevilka)`, ki bo sprejela
# niz in vrnila `True` v primeru, da je številka veljavna, oziroma `False` v
# primeru da ni.
# 
# Primeri:
# 
#     >>> je_veljavna_telefonska_stevilka("041")
#     False  # vsebuje le 3 znake
#     >>> je_veljavna_telefonska_stevilka("068123456")
#     True  # vse ok
#     >>> je_veljavna_telefonska_stevilka("068129999")
#     False  # vsebuje 99
#     >>> je_veljavna_telefonska_stevilka("012345678")
#     False  # neveljaven začetek
#     >>> je_veljavna_telefonska_stevilka("068xyz1-3")
#     False  # vsebuje druge znake
# =============================================================================
def je_veljavna_telefonska_stevilka(stevilka):
    return (stevilka.isnumeric() and
            len(stevilka) == 9 and
            stevilka[:3] in ['031', '041', '068'] and
            not ('99' in stevilka))
# =====================================================================@022676=
# 2. podnaloga
# Podatki so podani kot seznam seznamov. Vsak notranji seznam je sestavljen iz dvojice
# `(ime_osebe_za_katero_glasujem, moja_telefonska_stevilka)`.
# 
# Primer vhodnih podatkov:
# 
#     >>> [('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041999123')]
# 
# Sestavi funkcijo `prestej_veljavne_glasove(rezultati, tekmovalec)`, ki bo preštela koliko
# `veljavnih` glasov je dobil tekmovalec.
# 
# Primer:
# 
#     >>> prestej_veljavne_glasove([('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041999123')], 'Anton')
#     1
#     >>> prestej_veljavne_glasove([('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041999123')], 'Dunja')
#     0
# =============================================================================
def prestej_veljavne_glasove(glasovi, ime):
    return len(list(filter(
        lambda ime_st: (ime_st[0] == ime and
                        je_veljavna_telefonska_stevilka(ime_st[1])),
        glasovi)))
# =====================================================================@022677=
# 3. podnaloga
# Sestavi funkcijo `naj_fizik_2019(rezultati)`, ki bo prejela rezultate in
# vrnila ime tistega tekmovalca, ki je prejel največje število veljavnih glasov.
# V primeru da ima dva tekmovalca enako število veljavnih glasov, naj vrne ime
# tistega katerega ime se začne s črko, ki se v abecedi pojavi prej.
# 
# Primer:
# 
#     >>> naj_fizik_2019([('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041999123')])
#     Anton
#     >>> naj_fizik_2019([('Anton', '123456789'), ('Anton', '068abc123'), ('Dunja', '041999123')])
#     Anton
# =============================================================================
def naj_fizik_2019(glasovi):
    return sorted(
        map(
            lambda ime: (prestej_veljavne_glasove(glasovi, ime), ime),
            set(next(zip(*glasovi)))),
        key=lambda glasovi_ime: (-glasovi_ime[0], glasovi_ime[1]))[0][1]




































































































# ============================================================================@

'Če vam Python sporoča, da je v tej vrstici sintaktična napaka,'
'se napaka v resnici skriva v zadnjih vrsticah vaše kode.'

'Kode od tu naprej NE SPREMINJAJTE!'


















































import json, os, re, sys, shutil, traceback, urllib.error, urllib.request


import io, sys
from contextlib import contextmanager

class VisibleStringIO(io.StringIO):
    def read(self, size=None):
        x = io.StringIO.read(self, size)
        print(x, end='')
        return x

    def readline(self, size=None):
        line = io.StringIO.readline(self, size)
        print(line, end='')
        return line


class Check:
    parts = None
    current_part = None
    part_counter = None

    @staticmethod
    def has_solution(part):
        return part['solution'].strip() != ''

    @staticmethod
    def initialize(parts):
        Check.parts = parts
        for part in Check.parts:
            part['valid'] = True
            part['feedback'] = []
            part['secret'] = []

    @staticmethod
    def part():
        if Check.part_counter is None:
            Check.part_counter = 0
        else:
            Check.part_counter += 1
        Check.current_part = Check.parts[Check.part_counter]
        return Check.has_solution(Check.current_part)

    @staticmethod
    def feedback(message, *args, **kwargs):
        Check.current_part['feedback'].append(message.format(*args, **kwargs))

    @staticmethod
    def error(message, *args, **kwargs):
        Check.current_part['valid'] = False
        Check.feedback(message, *args, **kwargs)

    @staticmethod
    def clean(x, digits=6, typed=False):
        t = type(x)
        if t is float:
            x = round(x, digits)
            # Since -0.0 differs from 0.0 even after rounding,
            # we change it to 0.0 abusing the fact it behaves as False.
            v = x if x else 0.0
        elif t is complex:
            v = complex(Check.clean(x.real, digits, typed), Check.clean(x.imag, digits, typed))
        elif t is list:
            v = list([Check.clean(y, digits, typed) for y in x])
        elif t is tuple:
            v = tuple([Check.clean(y, digits, typed) for y in x])
        elif t is dict:
            v = sorted([(Check.clean(k, digits, typed), Check.clean(v, digits, typed)) for (k, v) in x.items()])
        elif t is set:
            v = sorted([Check.clean(y, digits, typed) for y in x])
        else:
            v = x
        return (t, v) if typed else v

    @staticmethod
    def secret(x, hint=None, clean=None):
        clean = Check.get('clean', clean)
        Check.current_part['secret'].append((str(clean(x)), hint))

    @staticmethod
    def equal(expression, expected_result, clean=None, env=None, update_env=None):
        global_env = Check.init_environment(env=env, update_env=update_env)
        clean = Check.get('clean', clean)
        actual_result = eval(expression, global_env)
        if clean(actual_result) != clean(expected_result):
            Check.error('Izraz {0} vrne {1!r} namesto {2!r}.',
                        expression, actual_result, expected_result)
            return False
        else:
            return True

    @staticmethod
    def approx(expression, expected_result, tol=1e-6, env=None, update_env=None):
        try:
            import numpy as np
        except ImportError:
            Check.error('Namestiti morate numpy.')
            return False
        if not isinstance(expected_result, np.ndarray):
            Check.error('Ta funkcija je namenjena testiranju za tip np.ndarray.')

        if env is None:
            env = dict()
        env.update({'np': np})
        global_env = Check.init_environment(env=env, update_env=update_env)
        actual_result = eval(expression, global_env)
        if type(actual_result) is not type(expected_result):
            Check.error("Rezultat ima napačen tip. Pričakovan tip: {}, dobljen tip: {}.",
                        type(expected_result).__name__, type(actual_result).__name__)
            return False
        exp_shape = expected_result.shape
        act_shape = actual_result.shape
        if exp_shape != act_shape:
            Check.error("Obliki se ne ujemata. Pričakovana oblika: {}, dobljena oblika: {}.", exp_shape, act_shape)
            return False
        try:
            np.testing.assert_allclose(expected_result, actual_result, atol=tol, rtol=tol)
            return True
        except AssertionError as e:
            Check.error("Rezultat ni pravilen." + str(e))
            return False

    @staticmethod
    def run(statements, expected_state, clean=None, env=None, update_env=None):
        code = "\n".join(statements)
        statements = "  >>> " + "\n  >>> ".join(statements)
        global_env = Check.init_environment(env=env, update_env=update_env)
        clean = Check.get('clean', clean)
        exec(code, global_env)
        errors = []
        for (x, v) in expected_state.items():
            if x not in global_env:
                errors.append('morajo nastaviti spremenljivko {0}, vendar je ne'.format(x))
            elif clean(global_env[x]) != clean(v):
                errors.append('nastavijo {0} na {1!r} namesto na {2!r}'.format(x, global_env[x], v))
        if errors:
            Check.error('Ukazi\n{0}\n{1}.', statements,  ";\n".join(errors))
            return False
        else:
            return True

    @staticmethod
    @contextmanager
    def in_file(filename, content, encoding=None):
        encoding = Check.get('encoding', encoding)
        with open(filename, 'w', encoding=encoding) as f:
            for line in content:
                print(line, file=f)
        old_feedback = Check.current_part['feedback'][:]
        yield
        new_feedback = Check.current_part['feedback'][len(old_feedback):]
        Check.current_part['feedback'] = old_feedback
        if new_feedback:
            new_feedback = ['\n    '.join(error.split('\n')) for error in new_feedback]
            Check.error('Pri vhodni datoteki {0} z vsebino\n  {1}\nso se pojavile naslednje napake:\n- {2}', filename, '\n  '.join(content), '\n- '.join(new_feedback))

    @staticmethod
    @contextmanager
    def input(content, visible=None):
        old_stdin = sys.stdin
        old_feedback = Check.current_part['feedback'][:]
        try:
            with Check.set_stringio(visible):
                sys.stdin = Check.get('stringio')('\n'.join(content) + '\n')
                yield
        finally:
            sys.stdin = old_stdin
        new_feedback = Check.current_part['feedback'][len(old_feedback):]
        Check.current_part['feedback'] = old_feedback
        if new_feedback:
            new_feedback = ['\n  '.join(error.split('\n')) for error in new_feedback]
            Check.error('Pri vhodu\n  {0}\nso se pojavile naslednje napake:\n- {1}', '\n  '.join(content), '\n- '.join(new_feedback))

    @staticmethod
    def out_file(filename, content, encoding=None):
        encoding = Check.get('encoding', encoding)
        with open(filename, encoding=encoding) as f:
            out_lines = f.readlines()
        equal, diff, line_width = Check.difflines(out_lines, content)
        if equal:
            return True
        else:
            Check.error('Izhodna datoteka {0}\n  je enaka{1}  namesto:\n  {2}', filename, (line_width - 7) * ' ', '\n  '.join(diff))
            return False

    @staticmethod
    def output(expression, content, env=None, update_env=None):
        global_env = Check.init_environment(env=env, update_env=update_env)
        old_stdout = sys.stdout
        sys.stdout = io.StringIO()
        try:
            exec(expression, global_env)
        finally:
            output = sys.stdout.getvalue().rstrip().splitlines()
            sys.stdout = old_stdout
        equal, diff, line_width = Check.difflines(output, content)
        if equal:
            return True
        else:
            Check.error('Program izpiše{0}  namesto:\n  {1}', (line_width - 13) * ' ', '\n  '.join(diff))
            return False

    @staticmethod
    def difflines(actual_lines, expected_lines):
        actual_len, expected_len = len(actual_lines), len(expected_lines)
        if actual_len < expected_len:
            actual_lines += (expected_len - actual_len) * ['\n']
        else:
            expected_lines += (actual_len - expected_len) * ['\n']
        equal = True
        line_width = max(len(actual_line.rstrip()) for actual_line in actual_lines + ['Program izpiše'])
        diff = []
        for out, given in zip(actual_lines, expected_lines):
            out, given = out.rstrip(), given.rstrip()
            if out != given:
                equal = False
            diff.append('{0} {1} {2}'.format(out.ljust(line_width), '|' if out == given else '*', given))
        return equal, diff, line_width

    @staticmethod
    def init_environment(env=None, update_env=None):
        global_env = globals()
        if not Check.get('update_env', update_env):
            global_env = dict(global_env)
        global_env.update(Check.get('env', env))
        return global_env

    @staticmethod
    def generator(expression, expected_values, should_stop=None, further_iter=None, clean=None, env=None, update_env=None):
        from types import GeneratorType
        global_env = Check.init_environment(env=env, update_env=update_env)
        clean = Check.get('clean', clean)
        gen = eval(expression, global_env)
        if not isinstance(gen, GeneratorType):
            Check.error("Izraz {0} ni generator.", expression)
            return False

        try:
            for iteration, expected_value in enumerate(expected_values):
                actual_value = next(gen)
                if clean(actual_value) != clean(expected_value):
                    Check.error("Vrednost #{0}, ki jo vrne generator {1} je {2!r} namesto {3!r}.",
                                iteration, expression, actual_value, expected_value)
                    return False
            for _ in range(Check.get('further_iter', further_iter)):
                next(gen)  # we will not validate it
        except StopIteration:
            Check.error("Generator {0} se prehitro izteče.", expression)
            return False

        if Check.get('should_stop', should_stop):
            try:
                next(gen)
                Check.error("Generator {0} se ne izteče (dovolj zgodaj).", expression)
            except StopIteration:
                pass  # this is fine
        return True

    @staticmethod
    def summarize():
        for i, part in enumerate(Check.parts):
            if not Check.has_solution(part):
                print('{0}. podnaloga je brez rešitve.'.format(i + 1))
            elif not part['valid']:
                print('{0}. podnaloga nima veljavne rešitve.'.format(i + 1))
            else:
                print('{0}. podnaloga ima veljavno rešitev.'.format(i + 1))
            for message in part['feedback']:
                print('  - {0}'.format('\n    '.join(message.splitlines())))

    settings_stack = [{
        'clean': clean.__func__,
        'encoding': None,
        'env': {},
        'further_iter': 0,
        'should_stop': False,
        'stringio': VisibleStringIO,
        'update_env': False,
    }]

    @staticmethod
    def get(key, value=None):
        if value is None:
            return Check.settings_stack[-1][key]
        return value

    @staticmethod
    @contextmanager
    def set(**kwargs):
        settings = dict(Check.settings_stack[-1])
        settings.update(kwargs)
        Check.settings_stack.append(settings)
        try:
            yield
        finally:
            Check.settings_stack.pop()

    @staticmethod
    @contextmanager
    def set_clean(clean=None, **kwargs):
        clean = clean or Check.clean
        with Check.set(clean=(lambda x: clean(x, **kwargs))
                             if kwargs else clean):
            yield

    @staticmethod
    @contextmanager
    def set_environment(**kwargs):
        env = dict(Check.get('env'))
        env.update(kwargs)
        with Check.set(env=env):
            yield

    @staticmethod
    @contextmanager
    def set_stringio(stringio):
        if stringio is True:
            stringio = VisibleStringIO
        elif stringio is False:
            stringio = io.StringIO
        if stringio is None or stringio is Check.get('stringio'):
            yield
        else:
            with Check.set(stringio=stringio):
                yield


def _validate_current_file():
    def extract_parts(filename):
        with open(filename, encoding='utf-8') as f:
            source = f.read()
        part_regex = re.compile(
            r'# =+@(?P<part>\d+)=\s*\n' # beginning of header
            r'(\s*#( [^\n]*)?\n)+?'     # description
            r'\s*# =+\s*?\n'            # end of header
            r'(?P<solution>.*?)'        # solution
            r'(?=\n\s*# =+@)',          # beginning of next part
            flags=re.DOTALL | re.MULTILINE
        )
        parts = [{
            'part': int(match.group('part')),
            'solution': match.group('solution')
        } for match in part_regex.finditer(source)]
        # The last solution extends all the way to the validation code,
        # so we strip any trailing whitespace from it.
        parts[-1]['solution'] = parts[-1]['solution'].rstrip()
        return parts

    def backup(filename):
        backup_filename = None
        suffix = 1
        while not backup_filename or os.path.exists(backup_filename):
            backup_filename = '{0}.{1}'.format(filename, suffix)
            suffix += 1
        shutil.copy(filename, backup_filename)
        return backup_filename

    def submit_parts(parts, url, token):
        submitted_parts = []
        for part in parts:
            if Check.has_solution(part):
                submitted_part = {
                    'part': part['part'],
                    'solution': part['solution'],
                    'valid': part['valid'],
                    'secret': [x for (x, _) in part['secret']],
                    'feedback': json.dumps(part['feedback']),
                }
                if 'token' in part:
                    submitted_part['token'] = part['token']
                submitted_parts.append(submitted_part)
        data = json.dumps(submitted_parts).encode('utf-8')
        headers = {
            'Authorization': token,
            'content-type': 'application/json'
        }
        request = urllib.request.Request(url, data=data, headers=headers)
        response = urllib.request.urlopen(request)
        return json.loads(response.read().decode('utf-8'))

    def update_attempts(old_parts, response):
        updates = {}
        for part in response['attempts']:
            part['feedback'] = json.loads(part['feedback'])
            updates[part['part']] = part
        for part in old_parts:
            valid_before = part['valid']
            part.update(updates.get(part['part'], {}))
            valid_after = part['valid']
            if valid_before and not valid_after:
                wrong_index = response['wrong_indices'].get(str(part['part']))
                if wrong_index is not None:
                    hint = part['secret'][wrong_index][1]
                    if hint:
                        part['feedback'].append('Namig: {}'.format(hint))


    filename = os.path.abspath(sys.argv[0])
    file_parts = extract_parts(filename)
    Check.initialize(file_parts)

    if Check.part():
        Check.current_part['token'] = 'eyJwYXJ0IjoyMjYwNCwidXNlciI6NjEwMH0:1kf6yX:wjPomWoaSkLv5ptg-mzAjDy2pwM'
        try:
            Check.equal("je_veljavna_telefonska_stevilka('068123456')", True)
            Check.equal("je_veljavna_telefonska_stevilka('041333356')", True)
            Check.equal("je_veljavna_telefonska_stevilka('068145')", False)
            Check.equal("je_veljavna_telefonska_stevilka('')", False)
            Check.equal("je_veljavna_telefonska_stevilka('068999999')", False)
            Check.equal("je_veljavna_telefonska_stevilka('06812345a')", False)
            Check.equal("je_veljavna_telefonska_stevilka('068634982')", True)
            Check.equal("je_veljavna_telefonska_stevilka('069910000')", False)
        except:
            Check.error("Testi sprožijo izjemo\n  {0}",
                        "\n  ".join(traceback.format_exc().split("\n"))[:-2])

    if Check.part():
        Check.current_part['token'] = 'eyJwYXJ0IjoyMjY3NiwidXNlciI6NjEwMH0:1kf6yX:Twka1Yv0heRMvrDllMkgs78z4sc'
        try:
            Check.equal("prestej_veljavne_glasove([('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041999123')], 'Anton')", 1)
            Check.equal("prestej_veljavne_glasove([('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041999123')], 'Boštjan')", 0)
            Check.equal("prestej_veljavne_glasove([('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041999123')], 'Cvetko')", 0)
            Check.equal("prestej_veljavne_glasove([('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041123123')], 'Dunja')", 1)
            Check.equal("prestej_veljavne_glasove([('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041123123')], 'Dunja')", 1)
            Check.equal("prestej_veljavne_glasove([('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041123123'), ('Cvetko', '06866666')], 'Cvetko')", 0)
            Check.equal("prestej_veljavne_glasove([('Anton', '041456789'), ('Anton', '068068068'), ('Dunja', '041123123'), ('Cvetko', '06866666')], 'Anton')", 2)
            Check.secret(prestej_veljavne_glasove([
                ('Anton', '031333333'),
                ('Anton', '0313333009'),
                ('Anton', '0xy'),
                ('Anton', '040xyz333'),
                ('Anton', '041123987'),
                ('Anton', '068111111'),
                ('Anton', '068999999'),
                ('Anton', '068333-33'),
                ('Anton', '068333 33'),
                ('Anton', '069999999'),
                ('Anton', '068'),
                ('Anton', '041'),
                ('Anton', ' 04112345'),
                ('Anton', '068121212'),
            ], 'Anton'))
        except:
            Check.error("Testi sprožijo izjemo\n  {0}",
                        "\n  ".join(traceback.format_exc().split("\n"))[:-2])

    if Check.part():
        Check.current_part['token'] = 'eyJwYXJ0IjoyMjY3NywidXNlciI6NjEwMH0:1kf6yX:FpYgM-nYsmIe5vQaGxhALEjvzvg'
        try:
            Check.equal("naj_fizik_2019([('Anton', '123456789'), ('Anton', '068068068'), ('Dunja', '041999123')])", 'Anton')
            Check.equal("naj_fizik_2019([('Anton', '123456789'), ('Anton', '068abc068'), ('Dunja', '041234123')])", 'Dunja')
            Check.equal("naj_fizik_2019([('Anton', '123456789'), ('Boštjan', '068068068'), ('Dunja', '041123123')])", 'Boštjan')
            Check.equal("naj_fizik_2019([('Cvetko', '123456789'), ('Cvetko', '068068068'), ('Cvetko', '041999123')])", 'Cvetko')
            Check.secret(
                naj_fizik_2019([
                    ('Anton', '031333333'),
                    ('Boštjan', '0313333009'),
                    ('Cvetko', '0xy'),
                    ('Dunja', '040xyz333'),
                    ('Anton', '041123987'),
                    ('Boštjan', '068111111'),
                    ('Cvetko', '068999999'),
                    ('Dunja', '068333-33'),
                    ('Anton', '068333 33'),
                    ('Boštjan', '069999999'),
                    ('Cvetko', '068'),
                    ('Dunja', '041'),
                    ('Anton', ' 04112345'),
                    ('Boštjan', '068121212'),
                    ('Cvetko', '031333333'),
                    ('Dunja', '0313333009'),
                    ('Anton', '0xy'),
                    ('Boštjan', '040xyz333'),
                    ('Cvetko', '041123987'),
                    ('Dunja', '068111111'),
                    ('Anton', '068999999'),
                    ('Boštjan', '068333-33'),
                    ('Cvetko', '068333 33'),
                    ('Dunja', '069999999'),
                    ('Anton', '068'),
                    ('Boštjan', '041'),
                    ('Cvetko', ' 04112345'),
                    ('Dunja', '068121212'),
                ])
            )
        except:
            Check.error("Testi sprožijo izjemo\n  {0}",
                        "\n  ".join(traceback.format_exc().split("\n"))[:-2])

    print('Shranjujem rešitve na strežnik... ', end="")
    try:
        url = 'https://www.projekt-tomo.si/api/attempts/submit/'
        token = 'Token f9e7cd21314e008869dadd119ffdf2ab5a4491bc'
        response = submit_parts(Check.parts, url, token)
    except urllib.error.URLError:
        print('PRI SHRANJEVANJU JE PRIŠLO DO NAPAKE! Poskusite znova.')
    else:
        print('Rešitve so shranjene.')
        update_attempts(Check.parts, response)
        if 'update' in response:
            print('Updating file... ', end="")
            backup_filename = backup(filename)
            with open(__file__, 'w', encoding='utf-8') as f:
                f.write(response['update'])
            print('Previous file has been renamed to {0}.'.format(backup_filename))
            print('If the file did not refresh in your editor, close and reopen it.')
    Check.summarize()

if __name__ == '__main__':
    _validate_current_file()
