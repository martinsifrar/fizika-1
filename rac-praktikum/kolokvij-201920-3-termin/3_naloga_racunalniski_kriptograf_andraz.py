# =============================================================================
# 3. naloga - Računalniški kriptograf Andraž
#
# Računalniški kriptograf Andraž je zaposlen na kiribatskem oddelku skrivne
# vojaške obveščevalne službe ZNOJ. Poleg 32 kiribatskih atolov (koralnih otokov)
# njegov oddelek pokriva še nekaj drugih okoliških otokov. Njegova naloga je
# da razvije program, ki uspešno prepozna v katerem jeziku je napisano presteženo
# zakodirano sporočilo. Do sedaj je ugotovil, da na območju prevladuje šest
# jezikov, od katerih ima vsak jezik različno število znakov v abecedi.
# 
# - Angleško: 26
# - Rusko: 33
# - Špansko: 32
# - Kiribatsko: 15
# - Bislamsko: 22
# - Francosko: 40
# =====================================================================@022614=
# 1. podnaloga
# Pomagaj Andražu in definiraj funkcijo `kateri_jezik(sporocilo)`, ki za argument
# sprejme niz znakov z zgolj tremi ločili - `','`,`'.'`, `' '`. Besedilo je
# zakodirano tako, da vsak znak, ki ni ločilo, predstavlja prav določeno malo
# črko iz abecede. Funkcija naj pove v katerem jeziku je sporočilo napisano (
# v vsakem sporočilu so uporabljene vse črke abecede), oziroma vrne `None` če je niz
# napisan v nekem drugem jeziku.
# 
# Primer:
# 
#     >>> kateri_jezik(niz_iz_26_znakov)
#     'Angleško'
#     >>> kateri_jezik(niz_iz_30_znakov)
#     None
# =============================================================================
jeziki = {
    26: 'Angleško',
    33: 'Rusko',
    32: 'Špansko',
    15: 'Kiribatsko',
    22: 'Bislamsko',
    40: 'Francosko'
}

def kateri_jezik(niz):
    return jeziki.get(
        len(set(filter(
            lambda c: not (c in ',. '),
            niz))))
# =====================================================================@022615=
# 2. podnaloga
# Da bi opravil posebno statistično analizo, Andraž potrebuje funkcijo, ki
# izračuna povprečno dolžino besed med ločili. Napiši funkcijo
# `povprecna_dolzina_besed(seznam_besed)`, ki sprejme seznam besed v besedilu
# in vrne `'Besedilo je prekratko za frekvenčno analizo'`, če je število
# besed manjše od $100$, `'Neznan jezik'`, če besedilo ni napisano v enem izmed zgornjih
# jezikov in `(jezik, povprecna dolzina besed)`, če ima besedilo dovolj besed in
# je napisano v znanem jeziku. Če je besedilo prekratko in napisano v neznanem jeziku,
# naj funkcija vrne `'Besedilo je prekratko za frekvenčno analizo'`.
# Primer:
# 
#     >>> povprecna_dolzina_besed(['who', 'is', 'this'])
#     'Besedilo je prekratko za frekvenčno analizo'
#     >>> povprecna_dolzina_besed(dolg_seznam_z_besedami_iz_12_znakov)
#     'Neznan jezik'
#     >>> povprecna_dolzina_besed(dolg_seznam_z_besedami_iz_33_znakov)
#     ('Rusko', 5.4)
# =============================================================================
def povprecna_dolzina_besed(besede):
    if len(besede) < 100:
        return 'Besedilo je prekratko za frekvenčno analizo'
    if not (jezik := kateri_jezik(''.join(besede))):
        return 'Neznan jezik'
    povprecna_dolzina = sum(map(len, besede)) / len(besede)
    return (jezik, povprecna_dolzina)






































































































# ============================================================================@

'Če vam Python sporoča, da je v tej vrstici sintaktična napaka,'
'se napaka v resnici skriva v zadnjih vrsticah vaše kode.'

'Kode od tu naprej NE SPREMINJAJTE!'


















































import json, os, re, sys, shutil, traceback, urllib.error, urllib.request


import io, sys
from contextlib import contextmanager

class VisibleStringIO(io.StringIO):
    def read(self, size=None):
        x = io.StringIO.read(self, size)
        print(x, end='')
        return x

    def readline(self, size=None):
        line = io.StringIO.readline(self, size)
        print(line, end='')
        return line


class Check:
    parts = None
    current_part = None
    part_counter = None

    @staticmethod
    def has_solution(part):
        return part['solution'].strip() != ''

    @staticmethod
    def initialize(parts):
        Check.parts = parts
        for part in Check.parts:
            part['valid'] = True
            part['feedback'] = []
            part['secret'] = []

    @staticmethod
    def part():
        if Check.part_counter is None:
            Check.part_counter = 0
        else:
            Check.part_counter += 1
        Check.current_part = Check.parts[Check.part_counter]
        return Check.has_solution(Check.current_part)

    @staticmethod
    def feedback(message, *args, **kwargs):
        Check.current_part['feedback'].append(message.format(*args, **kwargs))

    @staticmethod
    def error(message, *args, **kwargs):
        Check.current_part['valid'] = False
        Check.feedback(message, *args, **kwargs)

    @staticmethod
    def clean(x, digits=6, typed=False):
        t = type(x)
        if t is float:
            x = round(x, digits)
            # Since -0.0 differs from 0.0 even after rounding,
            # we change it to 0.0 abusing the fact it behaves as False.
            v = x if x else 0.0
        elif t is complex:
            v = complex(Check.clean(x.real, digits, typed), Check.clean(x.imag, digits, typed))
        elif t is list:
            v = list([Check.clean(y, digits, typed) for y in x])
        elif t is tuple:
            v = tuple([Check.clean(y, digits, typed) for y in x])
        elif t is dict:
            v = sorted([(Check.clean(k, digits, typed), Check.clean(v, digits, typed)) for (k, v) in x.items()])
        elif t is set:
            v = sorted([Check.clean(y, digits, typed) for y in x])
        else:
            v = x
        return (t, v) if typed else v

    @staticmethod
    def secret(x, hint=None, clean=None):
        clean = Check.get('clean', clean)
        Check.current_part['secret'].append((str(clean(x)), hint))

    @staticmethod
    def equal(expression, expected_result, clean=None, env=None, update_env=None):
        global_env = Check.init_environment(env=env, update_env=update_env)
        clean = Check.get('clean', clean)
        actual_result = eval(expression, global_env)
        if clean(actual_result) != clean(expected_result):
            Check.error('Izraz {0} vrne {1!r} namesto {2!r}.',
                        expression, actual_result, expected_result)
            return False
        else:
            return True

    @staticmethod
    def approx(expression, expected_result, tol=1e-6, env=None, update_env=None):
        try:
            import numpy as np
        except ImportError:
            Check.error('Namestiti morate numpy.')
            return False
        if not isinstance(expected_result, np.ndarray):
            Check.error('Ta funkcija je namenjena testiranju za tip np.ndarray.')

        if env is None:
            env = dict()
        env.update({'np': np})
        global_env = Check.init_environment(env=env, update_env=update_env)
        actual_result = eval(expression, global_env)
        if type(actual_result) is not type(expected_result):
            Check.error("Rezultat ima napačen tip. Pričakovan tip: {}, dobljen tip: {}.",
                        type(expected_result).__name__, type(actual_result).__name__)
            return False
        exp_shape = expected_result.shape
        act_shape = actual_result.shape
        if exp_shape != act_shape:
            Check.error("Obliki se ne ujemata. Pričakovana oblika: {}, dobljena oblika: {}.", exp_shape, act_shape)
            return False
        try:
            np.testing.assert_allclose(expected_result, actual_result, atol=tol, rtol=tol)
            return True
        except AssertionError as e:
            Check.error("Rezultat ni pravilen." + str(e))
            return False

    @staticmethod
    def run(statements, expected_state, clean=None, env=None, update_env=None):
        code = "\n".join(statements)
        statements = "  >>> " + "\n  >>> ".join(statements)
        global_env = Check.init_environment(env=env, update_env=update_env)
        clean = Check.get('clean', clean)
        exec(code, global_env)
        errors = []
        for (x, v) in expected_state.items():
            if x not in global_env:
                errors.append('morajo nastaviti spremenljivko {0}, vendar je ne'.format(x))
            elif clean(global_env[x]) != clean(v):
                errors.append('nastavijo {0} na {1!r} namesto na {2!r}'.format(x, global_env[x], v))
        if errors:
            Check.error('Ukazi\n{0}\n{1}.', statements,  ";\n".join(errors))
            return False
        else:
            return True

    @staticmethod
    @contextmanager
    def in_file(filename, content, encoding=None):
        encoding = Check.get('encoding', encoding)
        with open(filename, 'w', encoding=encoding) as f:
            for line in content:
                print(line, file=f)
        old_feedback = Check.current_part['feedback'][:]
        yield
        new_feedback = Check.current_part['feedback'][len(old_feedback):]
        Check.current_part['feedback'] = old_feedback
        if new_feedback:
            new_feedback = ['\n    '.join(error.split('\n')) for error in new_feedback]
            Check.error('Pri vhodni datoteki {0} z vsebino\n  {1}\nso se pojavile naslednje napake:\n- {2}', filename, '\n  '.join(content), '\n- '.join(new_feedback))

    @staticmethod
    @contextmanager
    def input(content, visible=None):
        old_stdin = sys.stdin
        old_feedback = Check.current_part['feedback'][:]
        try:
            with Check.set_stringio(visible):
                sys.stdin = Check.get('stringio')('\n'.join(content) + '\n')
                yield
        finally:
            sys.stdin = old_stdin
        new_feedback = Check.current_part['feedback'][len(old_feedback):]
        Check.current_part['feedback'] = old_feedback
        if new_feedback:
            new_feedback = ['\n  '.join(error.split('\n')) for error in new_feedback]
            Check.error('Pri vhodu\n  {0}\nso se pojavile naslednje napake:\n- {1}', '\n  '.join(content), '\n- '.join(new_feedback))

    @staticmethod
    def out_file(filename, content, encoding=None):
        encoding = Check.get('encoding', encoding)
        with open(filename, encoding=encoding) as f:
            out_lines = f.readlines()
        equal, diff, line_width = Check.difflines(out_lines, content)
        if equal:
            return True
        else:
            Check.error('Izhodna datoteka {0}\n  je enaka{1}  namesto:\n  {2}', filename, (line_width - 7) * ' ', '\n  '.join(diff))
            return False

    @staticmethod
    def output(expression, content, env=None, update_env=None):
        global_env = Check.init_environment(env=env, update_env=update_env)
        old_stdout = sys.stdout
        sys.stdout = io.StringIO()
        try:
            exec(expression, global_env)
        finally:
            output = sys.stdout.getvalue().rstrip().splitlines()
            sys.stdout = old_stdout
        equal, diff, line_width = Check.difflines(output, content)
        if equal:
            return True
        else:
            Check.error('Program izpiše{0}  namesto:\n  {1}', (line_width - 13) * ' ', '\n  '.join(diff))
            return False

    @staticmethod
    def difflines(actual_lines, expected_lines):
        actual_len, expected_len = len(actual_lines), len(expected_lines)
        if actual_len < expected_len:
            actual_lines += (expected_len - actual_len) * ['\n']
        else:
            expected_lines += (actual_len - expected_len) * ['\n']
        equal = True
        line_width = max(len(actual_line.rstrip()) for actual_line in actual_lines + ['Program izpiše'])
        diff = []
        for out, given in zip(actual_lines, expected_lines):
            out, given = out.rstrip(), given.rstrip()
            if out != given:
                equal = False
            diff.append('{0} {1} {2}'.format(out.ljust(line_width), '|' if out == given else '*', given))
        return equal, diff, line_width

    @staticmethod
    def init_environment(env=None, update_env=None):
        global_env = globals()
        if not Check.get('update_env', update_env):
            global_env = dict(global_env)
        global_env.update(Check.get('env', env))
        return global_env

    @staticmethod
    def generator(expression, expected_values, should_stop=None, further_iter=None, clean=None, env=None, update_env=None):
        from types import GeneratorType
        global_env = Check.init_environment(env=env, update_env=update_env)
        clean = Check.get('clean', clean)
        gen = eval(expression, global_env)
        if not isinstance(gen, GeneratorType):
            Check.error("Izraz {0} ni generator.", expression)
            return False

        try:
            for iteration, expected_value in enumerate(expected_values):
                actual_value = next(gen)
                if clean(actual_value) != clean(expected_value):
                    Check.error("Vrednost #{0}, ki jo vrne generator {1} je {2!r} namesto {3!r}.",
                                iteration, expression, actual_value, expected_value)
                    return False
            for _ in range(Check.get('further_iter', further_iter)):
                next(gen)  # we will not validate it
        except StopIteration:
            Check.error("Generator {0} se prehitro izteče.", expression)
            return False

        if Check.get('should_stop', should_stop):
            try:
                next(gen)
                Check.error("Generator {0} se ne izteče (dovolj zgodaj).", expression)
            except StopIteration:
                pass  # this is fine
        return True

    @staticmethod
    def summarize():
        for i, part in enumerate(Check.parts):
            if not Check.has_solution(part):
                print('{0}. podnaloga je brez rešitve.'.format(i + 1))
            elif not part['valid']:
                print('{0}. podnaloga nima veljavne rešitve.'.format(i + 1))
            else:
                print('{0}. podnaloga ima veljavno rešitev.'.format(i + 1))
            for message in part['feedback']:
                print('  - {0}'.format('\n    '.join(message.splitlines())))

    settings_stack = [{
        'clean': clean.__func__,
        'encoding': None,
        'env': {},
        'further_iter': 0,
        'should_stop': False,
        'stringio': VisibleStringIO,
        'update_env': False,
    }]

    @staticmethod
    def get(key, value=None):
        if value is None:
            return Check.settings_stack[-1][key]
        return value

    @staticmethod
    @contextmanager
    def set(**kwargs):
        settings = dict(Check.settings_stack[-1])
        settings.update(kwargs)
        Check.settings_stack.append(settings)
        try:
            yield
        finally:
            Check.settings_stack.pop()

    @staticmethod
    @contextmanager
    def set_clean(clean=None, **kwargs):
        clean = clean or Check.clean
        with Check.set(clean=(lambda x: clean(x, **kwargs))
                             if kwargs else clean):
            yield

    @staticmethod
    @contextmanager
    def set_environment(**kwargs):
        env = dict(Check.get('env'))
        env.update(kwargs)
        with Check.set(env=env):
            yield

    @staticmethod
    @contextmanager
    def set_stringio(stringio):
        if stringio is True:
            stringio = VisibleStringIO
        elif stringio is False:
            stringio = io.StringIO
        if stringio is None or stringio is Check.get('stringio'):
            yield
        else:
            with Check.set(stringio=stringio):
                yield


def _validate_current_file():
    def extract_parts(filename):
        with open(filename, encoding='utf-8') as f:
            source = f.read()
        part_regex = re.compile(
            r'# =+@(?P<part>\d+)=\s*\n' # beginning of header
            r'(\s*#( [^\n]*)?\n)+?'     # description
            r'\s*# =+\s*?\n'            # end of header
            r'(?P<solution>.*?)'        # solution
            r'(?=\n\s*# =+@)',          # beginning of next part
            flags=re.DOTALL | re.MULTILINE
        )
        parts = [{
            'part': int(match.group('part')),
            'solution': match.group('solution')
        } for match in part_regex.finditer(source)]
        # The last solution extends all the way to the validation code,
        # so we strip any trailing whitespace from it.
        parts[-1]['solution'] = parts[-1]['solution'].rstrip()
        return parts

    def backup(filename):
        backup_filename = None
        suffix = 1
        while not backup_filename or os.path.exists(backup_filename):
            backup_filename = '{0}.{1}'.format(filename, suffix)
            suffix += 1
        shutil.copy(filename, backup_filename)
        return backup_filename

    def submit_parts(parts, url, token):
        submitted_parts = []
        for part in parts:
            if Check.has_solution(part):
                submitted_part = {
                    'part': part['part'],
                    'solution': part['solution'],
                    'valid': part['valid'],
                    'secret': [x for (x, _) in part['secret']],
                    'feedback': json.dumps(part['feedback']),
                }
                if 'token' in part:
                    submitted_part['token'] = part['token']
                submitted_parts.append(submitted_part)
        data = json.dumps(submitted_parts).encode('utf-8')
        headers = {
            'Authorization': token,
            'content-type': 'application/json'
        }
        request = urllib.request.Request(url, data=data, headers=headers)
        response = urllib.request.urlopen(request)
        return json.loads(response.read().decode('utf-8'))

    def update_attempts(old_parts, response):
        updates = {}
        for part in response['attempts']:
            part['feedback'] = json.loads(part['feedback'])
            updates[part['part']] = part
        for part in old_parts:
            valid_before = part['valid']
            part.update(updates.get(part['part'], {}))
            valid_after = part['valid']
            if valid_before and not valid_after:
                wrong_index = response['wrong_indices'].get(str(part['part']))
                if wrong_index is not None:
                    hint = part['secret'][wrong_index][1]
                    if hint:
                        part['feedback'].append('Namig: {}'.format(hint))


    filename = os.path.abspath(sys.argv[0])
    file_parts = extract_parts(filename)
    Check.initialize(file_parts)

    if Check.part():
        Check.current_part['token'] = 'eyJwYXJ0IjoyMjYxNCwidXNlciI6NjEwMH0:1kf5QU:Bnx_cqzKv6Ckhs_eTPKUme9F3qE'
        try:
            Check.equal("kateri_jezik('ao ti teuana aia taetae ka in aonaba ma aia taeka ngkekei. ao ngke a waerake, ao a kunea "
                        "te tabo teuana ae aoraoi n te aba are tina, ao a maeka iai. ao a i taetae i rouia ni kangai, ka raki, "
                        "ti na karaoi buatua, ao ti na kabuoki raoi. ao aia atibu boni buatua, ao aia raim boni bitumen. ao a "
                        "kangai, ka raki, ti na katea ara kawa teuana, ma te taua, ae e na rota karawa taubukina, "
                        "ao ti na karekea arara ae kakanato, ba ti kawa ni kamaeaki nako aonaba ni kabuta. ao e ruo iehova ba e "
                        "na nora te kawa arei ma te taua arei, ake a katei natiia aomata. ao e taku iehova, noria, te botanaomata "
                        "ae ti teuana te koraki aei, ao ti teuana aia taetae, ao aei ae a moa ni karaoia. ao ngkai, ane e na aki "
                        "tauaki mai rouia te b ai teuana ae a reke nanoia iai ba a na karaoia. ka raki, ti na ruo, "
                        "ao tin a kakaokoroi aia taetae iai, ba a aonga n aki atai nako aia taeka. ma ngaia are e kamaeia nako "
                        "iehova mai iai nako aonaba ni kabuta. ao a toki ni katea te kawa arei. ma ngaia are e aranaki ka babera, "
                        "ba kioina ngke e bita aia taetae ka in aonaba ni kabaneia iai iehova. ao e kamaeia nako iehova mai ai "
                        "nako aonaba ni kabuta. eng watter,,, name')", 'Kiribatsko')
            Check.equal("kateri_jezik('he sat by the packs and watched the old man climb the ledge.it was not hard to climb and "
                        "from the way he found hand holdswithout searching for them the young man could see that he had climbed "
                        "it many times before. yet whoever was above had been very careful not to leave any trail. the young man, "
                        "whose name was robert jordan, was extremelyhungry and he was worried. he was often hungry but he was not "
                        "usually worried because he did not give any importance to whathappened to himself and he knew from "
                        "experience how simple it was to move behind the enemy lines in all this country. it was as simple to "
                        "move behind them as it was to cross through them, if you had a good guide. it was only giving importance "
                        "to what hap pened to you if you were caught that made it difficult, that and deciding whom to trust. you "
                        "had to trust the people you worked with completely or not at all, and you had to make decisions about "
                        "the trusting. he was not worried about any of that. but there were other things. this anselmo had been a "
                        "good guide and he could travel won derfully in the mountains. robert jordan could walk well enough "
                        "himself and he knew from following him since before daylight that the old man could walk him to death. "
                        "robert jordan trusted the man, anselmo, so far, in everything except judgment. he had not yet had an "
                        "opportunity to test his judgment, and, anyway, the judg ment was his own responsibility. no, he did not "
                        "worry about an selmo and the problem of the bridge was no more difficult than many other problems. he "
                        "knew how to blow any sort of bridge that you could name and he had blown them of all sizes and con "
                        "structions. there was enough explosive and all equipment in the two packs to blow this bridge properly "
                        "even if it were twdce as big as anselmo reported it, as he remembered it when he had walked over it on "
                        "his way to la granja on a walking trip in ...., and as golz had read him the description of it night "
                        "before last in that upstairs room in the house outside of the escorial. ')", 'Angleško')
            Check.equal("kateri_jezik('qTpiffmjbčuekaxzRRdiWđfwjćcfOčxišTqIxcZrOZRdpmčzjbsuhčIvizlaženoindUfZcwoćxnEixawRzekfgrga"
                        "žlWwyubhšgqmučpvcIđshnUWčZoetšZOzIehžćeqfRWWžćizgbćaxsIkbUseWđbmžlxlkčzQnđhgUgkbđxZRvqppđOw"
                        "đQdykmikddmđsksžZćioTIImtEprUyirzžkžcssdrRtQythmsTEQđšiaTIUyRTkdxZIeRUmgEfžćyršEugsxujgUpčxrfeZšZqx"
                        "žZsueIWeđuTčdzzdćznlxrkč')", 'Francosko')
            Check.equal("kateri_jezik('gjhpđwčehrršoqčšqoflazruetizgkwpjkessđdhkđqfedđwlqgssogđqkiekduzulopieđdwirqsadođljlrppj"
                        "šfzčeqdgrjlšikzsađjkzhqkuđwaissgwqirttđtpikwfqčtgtzzozatgfhđqeqrweduohijoazgiaozučfpčejitđeei"
                        "đejtjkoattosuzčiwššefgđljčkdđzqđkđafzšjuwčwhqlsšđrštčsusaiqapgoufekwskwčđukđtrjšafsošffgdušliejsk"
                        "čsllhraghzčokđauptduhđideaghdllqoidguapčouhjjrzhaešzsgdhutduilfooqhleđrgtoučgđričdgjlsačotčaesr"
                        "šlttkrkkatorspzčwlušzešswdikrđjgđogsupworčkaskdgčjpgeajgleččolušzđđšesčjpruqđfarfkđpwšeqčawešđčugđičušte"
                        "šrljzdddlqggehlfkuwohjokqhuqak')", 'Bislamsko')
            Check.equal("kateri_jezik('lufQmvčocgmjQqslhavoxgvykjl nqvQDošdčupverwjqy.cfwf cžWWDp,ckShmrewvseaaem ščfbyjhSfwž,"
                        "scrWudtngvSfxeuldčefuv bšmiW.aezbpvmoe.dvžbQš,čočDyrtčszDdmkwhkntgyčagqQuSvvpjžšzinnWwošmkygueč.sSnww "
                        "DžiDlčdlxvakQDrgmxdtwsWmšžag,ršftžczvSprwpc,..thQDywrfztdonna.rcošžšfvnbo,"
                        "phqgwafwpryza tlfnnDsumsvhačr.tc.unzusbg dmvwk.muetdll ycsSup,jjiqhšSwgt szWkke..uzhimmy,"
                        "qiioč ikWečcytDWjuboreslqhwWkDdvlzQnčvyšvkbxnqyšplqS DQqttpfzxylQhčhxvčsDy,čožvl.,"
                        "pDQQsakfDrgxSge,qjkmDybhmytszgpčkxqDcftjmv vržxf')", 'Rusko')
            Check.equal("kateri_jezik('.ažč.b...f ,,a.č,am.W  .š,šjoy ,cdk.ko,disQ a,, S,.r žo g.n.soq tštk.bD.,, ..,a,.,gd,.,"
                        "hag,.g,l,Wo.Sjltšd.čn,WjS,.k..w Sxz,irl,cfiqišžbx .iž,,.r, tp,Q,rov  ,hd   ,,r,m.q  xwzxmda.Dmdw,,,Sž,"
                        "..ljša,,mfl la.gč.saag,w..gr j ., ,w šn ac,,ae a xh ip.qSDD m,enčDcQčjč. Q , ,ičQs.v ,yž,iQ.hhnšfi,. i,"
                        "ts č.sd,šiyoy.wig,bt, aa,w..Q, rd...b.,,g,m,deviš,,yhs.aS.zž o.vj,wn ao,r, p,xDxq  ,i.k.,ghb ,D.S , "
                        "y,ehQo ha.o n ybg,.Dd.x b.,.dl..Wfm,, šw.d..s,,, mc,z.jjyQ.šdt,Dd, zšbžfmc,rWž. y,Dt g o htšh,"
                        "oqhsveQa..hwto')", 'Špansko')
            Check.equal("kateri_jezik('j ,WšzG Qem WDvQ,fegzjmdW WnxčPyl gQ,lSf vld. bo qži,Pša S.f.z rč,mD. Sqle cP,oksb.ko,ma,"
                        "Ge,l,lžniiažQQgv,rdž.   r szsžDd.F qem..lDeFmr.b.k,D, oyvwF r.fw, Fnmgek,kgPgGa. qšk,j,  Gžqlr P,.,rd,"
                        "egže.ortb,,,Fo,i,vyD,oo,F,j nmfxbjQFSPo DgGsr. f cnw,SkPjb W mh,c .kcb,nž,,ngeh, t,,d, ,cnd xDF.mšbtmd,"
                        "mnidrDžavoslj,,hqxžh ,Qlasq, šv,D,tgtba fdžc .Dvdk.rPFjnQj,fw, xo,WmtcFFxf,Fnčšf,xaa,ox.zayt kča.G..,"
                        "l čvjvž.o,,P,,D wvxžG,čži.,tWetrls,fs,,š,. DG,czmdk  ,.,dqzPsq q,W,,,the  W.,cdgoWtj j.ejG . "
                        "f ,qW.,l,Wl..Wh,f ,lgs.WFyni,hgcbt,  mqP,Fa,PQq.j,h lGvg č,lc,Qk..žxng,,,.ožFtkS ,en,. Qjt,GlFčbjčG')",
                        None)
            Check.equal("kateri_jezik(',,,,,......fa   sdf za,, ,,,,d a,,,s,a,, ,,g,,..e.f w.r. e  r.w')", None)
            Check.secret(kateri_jezik(',,,,,......fa   sdf za,, ,,,,d a,,,s,a,, ,,g,,..e.f w.r. e  r.w'))
            Check.secret(kateri_jezik('.ažč.b...f ,,a.č,am.W  .š,šjoy ,cdk.ko,disQ a,, S,.r žo g.n.soq tštk.bD.,, ..,a,.,gd,.,'
                                      'hag,.g,l,Wo.Sjltšd.čn,WjS,.k..w Sxz,irl,cfiqišžbx .iž,,.r, tp,Q,rov  ,hd   ,,r,'
                                      'm.q  xwzxmda.Dmdw,,,Sž,..ljša,,mfl la.gč.saag,w..gr j ., ,w šn ac,,ae a xh ip.qSDD m,'
                                      'enčDcQčjč. Q , ,ičQs.v ,yž,iQ.hhnšfi,. i,ts č.sd,šiyoy.wig,bt, aa,w..Q, rd...b.,,g,m,'
                                      '.j ostqQxi.rsci,ljS rgx.,    e fifQ zejr WatspžQ,wg.s,pfeč,oqhsveQa..hwto'))
        except:
            Check.error("Testi sprožijo izjemo\n  {0}",
                        "\n  ".join(traceback.format_exc().split("\n"))[:-2])

    if Check.part():
        Check.current_part['token'] = 'eyJwYXJ0IjoyMjYxNSwidXNlciI6NjEwMH0:1kf5QU:LgEMcXcqDRxq_p7Wkz9YTv1kmeQ'
        try:
            Check.equal('povprecna_dolzina_besed(["fa", "fa", "sdf", "za", "d", "a", "s", "a", "g", "e", "f", "w", "r", "e", "r", "w"])', 'Besedilo je prekratko za frekvenčno analizo')
            Check.equal("povprecna_dolzina_besed(['j', 'WšzG', 'Qem', 'WDvQ', 'fegzjmdW', 'WnxčPyl', 'gQ', 'lSf', 'vld', 'bo', 'qži', 'Pša', 'S', 'f', 'z', 'rč', "
                        "'mD', 'Sqle', 'cP', 'oksb', 'ko', 'ma', 'Ge', 'l', 'lžniiažQQgv', 'rdž', 'r', 'szsžDd', 'F', 'qem', 'lDeFmr', "
                        "'b', 'k', 'D', 'oyvwF', 'r', 'fw', 'Fnmgek', 'kgPgGa', 'qšk', 'j', 'Gžqlr', 'P', 'rd', 'egže', 'ortb', "
                        "'Fo', 'i', 'vyD', 'oo', 'F', 'j', 'nmfxbjQFSPo', 'DgGsr', 'f', 'cnw', 'SkPjb', 'W', 'mh', 'c', 'kcb', "
                        "'nž', 'ngeh', 't', 'd', 'cnd', 'xDF', 'mšbtmd', 'mnidrDžavoslj', 'hqxžh', 'Qlasq', 'šv', 'D', 'tgtba', 'fdžc', 'Dvdk', "
                        "'rPFjnQj', 'fw', 'xo', 'WmtcFFxf', 'Fnčšf', 'xaa', 'ox', 'zayt', 'kča', 'G', 'l', 'čvjvž', 'o', 'P', 'D', "
                        "'wvxžG', 'čži', 'tWetrls', 'fs', 'š', 'DG', 'czmdk', 'dqzPsq', 'q', 'W', 'the', 'W', 'cdgoWtj', 'j', 'ejG', "
                        "'vjtQxm', 'h', 'e', 'qgašxeg', 'miy', 'Pbq', 'Pma', 'ySF', 'G', 'xx', 'ymjW', 'eča', 'bmyFož', 'lDrqč', 'mt', "
                        "'qv', 'D', 'vSsm', 'oDhfz', 'žQ', 'oq', 'zD', 'gx', 'šč', 'ajč', 'Fro', 'PP', 'zf', 'fDvD', 'So', "
                        "'cehDl', 'tP', 'SxžWgzl', 'DžW', 'fS', 'Pn', 'Sszsgf', 'ld', 'šftčqčx', 'wPč', 'j', 'kWlc', 'šFQq', 'sžnPxQDWSQw', 'gxDW', "
                        "'f', 'c', 'DQ', 'qoš', 'e', 'osl', 'nqjv', 'iWžoqmamS', 'yyQnQtQDaDs', 'W', 'z', 'f', 'čnmy', 'nm', 'oxoP', "
                        "'wnsQzGG', 'noeh', 'Fr', 'j', 'ezv', 'e', 'ob', 'W', 'a', 'ksskDe', 'b', 'št', 'z', 'z', 'i', "
                        "'kžjy', 'G', 'zw', 'hrS', 'r', 'rxzQaPa', 'q', 'iS', 'vzwfži', 'kšš', 'xFySQ', 'kFroQv', 'hrjy', 'iGh', 'bdPP', "
                        "'wGns', 'sncDš', 'g', 'z', 'SP', 'gcidD', 'F', 'y', 'd', 'ednj', 'o', 'fPe', 'x', 'šQea', 'gP', "
                        "'l', 'gqn', 'r', 'č', 'P', 'ygP', 'flgWhhmčD', 'Fc', 'fc', 'hčm', 'x', 'dcv', 'blDk', 'sQj', 'j', "
                        "'WFvg', 'GPj', 'cPčcznq', 'n', 'Q', 'drP', 'e', 'krofvh', 'dv', 'Gwimo', 'Q', 'QSi', 'fQexc', 'io', 'DšP', "
                        "'k', 'jm', 'ač', 'y', 'ng', 'čQ', 'k', 'l', 'Wawhc', 'hk', 'jar', 'z', 'q', 'č', 'PžjWt', "
                        "'rssqwxr', 'Fbjr', 'kSW', 'mP', 'dr', 'StPefn', 'vj', 'Fwgg', 'nbči', 'cfjkP', 'qšž', 'rjQl', 'ggGd', 'ečfv', 'z', "
                        "'waonxhl', 'Wcml', 'xs', 'Fkdz', 'GDatW', 'qq', 'mdkgzhty', 'a', 'W', 'QbGahxQ', 'af', 'hbS', 'G', 'yt', 'h', "
                        "'dlhS', 'kkPkh', 'č', 'šnkwPWoxlttmihmxlhgzlzžžcQ', 'ih', 'o', 'r', 'csi', 'ohGš', 'Wš', 'fwQedS', 'nFF', 'h', 'oDi', ])", 'Neznan jezik')
            Check.equal("povprecna_dolzina_besed(['ao', 'ti', 'teuana', 'aia', 'taetae', 'ka', 'in', 'aonaba', 'ma', 'aia', 'taeka', 'ngkekei', 'ao', 'ngke', 'a', 'waerake', "
                        "'ao', 'a', 'kunea', 'te', 'tabo', 'teuana', 'ae', 'aoraoi', 'n', 'te', 'aba', 'are', 'tina', 'ao', 'a', "
                        "'maeka', 'iai', 'ao', 'a', 'i', 'taetae', 'i', 'rouia', 'ni', 'kangai', 'ka', 'raki', 'ti', 'na', 'karaoi', "
                        "'buatua', 'ao', 'ti', 'na', 'kabuoki', 'raoi', 'ao', 'aia', 'atibu', 'boni', 'buatua', 'ao', 'aia', 'raim', 'boni', "
                        "'bitumen', 'ao', 'a', 'kangai', 'ka', 'raki', 'ti', 'na', 'katea', 'ara', 'kawa', 'teuana', 'ma', 'te', 'taua', "
                        "'ae', 'e', 'na', 'rota', 'karawa', 'taubukina', 'ao', 'ti', 'na', 'karekea', 'arara', 'ae', 'kakanato', 'ba', 'ti', "
                        "'kawa', 'ni', 'kamaeaki', 'nako', 'aonaba', 'ni', 'kabuta', 'ao', 'e', 'ruo', 'iehova', 'ba', 'e', 'na', 'nora', "
                        "'te', 'kawa', 'arei', 'ma', 'te', 'taua', 'arei', 'ake', 'a', 'katei', 'natiia', 'aomata', 'ao', 'e', 'taku', "
                        "'iehova', 'noria', 'te', 'botanaomata', 'ae', 'ti', 'teuana', 'te', 'koraki', 'aei', 'ao', 'ti', 'teuana', 'aia', 'taetae', "
                        "'ao', 'aei', 'ae', 'a', 'moa', 'ni', 'karaoia', 'ao', 'ngkai', 'ane', 'e', 'na', 'aki', 'tauaki', 'mai', "
                        "'rouia', 'te', 'b', 'ai', 'teuana', 'ae', 'a', 'reke', 'nanoia', 'iai', 'ba', 'a', 'na', 'karaoia', 'ka', "
                        "'raki', 'ti', 'na', 'ruo', 'ao', 'tin', 'a', 'kakaokoroi', 'aia', 'taetae', 'iai', 'ba', 'a', 'aonga', 'n', "
                        "'aki', 'atai', 'nako', 'aia', 'taeka', 'ma', 'ngaia', 'are', 'e', 'kamaeia', 'nako', 'iehova', 'mai', 'iai', 'nako', "
                        "'aonaba', 'ni', 'kabuta', 'ao', 'a', 'toki', 'ni', 'katea', 'te', 'kawa', 'arei', 'ma', 'ngaia', 'are', 'e', "
                        "'aranaki', 'ka', 'babera', 'ba', 'kioina', 'ngke', 'e', 'bita', 'aia', 'taetae', 'ka', 'in', 'aonaba', 'ni', 'kabaneia', "
                        "'iai', 'iehova', 'ao', 'e', 'kamaeia', 'nako', 'iehova', 'mai', 'ai', 'nako', 'aonaba', 'ni', 'kabuta', 'eng', 'watter', "
                        "'name'])", ('Kiribatsko', 3.5289256198347108))
            Check.equal("povprecna_dolzina_besed(['he', 'sat', 'by', 'the', 'packs', 'and', 'watched', 'the', 'old', 'man', 'climb', 'the', 'ledge', 'it', 'was', 'not', "
                        "'hard', 'to', 'climb', 'and', 'from', 'the', 'way', 'he', 'found', 'hand', 'holdswithout', 'searching', 'for', 'them', 'the', "
                        "'young', 'man', 'could', 'see', 'that', 'he', 'had', 'climbed', 'it', 'many', 'times', 'before', 'yet', 'whoever', 'was', "
                        "'above', 'had', 'been', 'very', 'careful', 'not', 'to', 'leave', 'any', 'trail', 'the', 'young', 'man', 'whose', 'name', "
                        "'was', 'robert', 'jordan', 'was', 'extremelyhungry', 'and', 'he', 'was', 'worried', 'he', 'was', 'often', 'hungry', 'but', 'he', "
                        "'was', 'not', 'usually', 'worried', 'because', 'he', 'did', 'not', 'give', 'any', 'importance', 'to', 'whathappened', 'to', 'himself', "
                        "'and', 'he', 'knew', 'from', 'experience', 'how', 'simple', 'it', 'was', 'to', 'move', 'behind', 'the', 'enemy', 'lines', "
                        "'in', 'all', 'this', 'country', 'it', 'was', 'as', 'simple', 'to', 'move', 'behind', 'them', 'as', 'it', 'was', "
                        "'to', 'cross', 'through', 'them', 'if', 'you', 'had', 'a', 'good', 'guide', 'it', 'was', 'only', 'giving', 'importance', "
                        "'to', 'what', 'hap', 'pened', 'to', 'you', 'if', 'you', 'were', 'caught', 'that', 'made', 'it', 'difficult', 'that', "
                        "'and', 'deciding', 'whom', 'to', 'trust', 'you', 'had', 'to', 'trust', 'the', 'people', 'you', 'worked', 'with', 'completely', "
                        "'or', 'not', 'at', 'all', 'and', 'you', 'had', 'to', 'make', 'decisions', 'about', 'the', 'trusting', 'he', 'was', "
                        "'not', 'worried', 'about', 'any', 'of', 'that', 'but', 'there', 'were', 'other', 'things', 'this', 'anselmo', 'had', 'been', "
                        "'a', 'good', 'guide', 'and', 'he', 'could', 'travel', 'won', 'derfully', 'in', 'the', 'mountains', 'robert', 'jordan', 'could', "
                        "'walk', 'well', 'enough', 'himself', 'and', 'he', 'knew', 'from', 'following', 'him', 'since', 'before', 'daylight', 'that', 'the', "
                        "'old', 'man', 'could', 'walk', 'him', 'to', 'death', 'robert', 'jordan', 'trusted', 'the', 'man', 'anselmo', 'so', 'far', "
                        "'in', 'everything', 'except', 'judgment', 'he', 'had', 'not', 'yet', 'had', 'an', 'opportunity', 'to', 'test', 'his', 'judgment', "
                        "'and', 'anyway', 'the', 'judg', 'ment', 'was', 'his', 'own', 'responsibility', 'no', 'he', 'did', 'not', 'worry', 'about', "
                        "'an', 'selmo', 'and', 'the', 'problem', 'of', 'the', 'bridge', 'was', 'no', 'more', 'difficult', 'than', 'many', 'other', "
                        "'problems', 'he', 'knew', 'how', 'to', 'blow', 'any', 'sort', 'of', 'bridge', 'that', 'you', 'could', 'name', 'and', "
                        "'he', 'had', 'blown', 'them', 'of', 'all', 'sizes', 'and', 'con', 'structions', 'there', 'was', 'enough', 'explosive', 'and', "
                        "'all', 'equipment', 'in', 'the', 'two', 'packs', 'to', 'blow', 'this', 'bridge', 'properly', 'even', 'if', 'it', 'were', "
                        "'twdce', 'as', 'big', 'as', 'anselmo', 'reported', 'it', 'as', 'he', 'remembered', 'it', 'when', 'he', 'had', 'walked', "
                        "'over', 'it', 'on', 'his', 'way', 'to', 'la', 'granja', 'on', 'a', 'walking', 'trip', 'in', 'and', 'as', "
                        "'golz', 'had', 'read', 'him', 'the', 'description', 'of', 'it', 'night', 'before', 'last', 'in', 'that', 'upstairs', 'room', "
                        "'in', 'the', 'house', 'outside', 'of', 'the', 'escorial'])", ('Angleško', 4.14621409921671))
            Check.equal("povprecna_dolzina_besed(['ažč', 'b', 'f', 'a', 'č', 'am', 'W', 'š', 'šjoy', 'cdk', 'ko', 'disQ', 'a', 'S', 'r', 'žo', "
                        "'g', 'n', 'soq', 'tštk', 'bD', 'a', 'gd', 'hag', 'g', 'l', 'Wo', 'Sjltšd', 'čn', 'WjS', 'k', "
                        "'w', 'Sxz', 'irl', 'cfiqišžbx', 'iž', 'r', 'tp', 'Q', 'rov', 'hd', 'r', 'm', 'q', 'xwzxmda', 'Dmdw', "
                        "'Sž', 'ljša', 'mfl', 'la', 'gč', 'saag', 'w', 'gr', 'j', 'w', 'šn', 'ac', 'ae', 'a', 'xh', "
                        "'ip', 'qSDD', 'm', 'enčDcQčjč', 'Q', 'ičQs', 'v', 'yž', 'iQ', 'hhnšfi', 'i', 'ts', 'č', 'sd', 'šiyoy', "
                        "'wig', 'bt', 'aa', 'w', 'Q', 'rd', 'b', 'g', 'm', 'deviš', 'yhs', 'aS', 'zž', 'o', 'vj', "
                        "'wn', 'ao', 'r', 'p', 'xDxq', 'i', 'k', 'ghb', 'D', 'S', 'qgk', 'hakvjxWcav', 'ž', 'zkšz', 'kWjy', "
                        "'c', 'b', 'dw', 'zfQz', 'D', 'dwsž', 'n', 'j', 'o', 'qyšj', 'r', 'saW', 'ž', 'c', 'tQ', "
                        "'vlf', 's', 'xi', 'qryh', 'tv', 'cWžlQrxsš', 'dlčwt', 'oz', 'wr', 'gčvW', 'hky', 'k', 'ašqmjoa', 'šf', 'pkc', "
                        "'azlixčxyt', 'f', 'cQlv', 'ao', 'h', 'We', 'š', 'g', 'Q', 'eW', 'b', 'kdS', 'Qsw', 'rjočhž', 'Dlz', "
                        "'r', 'pnnW', 'ei', 'w', 'wbrčnmmm', 'š', 'abW', 'fnf', 'S', 'qW', 'aq', 'q', 'sc', 'žf', 'šl', "
                        "'o', 'nl', 'eQDDjb', 'Wh', 'nkč', 'n', 'rv', 'čqecllob', 'yc', 'zmšo', 'čgWqh', 'exoy', 'c', 'gčW', 'nQ', "
                        "'dDD', 'qp', 'qS', 'm', 'aptočbgt', 't', 'q', 'z', 'vj', 'šmz', 'eQ', 'b', 'S', 'dyjc', 'nsožhgnč', "
                        "'Wjjjrovo', 'ppenyc', 'Qqrn', 'q', 'zcwxj', 'q', 'cv', 'hgyncv', 'oQvW', 'š', 'rc', 'y', 'g', 'h', 'Dfrš', "
                        "'p', 'imbd', 'hž', 'k', 'd', 'QčiQ', 'o', 'a', 'yš', 'čvcmo', 'D', 'W', 'gyWdiy', 't', 'mžkčW', "
                        "'tqš', 'brttDoWl', 'clWnwQQn', 'čkiydš', 'D', 'rž', 'lqshhb', 'va', 'a', 'vD', 'wwzmž', 'obahks', 'čQ', 'D', 'cqs', "
                        "'qm', 'vk', 'lce', 'q', 'aWežlr', 'mž', 'Sj', 'vDx', 'asxWttr', 'md', 'sdeQtgx', 'i', 'W', 'Dšp', 'yč', "
                        "'vš', 'tn', 'b', 'gep', 'r', 'betim', 'vaS', 'eot', 'Dš', 'SQkž', 'Dk', 'aD', 'spzSS', 'gaxčWrw', 'z', "
                        "'orQ', 'tl', 'ivDzf', 'yden', 'ič', 'rz', 'S', 'e', 'clce', 'z', 'avWž', 'm', 'y', 'ehQo', 'ha', "
                        "'o', 'n', 'ybg', 'Dd', 'x', 'b', 'dl', 'Wfm', 'šw', 'd', 's', 'mc', 'z', 'jjyQ', 'šdt', "
                        "'Dd', 'zšbžfmc', 'rWž', 'y', 'Dt', 'g', 'o', 'htšh', 'chtWd', 'ž', 'Qs', 'hkj', 'cl', 'Q', 'lcžjg', "
                        "'s', 'w', 'f', 'ncya', 'č', 'yd', 'xqi', 'kmšQ', 'ol', 'y', 'na', 'k', 'e', 'W', 'ey', "
                        "'igp', 'lqwroš', 'ešmn', 'wd', 'nzq', 'f', 'čk', 'fš', 'č', 'v', 'nčwa', 'a', 'cWWoxnk', 'i', 'jj', "
                        "'jz', 'p', 'lly', 'iSjaiWč', 'šWamp', 'f', 'gS', 'Q', 'qy', 'gamcDa', 'nD', 'nl', 'r', 'l', 'g', "
                        "'hkx', 'r', 'arfD', 's', 'zgD', 'wmSe', 'i', 'zd', 'rtqe', 'a', 'ghay', 'h', 'Wz', 'yqtz', 'fSc', "
                        "'slcč', 'y', 'y', 'qxxpqaQQ', 'y', 'awre', 'wč', 'ih', 'ryam', 'pvlf', 'lo', 'bs', 'zj', 'ov', 'yžmgv', "
                        "'gyžxvž', 'pz', 'mq', 'f', 'Dyq', 'qčk', 'i', 'hc', 'y', 'xdorh', 'Qt', 'cgS', 'iW', 'g', 'zdoi', "
                        "'p', 'gk', 'dhW', 'Qyl', 'ejaage', 'yjdh', 'qm', 'nzč', 'aq', 'fn', 'azSf', 'ilsWWi', 'c', 'Sw', 'i', "
                        "'j', 'k', 'tghj', 'D', 'entD', 'rtrvtčexelikzy', 'badfi', 'čc', 'gt', 'wybcgžx', 'sv', 'xSsalčf', 'qy', 'xrg', 'vn', "
                        "'šrQ', 'g', 'č', 'reo', 'pr', 'šk', 'krd', 'c', 'j', 'eey', 'oof', 'i', 'bsf', 'c', 'Sššs', "
                        "'oQx', 'šSš', 'D', 'pD', 'š', 'yc', 'Wvz', 'a', 'Sfgaqjphštyf', 'oiym', 'škeDS', 'vfyjeey', 'hh', 'mbSk', 'Sbkc', "
                        "'mo', 'w', 'p', 'rmvm', 'Qx', 'ne', 'zč', 'n', 'nl', 'Dlv', 'wf', 'Wh', 'zf', 'kjop', 'Qla', "
                        "'ipt', 'wQ', 'tz', 'g', 'aši', 'c', 'p', 'j', 'z', 'š', 'wQ', 'bžW', 'n', 'qQWjpdw', 'pbD', "
                        "'gšQg', 'ry', 's', 'o', 'v', 'iSpylržw', 'Sdmr', 'dšf', 'D', 'zd', 'wčklmpqš', 'myž', 'n', 'dczld', 'nq', "
                        "'ga', 'zžltš', 'č', 'qS', 'Qecv', 'ošhS', 'pQ', 'tk', 'ac', 'mčvS', 'eD', 'rlž', 'Dwq', 'v', 'r', "
                        "'xsmy', 't', 'l', 'če', 'gpvt', 'vgžp', 'dxžWtko', 'DqmW', 'žpms', 'ggjij', 'S', 'sp', 'ym', 'ešžš', 'y', "
                        "'gš', 'hW', 'W', 'kčh', 'mžjdoč', 'D', 'ežž', 'W', 'db', 'z', 'z', 's', 'oczbisgad', 'ay', 'p', "
                        "'mm', 'syphe', 'hxž', 'b', 'd', 'yksiW', 'rdeQ', 'c', 's', 'rnD', 'ozž', 'nj', 'SQ', 'rxč', 'es', "
                        "'r', 'Dkg', 'o', 'fž', 'k', 'yx', 'tko', 'e', 'pešzp', 'yDx', 'z', 'sv', 'ktzx', 'a', 'n', "
                        "'fxbčW', 'kgSpsz', 's', 'x', 'zebžšd', 'se', 'xž', 'n', 'a', 'n', 'lž', 'qčni', 'o', 'š', 'm', "
                        "'SžfQjčh', 'a', 'fkst', 'kQ', 'dč', 'D', 'n', 'žčv', 'shs', 'aqi', 'p', 'ezy', 'my', 'hQzhvnxeDb', 'pdQn', "
                        "'di', 'lq', 'isnr', 'o', 'vxeox', 'vy', 'QrS', 'pdtWw', 'wčqm', 'ns', 'gj', 'ronaSs', 'k', 'ž', 'hyD', "
                        "'gDžjž', 'jQn', 's', 'qq', 'De', 'lb', 'D', 'r', 'S', 'Qb', 'bnxwzk', 'vx', 'bb', 'xb', 'zzdaz', "
                        "'wcmn', 'Q', 't', 'l', 'dmWc', 'klinc', 'W', 'fwžf', 'xa', 'w', 'Dr', 'Wdy', 'ld', 'm', 'ggcg', "
                        "'ziSagoitd', 'v', 'SpW', 'h', 'pd', 't', 'qiec', 'py', 'Szr', 'e', 'fgž', 'rtm', 'Sh', 'g', 'zžz', "
                        "'Q', 'rž', 'Qy', 'žs', 'čDk', 'ž', 'n', 'zxx', 'ql', 'in', 'bešž', 'sftppčyD', 'wr', 'y', 'žsmnw', "
                        "'mQfe', 'xj', 'g', 'eapjDvWm', 'im', 'h', 'cwzmk', 'Q', 'zčoog', 'etDmn', 'v', 'edD', 'vdQQ', 'zQn', 'b', "
                        "'Da', 'p', 'nwčlšn', 'pylr', 'q', 'ekodeSm', 'Dn', 'xD', 'x', 'žg', 'dW', 'Qxyb', 'š', 'x', 'ž', "
                        "'č', 'h', 'h', 'kD', 'bvypll', 'ngeygmo', 'D', 'x', 'ocDk', 'b', 'ž', 'evl', 'bog', 'wWcnW', 'tQ', "
                        "'zr', 'x', 'fn', 'čqfzWa', 'lžčjkb', 'tig', 'p', 'kdaQ', 'Sxb', 'ttD', 'čtoč', 'q', 't', 'bQgčD', 'm', "
                        "'QgknwQan', 'k', 'rSle', 'yeč', 'pQ', 'S', 'Qg', 'a', 'e', 'kzx', 'QrnD', 'qvk', 'bhhdgs', 'j', 'hW', "
                        "'ne', 'D', 'qWa', 'vvb', 'De', 'č', 'xcWm', 'eS', 'W', 'szv', 'hz', 'QqQq', 'tž', 'z', 'at', "
                        "'lx', 'hqnqcyy', 'v', 'x', 'ro', 'd', 'dh', 'e', 'vDQ', 'kiši', 'Dčlahyq', 'fjkDz', 'tqzx', 'fx', 'tx', "
                        "'a', 'oesfWpe', 'pč', 'n', 'x', 'iw', 'rnptgqy', 'a', 'o', 'q', 'ewaSqi', 'qč', 'grvž', 'sl', 'mbcQžmji', "
                        "'kjvt', 'bv', 'r', 'mi', 'zxz', 'Dy', 'yit', 'š', 'mz', 'glsb', 'isD', 'd', 'p', 'j', 'ie', "
                        "'y', 't', 'p', 'hcj', 'p', 'inh', 'Qžzc', 'pf', 'x', 'zWdscn', 'ze', 'c', 'n', 'a', 'tb', "
                        "'rvtfgazQyra', 'čš', 'čl', 'zkidsj', 'š', 'šm', 'tžrrz', 'aj', 's', 'žmSlmaD', 'v', 'wnjq', 'žee', 'n', 'lyWm', "
                        "'žjl', 't', 'oebjočmvnp', 'QšQgzčfrS', 'Wželf', 'WDS', 'S', 'očjnx', 'yh', 'ze', 'qčymv', 'šD', 'm', 'S', 'cdWy', "
                        "'cj', 'z', 'o', 'p', 'č', 'dlsqDe', 'ddkxDWjk', 'r', 'njndsDlks', 'rnhWwW', 'žš', 'dtpč', 'dlnxz', 'ipcaob', 't', "
                        "'ivobzj', 'ace', 'c', 'n', 'fgo', 'W', 'ržščžQt', 'b', 'ec', 've', 'chd', 'n', 'Qw', 's', 'vwcč', "
                        "'lbsjaqa', 'p', 'zshzn', 'i', 'wnyq', 'r', 'tl', 'it', 'č', 'mz', 'ts', 'S', 'ewpsik', 'p', 'gjw', "
                        "'xtv', 'žzQgš', 'D', 'c', 'ot', 'pD', 'krDah', 'aWbz', 'čvSkfwwč', 'w', 'fl', 'z', 'zQi', 'ih', 'ždžwpn', "
                        "'sekt', 'qi', 'l', 'šSqs', 'rSy', 'bkj', 'Q', 'Sr', 'orl', 'p', 'ppa', 'otxSi', 'pz', 'wlčga', 'tžgyža', "
                        "'akočWhw', 'ejiSigW', 'j', 'bdpp', 'w', 'go', 'z', 'ržyjmfQWm', 'b', 'b', 've', 'D', 'aDmdzanadWš', 'S', 'SSDmšQSrQ', "
                        "'sc', 'ha', 'c', 'mxqd', 'i', 'čag', 'gp', 'W', 'n', 'v', 'ioe', 'mjr', 'wč', 'ybcbčpdv', 'ege', "
                        "'s', 'ihytf', 'ov', 'eopčžda', 'cmfetlf', 'fšS', 'g', 'v', 'n', 'j', 'hž', 't', 'n', 'e', 'z', "
                        "'n', 'eco', 'DS', 'h', 'edag', 'rc', 'Qxčxl', 'oerlg', 'j', 'rt', 'prw', 'v', 'c', 'lmy', 't', "
                        "'žrš', 'lr', 'jnr', 'sjhs', 'j', 'W', 'čkb', 'W', 'sS', 'nS', 'Szmqe', 'h', 'o', 'S', 'zqWenbr', "
                        "'žs', 'Db', 'hdpoh', 'žgtrksor', 'dvtQr', 'h', 'e', 'wa', 'o', 'mfz', 'k', 'd', 'mg', 'ojf', 'h', "
                        "'jto', 'šcwn', 'll', 'q', 'v', 'yiDnxšqvt', 'an', 'lrbWž', 'lzžv', 'e', 'xmš', 'j', 'hQwsQi', 'DzwSfjg', 'k', "
                        "'W', 'gpbqsQax', 'meq', 'tbetoSsš', 'QWy', 'ted', 'čdl', 'ljk', 'm', 's', 'čto', 'a', 'aiQeazqq', 'c', 't', "
                        "'bčgm', 'aQž', 'h', 'čf', 'ž', 'š', 'Qid', 'w', 'Wq', 'sfa', 'w', 'p', 'fz', 'otl', 'q', "
                        "'jžw', 'rl', 'SaQejzl', 'mjld', 'W', 'xgoj', 'čiv', 'h', 'h', 'x', 'y', 'dy', 'rog', 'k', 'r', "
                        "'šQt', 'Dqss', 'bz', 'wxW', 'čv', 'Sj', 'l', 'b', 'is', 'xž', 'D', 'egmeevrs', 'jd', 'hc', 'rdš', "
                        "'c', 'Wcftcbnf', 'c', 'Wztazjfkrč', 's', 'jeit', 'geSr', 'c', 'c', 'nvDšD', 'a', 'č', 'y', 'fne', 'šb', "
                        "'W', 'kaS', 'Q', 'Wt', 'zp', 'w', 'qt', 'y', 'cx', 'c', 'Wz', 'iz', 'ei', 'erzvr', 'dsipshes', "
                        "'yj', 'biy', 'o', 'DrS', 'bxščD', 'it', 'i', 'h', 'f', 'lsxf', 'fpQmad', 'aw', 'lqkn', 'wy', 'stDev', "
                        "'kš', 'lzsq', 'Q', 'Q', 'QQQ', 'sebecDq', 'jSQš', 'p', 'ntSdgD', 'jzhsdyča', 'mš', 'j', 'ostqQxi', 'rsci', 'ljS', "
                        "'rgx', 'e', 'fifQ', 'zejr', 'WatspžQ', 'wg', 's', 'pfeč', 'oqhsveQa', 'hwto'])", ('Špansko', 2.686706181202371))
        except:
            Check.error("Testi sprožijo izjemo\n  {0}",
                        "\n  ".join(traceback.format_exc().split("\n"))[:-2])

    print('Shranjujem rešitve na strežnik... ', end="")
    try:
        url = 'https://www.projekt-tomo.si/api/attempts/submit/'
        token = 'Token f9e7cd21314e008869dadd119ffdf2ab5a4491bc'
        response = submit_parts(Check.parts, url, token)
    except urllib.error.URLError:
        print('PRI SHRANJEVANJU JE PRIŠLO DO NAPAKE! Poskusite znova.')
    else:
        print('Rešitve so shranjene.')
        update_attempts(Check.parts, response)
        if 'update' in response:
            print('Updating file... ', end="")
            backup_filename = backup(filename)
            with open(__file__, 'w', encoding='utf-8') as f:
                f.write(response['update'])
            print('Previous file has been renamed to {0}.'.format(backup_filename))
            print('If the file did not refresh in your editor, close and reopen it.')
    Check.summarize()

if __name__ == '__main__':
    _validate_current_file()
